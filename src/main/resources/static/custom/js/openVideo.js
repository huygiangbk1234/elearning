var video = document.querySelector('video');
var buffer;
var queue=[];
//var mimeCodec = 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"'; //avc1.42E01E avc1.58A01E avc1.4D401E avc1.64001E mp4v.20.8 mp4v.20.240
var mimeCodec = 'video/webm;codecs=vp8';//webm
//var mineCodec= 'video/x-matroska; codecs="theora, vorbis"';//mkv

document.querySelector('button').addEventListener('click',function(){fetchAB();});

var mediaSource =new MediaSource();
video.src=URL.createObjectURL(mediaSource);

mediaSource.addEventListener('sourceopen',function(){
	buffer = mediaSource.addSourceBuffer(mimeCodec);
	buffer.addEventListener('updatestart', function(e) { console.log('updatestart: ' + mediaSource.readyState); });
	buffer.addEventListener('update', function(e) { console.log('update: ' + mediaSource.readyState); });
	buffer.addEventListener('updateend', function(e) { console.log('updateend: ' + mediaSource.readyState); });
	buffer.addEventListener('error', function(e) { console.log('error: ' + mediaSource.readyState +" "+e); });
	buffer.addEventListener('abort', function(e) { console.log('abort: ' + mediaSource.readyState); });
	
	buffer.addEventListener('update',function(){
		if (queue.length > 0 && !buffer.updating) {
	      buffer.appendBuffer(queue.shift());
	    }
	});
	connect();
	//fetchAB();
});
mediaSource.addEventListener('sourceopen', function(e) { console.log('sourceopen: ' + mediaSource.readyState); });
mediaSource.addEventListener('sourceended', function(e) { console.log('sourceended: ' + mediaSource.readyState); });
mediaSource.addEventListener('sourceclose', function(e) { console.log('sourceclose: ' + mediaSource.readyState); });
mediaSource.addEventListener('error', function(e) { console.log('error: ' + mediaSource.readyState); });


//function sourceOpen () {
//  
//  var mimeCodec = 'video/webm;codecs=vp8';
// // var mimeCodec = 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"';
//  if ('MediaSource' in window && MediaSource.isTypeSupported(mimeCodec)) {
//  	  var mediaSource = new MediaSource();
//  	  video.src = URL.createObjectURL(mediaSource);
//  	  fetchAB(function (buf) {
//    	var sourceBuffer = mediaSource.addSourceBuffer(mimeCodec);
//  	  	mediaSource.addEventListener('updateend', function (_) {
//	        mediaSource.endOfStream();
//	        video.play();
//	        console.log(mediaSource.readyState); // ended
//	      });
//  	  	sourceBuffer.appendBuffer(buf);
//  	  	mediaSource.addEventListener("sourceended",function(){alert("a");});
//  	  });
//	} else {
//	  console.error('Unsupported MIME type or codec: ', mimeCodec);
//	}
//  
//};

function fetchAB ( cb) {
  var xhr = new XMLHttpRequest;
  xhr.open('get', "getVideo");
  xhr.responseType = 'arraybuffer';
  xhr.onload = function () {
    //cb(xhr.response);
    if (buffer.updating || queue.length > 0) {
        queue.push(xhr.response);
      } else {
        buffer.appendBuffer(xhr.response);
      }
  };
  xhr.send();
};

function connect() {
    // Create and init the SockJS object
    var socket = new SockJS('/message');
    var stompClient = Stomp.over(socket);	
    // Subscribe the '/notify' channell
    stompClient.connect({}, function(frame) {
      stompClient.subscribe('/user/queue/notify', function(notification) {
    	  
    	  var data=JSON.parse(notification.body).content;
    	  document.querySelector("img").src="data:image/png;base64,"+data;
//    	  var blob = new Blob([data], {type: 'image/png'});
//    	  var reader = new FileReader();
//    	  if (buffer.updating || queue.length > 0) {
//		        queue.push(blob);
//		      } else {
//		        buffer.appendBuffer(blob);
//		      } 
//    	    reader.readAsDataURL(blob); 
//    	    reader.onloadend = function() {
//    	    	video.src=(reader.result);
//    	    	
//    	    }
      });
    });
    
    return;
  } // function connect
var BASE64_MARKER = ';base64,';

function convertDataURIToBinary(b64Data) {
	var byteCharacters = atob(b64Data);
	var byteNumbers = new Array(byteCharacters.length);
	for (var i = 0; i < byteCharacters.length; i++) {
	    byteNumbers[i] = byteCharacters.charCodeAt(i);
	}
  return byteNumbers;
}
  /**
   * Display the notification message.
   */
  function notify(message) {
    $("#notifications-area").append(message + "\n");
    return;
  }
  
  /**
   * Init operations.
   */
  $(document).ready(function() {
    
    // Start the web socket connection.
  //  connect();
    
  });
window.addEventListener("load",function() {
	var menu = new Vue({
		el : '#pn-menu',
		data : {
			isShow : false,
			isShowing : true,
			time : 2000,
			actionHide : null,
		},
		watch : {
			isShow : function(x) {
				if (x) {
					this.show();
				}
			}
		},
		methods : {
			autoHide : function(t) {
				t = t || this.time;
				if (this.isShow)
					return;
				if (this.actionHide == null) {
					this.actionHide = setTimeout(function() {
						this.hide();
					}.bind(this), t);
				}
			},
			show : function() {
				if (this.actionHide) {
					clearTimeout(this.actionHide);
					this.actionHide = null;
				}
				$("#pn-menu").animate({
					bottom : "0px",
					height : "80px"
				});
			},
			hide : function() {
				$("#pn-menu").animate({
					bottom : "-80px",
					height : "160px"
				});
				this.actionHide = null;
			},
		}
	});
	menu.autoHide(4000);
	$("#pn-menu").mouseenter(function() {
		menu.show()
	});
	$("#pn-menu").mouseleave(function() {
		menu.autoHide()
	});
});

window.addEventListener("load",function(){
	var overview=new Vue({
		el:"#overview",
		data:{
			assignments:null,
			idCourse:-1,
			course:{},
			timeEnd:0,
			timeStart:0,
			dateStart:0,
			dateEnd:0,
		},
		mounted:function(){
			this.openDate();
			this.openTime();
		},
		watch:{
			dateEnd:function(d){this.course.timeEnd=d+this.timeEnd*60*1000;},
			timeEnd:function(d){this.course.timeEnd=this.dateEnd+d*60*1000;},
			dateStart:function(d){this.course.timeStart=d+this.timeStart*60*1000},
			timeStart:function(d){this.course.timeStart=this.dateStart+d*60*1000;},
		},
		methods:{
			openDate:function(b){
				var self=this;
				$("#ip-dateS").pickadate({
					format : 'dd/mm/yyyy',
					today : '',
					clear : '',
					close : '',
					onStart: function() {
					    var date = new Date();
					    this.set('select', date);
					    $("#ip-dateS_root").css({'pointer-events':'none'});
					},
					onOpen:function(){
						$("#ip-dateS_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-dateS_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(overview)overview.dateStart=this.get('select').pick;
					},
				});
				$("#ip-dateE").pickadate({
					format : 'dd/mm/yyyy',
					today : '',
					clear : '',
					close : '',
					onStart: function() {
					    var date = new Date();
					    this.set('select', date);
					    $("#ip-dateE_root").css({'pointer-events':'none'});
					},
					onOpen:function(){
						$("#ip-dateE_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-dateE_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(overview)overview.dateEnd=this.get('select').pick;
					},
				});
			},
			openTime:function(b){
				var self=this;
				$("#ip-timeS").pickatime({
					clear : '',
					onStart: function() {
					    this.set('select', 0);
					    $("#ip-timeS_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(overview)overview.timeStart=this.get('select').pick;
					},
					onOpen:function(){
						$("#ip-timeS_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-timeS_root").css({'pointer-events':'none'});
					},
				});
				$("#ip-timeE").pickatime({
					clear : '',
					onStart: function() {
					    this.set('select', 0);
					    $("#ip-timeE_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(overview)overview.timeEnd=this.get('select').pick;
					},
					onOpen:function(){
						$("#ip-timeE_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-timeE_root").css({'pointer-events':'none'});
					},
				});
			},
			openEdit:function(){
				this.fix();
				$("#modalEditCourse").modal('show');
				this.$forceUpdate();
			},
			save:function(){
				if(checkEmpty(this.course.name)){
					alert("Name empty!");
					return;
				}
				if(this.course.timeEnd<new Date().getTime()||this.course.timeEnd<this.course.timeStart){
					alert("Time error!");
					return;
				}
				var self=this;
				this.course.info=this.course.temp.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				$.ajax({
					url:"/course/"+self.idCourse+"/update",
					data:JSON.stringify(self.course),
					method:'post',
					contentType:"application/json;charset=UTF-8",
					success:function(){location.reload()}
				})
			},
			fix:function(){
				var find = '<br/>';
				var re = new RegExp(find, 'g');
				this.course.temp=this.course.info.replace(re,'\n');
				var t=new Date(this.course.timeEnd);
	             $("#ip-dateE").pickadate('picker').set('select',t);
	             $("#ip-timeE").pickatime('picker').set('select',t);
	             var t=new Date(this.course.timeStart);
	             $("#ip-dateS").pickadate('picker').set('select',t);
	             $("#ip-timeS").pickatime('picker').set('select',t);
			},
			loadAssignment:function(){
				var url="/course/"+this.idCourse+"/assignments/get/notsubmit";
				var s=this;
				$.ajax({
					url:url,
					success:function(data){
						if(data==null||data.length==0) return;
						s.assignments=data;
					}
				})
			}
		}
	});
	overview.idCourse=test.idCourse;
	overview.course=test.course;
	overview.loadAssignment();
});
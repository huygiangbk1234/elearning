var remove=function(e){
		$(e.target).parent().remove();
	}
window.addEventListener("load",function(){
	var File=Vue.extend({
		template:"<input type='file' name='' id='files' v-on:change='$parent.onChange()'/>"
	});
	var assignment=new Vue({
		el:'#assignment_detail',
		data:{
			idCourse:-1,
			assignment:{},
			isSubmit:true,
			title:{},
			listSubmit:null,
			userNotSubmit:null,
			userSubmit:[],
			groupSubmits:[],
			groupNotSubmits:[],
			submit:{},
			groups:[],
			
		},
		watch:{
			listSubmit:function(){
				if(this.assignment.groupSubmit){
					this.groupSubmits=[];
					for(var i=0;i<this.listSubmit.length;i++){
						var sa=this.listSubmit[i];
						var g=this.getGroupById(this.groupSubmits,sa.member.idGroup);
						if(g){
							g.members.push(sa.member);
						}else{
							var nameGroup=this.getNameGroup(sa.member.idGroup);
							var g={
									id:sa.member.idGroup,
									name:nameGroup,
									time:sa.updatedAt,
									files:sa.files,
									members:[],
									isOpen:false,
							};
							if(sa.root==0) sa.member.boss=true;
							g.members.push(sa.member);
							this.groupSubmits.push(g);
						}
					}
				}else{
					this.userSubmit=[];
					for(var i=0;i<this.listSubmit.length;i++){
						var sa=this.listSubmit[i];
						sa.member.isOpen=false;
						sa.member.files=sa.files;
						sa.member.time=sa.updatedAt;
						this.userSubmit.push(sa.member);
					}
				}
			},
			userNotSubmit:function(){
				for(var i=0;i<this.userNotSubmit.length;i++)
				if(this.userNotSubmit[i].role=='admin') {
					this.userNotSubmit.splice(i,1);
				}
				if(this.assignment.groupSubmit){
					this.groupNotSubmits=[];
					for(var i=0;i<this.userNotSubmit.length;i++){
						var member=this.userNotSubmit[i];
						var g=this.getGroupById(this.groupNotSubmits,member.idGroup);
						if(g){
							g.members.push(member);
						}else{
							var nameGroup=this.getNameGroup(member.idGroup);
							var g={
									id:member.idGroup,
									name:nameGroup,
									members:[],
									isOpen:false,
							};
							g.members.push(member);
							this.groupNotSubmits.push(g);
						}
					}
				}
			}
		},
		methods:{
			formatDate:function(time){
				var today=new Date(time);
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!

				var yyyy = today.getFullYear();
				if(dd<10){
				    dd='0'+dd;
				} 
				if(mm<10){
				    mm='0'+mm;
				} 
				var h=today.getHours();
				var m=today.getMinutes();
				
				var today =h+"h"+m+","+ dd+'/'+mm+'/'+yyyy;
				return today;
			},
			toggleOpen:function(g){
				g.isOpen=!g.isOpen;
				assignment.$forceUpdate();
			},
			getNameGroup:function(id){
				for(var i=0;i<this.groups.length;i++){
					if(this.groups[i].id==id)return this.groups[i].name;
				}
				return "Group";
			},
			getGroupById:function(g,id){
				for(var i=0;i<g.length;i++){
					if(g[i].id==id)return g[i];
				}
				return null;
			},
			deleteAssignment:function(){
				if(confirm("Are you sure?")){
					var url="/course/"+this.idCourse+"/assignment/"+this.assignment.id+"/delete";
					var urls="/course/"+this.idCourse+"/assignments/";
					$.ajax({
						url:url,
						method:'post',
						success:function(data){
							location.href=urls;
						},
						error:function(){
							console.log("error");
						}
					});
				}
			},
			getTime:function(time){
				var today=new Date(time);
				var dd = today.getDate();
				var mm = today.getMonth()+1; // January is 0!
				var hh= today.getHours();
				var mins=today.getMinutes();
				var yyyy = today.getFullYear();
				if(dd<10){
				    dd='0'+dd;
				} 
				if(mm<10){
				    mm='0'+mm;
				} 
				if(hh<10){
					hh='0'+hh;
				} 
				if(mins<10){
					mins='0'+mins;
				} 
				var today = hh+"h"+mins+" "+dd+'/'+mm+'/'+yyyy;
				return today;
			},
			getIcon:function(name){
				var ext=name.split(".");
				ext=ext[ext.length-1].toLowerCase();
				if(ext=='txt') return "<i class='fa fa-file-text-o' aria-hidden='true'></i>";
				else if(ext=='pdf') return "<i class='fa fa-file-pdf-o red' aria-hidden='true'></i>";
				else if(ext=='docx')  return "<i class='fa fa-file-word-o blue' aria-hidden='true'></i>";
				else if(ext=='mp4')  return "<i class='fa fa-file-video-o red' aria-hidden='true' ></i>";
				else if(ext=='png'||ext=='jpg'||ext=='gif')  return "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
				return  "<i class='fa fa-file' aria-hidden='true'></i>";
			},
			loadAllAssignment:function(){
				var url="/course/"+this.idCourse+"/assignments/get";
				var s=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						s.list=data;
						s.fix();
					}
				})
			},
			saveEdit:function(){
				
				var fileDelete=[];
				for(var i=0;i<this.submit.files.length;i++){
					if(this.submit.files[i].isDelete) {
						fileDelete.push(this.submit.files[i].id);
					}
				}
				var b=fileDelete.length==this.submit.files.length;
				if($("#listF [type=file]").length==0){
					if(b){
						alert("Empty data!");return;
					}
				}
				if(!confirm("Are you sure?")) return;
				var form = document.getElementById("formFile");
				var formData = new FormData(form);
				formData.append('fileDelete',fileDelete);
				var url="/course/"+this.idCourse+"/assignment/"+this.submit.id+"/submit/update";
				var u="/course/"+this.idCourse+"/assignment/"+this.assignment.id;
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						location.href=u;
					},
					error:function(){
						alert("error send data");
					}
				});
			},
			openEdit:function(){
				this.submit.isEdit=true;
				assignment.$forceUpdate();
			},
			submitAssignment:function(){
				if($("#listF [type=file]").length==0){alert("Empty data!");return;}
				if(!confirm("Are you sure?")) return;
				var form = document.getElementById("formFile");
				var formData = new FormData(form);
				formData.append("idAssignment",this.assignment.id);
				var url="/course/"+this.idCourse+"/assignment/"+this.assignment.id+"/submit";
				var u="/course/"+this.idCourse+"/assignment/"+this.assignment.id;
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						location.href=u;
					},
					error:function(){
						alert("error send data");
					}
				});
			},
			remove:function(e,i){
				this.submit.files[i].isDelete=true;
				remove(e);
			},
			createFileInput:function(name){
				return "<div class='f'>"+
				"<i class='fa fa-file-text-o' aria-hidden='true'></i>"+
				"<span>"+name+"</span>"+
				"<i class='fa fa-times r' aria-hidden='true' onclick='remove(event)'></i>"+
				"</div>";
			},
			onChange:function(){
				var id="files";
				var f=$("#files");
				var name=f.val();
				var b=false;
				$('#listF [type=file]').each(function(index,input){
					if($(input).val()==name) b=true;
				});
				if(b) return;
				name=name.split("\\");
				name=name[name.length-1];
				f.after("<div id='temp'></div>");
				new File({parent:this}).$mount("#temp");
				var list=$("#listF");
				f.attr({"id":'','name':"files"});
				
				var d=this.createFileInput(name);
				var a=$(d).appendTo(list);
				$(a).append(f);
			},
		}
	});
	assignment.idCourse=test.idCourse;
	assignment.assignment=test.assignment;
	assignment.title=test.title;
	assignment.submit=test.submit;
	assignment.groups=test.groups;
	assignment.userNotSubmit=test.userNotSubmit;
	assignment.listSubmit=test.listSubmit;
	if(assignment.submit)assignment.submit.isEdit=false;
});
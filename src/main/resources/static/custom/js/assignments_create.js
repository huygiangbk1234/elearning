var remove=function(e){
		$(e.target).parent().remove();
	}
window.addEventListener("load",function(){
	var File=Vue.extend({
		template:"<input type='file' name='' id='files' v-on:change='$parent.onChange()'/>"
	});
	var assignment=new Vue({
		el:'#create-assignment',
		data:{
			idCourse:-1,
			course:null,
			lessons:[],
			currentLesson:0,
			assignment:{timeEnd:new Date().getTime(),title:"",content:"",idLesson:-1,groupSubmit:false},
			timeEnd:0,
			dateEnd:new Date().getTime(),
			lastUrl:null,
			isEdit:false,
			
			datePicker:null,
			timePicker:null,
		},
		watch:{
			currentLesson:function(n){this.assignment.idLesson=this.lessons[n].id},
			dateEnd:function(d){this.assignment.timeEnd=d+this.timeEnd*60*1000},
			timeEnd:function(d){this.assignment.timeEnd=this.dateEnd+d*60*1000;console.log(this.timeEnd)},
		},
		mounted:function(){
			this.openDate();
			this.openTime();
		},
		methods:{
			openDate:function(b){
				var self=this;
				$("#ip-date").pickadate({
					format : 'dd/mm/yyyy',
					today : '',
					clear : '',
					close : '',
					onStart: function() {
					    var date = new Date();
					    this.set('select', date);
					},
					onOpen:function(){
						$("#ip-date_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-date_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(assignment)assignment.dateEnd=this.get('select').pick;
					},
				});
			},
			openTime:function(b){
				var self=this;
				$("#ip-time").pickatime({
					clear : '',
					onStart: function() {
					    this.set('select', 0);
					},
					onSet:function(){
						if(assignment)assignment.timeEnd=this.get('select').pick;
					},
					onOpen:function(){
						$("#ip-time_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-time_root").css({'pointer-events':'none'});
					},
				});
				//if(b)this.timePicker.pickadate('picker').close();
			},
			createAssignment:function(){
				if(this.lessons.length==0)return;
				if(checkEmpty(this.assignment.title)){
					alert("Title is empty");
					return;
				};
				if(checkEmpty(this.assignment.content)){
					alert("Content is empty");
					return;
				};
				if(this.dateEnd==0){
					alert("Time is empty");
					return;
				};
				if(this.assignment.timeEnd<= new Date().getTime()||this.assignment.timeEnd>this.course.timeEnd){
					alert("Time end is not correct");
					return;
				}
				this.assignment.idLesson=this.lessons[this.currentLesson].id
				var form = document.getElementById("formFile");
				var formData = new FormData(form);
				var content=this.assignment.content.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				formData.append('title',this.assignment.title);
				formData.append('content',content);
				formData.append('idLesson',this.assignment.idLesson);
				formData.append('timeEnd',this.assignment.timeEnd);
				formData.append('groupSubmit',this.assignment.groupSubmit);
				var url="/course/"+this.idCourse+"/assignments/create";
				var u="/course/"+this.idCourse+"/assignments";
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						if(!self.lastUrl) location.href=u;
						else{
							window.location.href=self.lastUrl;
						}
					},
					error:function(){
						alert("error send data");
					}
				});
			},
			createLesson:function(){
				var text=$("#ip-name").val();
				$("#modalCreateLesson").modal('hide');
				if(checkEmpty(text))return;
				var lesson={name:text,idCourse:this.idCourse,pos:this.lessons.length};
				var url="/course/"+this.idCourse+"/lesson/create";
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(lesson),
					contentType:"application/json;charset=UTF-8",
					success:function(data){
						self.lessons.push(data);
						self.currentLesson=self.lessons.length;
						js.menu_lesson.loadAllLesson();
						assignment.$forceUpdate();
					}
				});
			},
			setDefaultLesson:function(id){
				for(var i=0;i<this.lessons.length;i++){
					if(this.lessons[i].id==id){
						this.currentLesson=i;
						break;
					}
				}
			},
			loadAllLesson:function(){
				var url="/course/"+this.idCourse+"/lesson/get";
				var s=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						s.lessons=data;
						if(test.idLesson){
							s.setDefaultLesson(test.idLesson);
							s.lastUrl=test.url;
						}
					}
				})
			},
			getIcon:function(name){
				var ext=name.split(".");
				ext=ext[ext.length-1].toLowerCase();
				if(ext=='txt') return "<i class='fa fa-file-text-o' aria-hidden='true'></i>";
				else if(ext=='pdf') return "<i class='fa fa-file-pdf-o red' aria-hidden='true'></i>";
				else if(ext=='docx')  return "<i class='fa fa-file-word-o blue' aria-hidden='true'></i>";
				else if(ext=='mp4')  return "<i class='fa fa-file-video-o red' aria-hidden='true' ></i>";
				else if(ext=='png'||ext=='jpg'||ext=='gif')  return "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
				return  "<i class='fa fa-file' aria-hidden='true'></i>";
			},
			fix:function(){
				var find = '<br/>';
				var re = new RegExp(find, 'g');
				this.assignment.content=this.assignment.content.replace(re,'\n');
				this.setDefaultLesson(this.assignment.idLesson);
				var t=new Date(this.assignment.timeEnd);
	             $("#ip-date").pickadate('picker').set('select',t);
	             $("#ip-time").pickatime('picker').set('select',t);
			},
		
			remove:function(e,i){
				this.assignment.files[i].isDelete=true;
				remove(e);
			},
			createFileInput:function(name){
				return "<div class='f'>"+
				"<i class='fa fa-file-text-o' aria-hidden='true'></i>"+
				"<span>"+name+"</span>"+
				"<i class='fa fa-times r' aria-hidden='true' onclick='remove(event)'></i>"+
				"</div>";
			},
			onChange:function(){
				var id="files";
				var f=$("#files");
				var name=f.val();
				var b=false;
				$('#listF [type=file]').each(function(index,input){
					if($(input).val()==name) b=true;
				});
				if(b) return;
				name=name.split("\\");
				name=name[name.length-1];
				f.after("<div id='temp'></div>");
				new File({parent:this}).$mount("#temp");
				var list=$("#listF");
				f.attr({"id":'','name':"files"});
				
				var d=this.createFileInput(name);
				var a=$(d).appendTo(list);
				$(a).append(f);
			},
			saveEdit:function(){
				var l=this.assignment;
				var fileDelete=[];
				for(var i=0;i<l.files.length;i++){
					if(l.files[i].isDelete) {
						fileDelete.push(l.files[i].id);
					}
				}
				if(this.lessons.length==0)return;
				if(checkEmpty(this.assignment.title)){
					alert("Title is empty!");
					return;
				}
				if(checkEmpty(this.assignment.content)){
					alert("Content is empty!");
					return;
				}
				if(this.dateEnd==0) {
					return;
				}
				if(this.assignment.timeEnd<=new Date().getTime()) {
					alert("time end is not correct!");
					return;
				}
				this.assignment.idLesson=this.lessons[this.currentLesson].id
				var form = document.getElementById("formFile");
				var formData = new FormData(form);
				var content=this.assignment.content.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				formData.append('title',this.assignment.title);
				formData.append('content',content);
				formData.append('idLesson',this.assignment.idLesson);
				formData.append('timeEnd',this.assignment.timeEnd);
				formData.append('groupSubmit',this.assignment.groupSubmit);
				
				formData.append('fileDelete',fileDelete);
				var url="/course/"+this.idCourse+"/assignment/"+l.id+"/update";
				var u="/course/"+this.idCourse+"/assignments/";
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						location.href=u;
					},
					error:function(){
						alert("error send data");
					}
				});
			},
			
		}
	});
	assignment.idCourse=test.idCourse;
	assignment.course=test.course;
	assignment.loadAllLesson();
	if(test.isEdit){
		assignment.assignment=test.assignment;
		assignment.isEdit=true;
		assignment.fix();
	}
});
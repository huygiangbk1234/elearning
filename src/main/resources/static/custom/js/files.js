window.addEventListener("load",function(){
	var files=new Vue({
		el:'#files',
		data:{
			fileLessons:[],
			files:[],
		},
		methods:{
			getIcon:function(name){
				var ext=name.split(".");
				ext=ext[ext.length-1].toLowerCase();
				if(ext=='txt') return "<i class='fa fa-file-text-o' aria-hidden='true'></i>";
				else if(ext=='pdf') return "<i class='fa fa-file-pdf-o red' aria-hidden='true'></i>";
				else if(ext=='docx')  return "<i class='fa fa-file-word-o blue' aria-hidden='true'></i>";
				else if(ext=='mp4')  return "<i class='fa fa-file-video-o red' aria-hidden='true' ></i>";
				else if(ext=='png'||ext=='jpg'||ext=='gif')  return "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
				return  "<i class='fa fa-file' aria-hidden='true'></i>";
			},
		}
	});
	
	files.fileLessons=test.fileLessons;
	files.files=test.files;
});
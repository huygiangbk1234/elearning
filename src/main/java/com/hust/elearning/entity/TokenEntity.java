package com.hust.elearning.entity;
import java.util.Calendar;
import java.util.Date;

import com.hust.elearning.util.constants.Constants;



public class TokenEntity {

	private Long id;

	private long expiryDate;

	private String token;

	private int idAccount;
	
	public TokenEntity(){
		
	}
	
	public TokenEntity(final String token) {
        super();
        this.token = token;
        this.expiryDate = calculateExpiryDate(Constants.TOKEN_EXPIRATION);
    }
	public TokenEntity(final String token,final int id) {
        super();
        this.token = token;
        this.expiryDate = calculateExpiryDate(Constants.TOKEN_EXPIRATION);
        this.idAccount=id;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(long expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}

	/** Calculate Expire Date
	 * @param expiryTimeInMinutes 
	 * @return Date
	 */
	private long calculateExpiryDate(final int expiryTimeInMinutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return cal.getTime().getTime();
    }
	public boolean checkDate() {
		Date c=new Date();
		if(this.expiryDate<c.getTime()) {
			return false;
		}
		return true;
	}
//
//    /** Override hashCode
//	 * @param expiryTimeInMinutes 
//	 * @return Date
//	 */
//    @Override
//    public int hashCode() {
//        final int prime = 31;
//        int result = 1;
//        result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
//        result = prime * result + ((token == null) ? 0 : token.hashCode());
//        return result;
//    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Token [String=").append(token).append("]").append("[Expires").append(expiryDate).append("]");
        return builder.toString();
    }
    
}

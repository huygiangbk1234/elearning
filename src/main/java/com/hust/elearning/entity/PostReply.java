package com.hust.elearning.entity;

import java.util.List;

public class PostReply {

	private int id;

	private int idPost;
	
	private String content;

	private int status;
	
	private CourseMember creator;
	
	private int parent;

	private List<Document> files;
	
	private long createdAt;
	
	private List<PostReply> comments;
	

	public List<PostReply> getComments() {
		return comments;
	}

	public void setComments(List<PostReply> comments) {
		this.comments = comments;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public CourseMember getCreator() {
		return creator;
	}

	public void setCreator(CourseMember creator) {
		this.creator = creator;
	}

	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public List<Document> getFiles() {
		return files;
	}

	public void setFiles(List<Document> files) {
		this.files = files;
	}

	public CourseMember getMember() {
		return creator;
	}

	public void setMember(CourseMember member) {
		this.creator = member;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	

}

package com.hust.elearning.entity;

public class ConversationReply {

	private int id;

	private String content;

	private boolean isRead;

	private int idCreator;

	private long time;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public int getIdCreator() {
		return idCreator;
	}

	public void setIdCreator(int idCreator) {
		this.idCreator = idCreator;
	}

	public long getTimeRead() {
		return time;
	}

	public void setTimeRead(long timeRead) {
		this.time = timeRead;
	}
	
	

}

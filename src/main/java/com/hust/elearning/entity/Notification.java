package com.hust.elearning.entity;

public class Notification {
	private int id;
	private CourseMember creator;
	private String parent;
	private long createdAt;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public CourseMember getCreator() {
		return creator;
	}
	public void setCreator(CourseMember creator) {
		this.creator = creator;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
}

package com.hust.elearning.entity;



public class Account {

	private int id;

	private String password;

	private String email;

	private int status;

	private Long createdAt;
	
	private Long updatedAt;

	private String fullname;

	private String nickname;

	private String avatar;

	private String tel;
	
	private String role;
	
	private String lastRole;
	
	private int lastCourse;
	
	public Account() {this.role="user";}
	
	public Account(int id) {this.id=id;}

	public Account(int id,String email,String pwd,Long createdAt,Long updatedAt,int status,String fullname,String nickname,String avatar,String tel,String role,String lastRole,int lastCourse) {
		this.id=id;
		this.email=email;
		this.password="";
		this.createdAt=createdAt;
		this.updatedAt=updatedAt;
		this.status=status;
		this.fullname=fullname;
		this.nickname=nickname;
		this.avatar=avatar;
		this.tel=tel;
		this.role=role;
		this.lastRole=lastRole;
		this.lastCourse=lastCourse;
	}
	public Account(String email,String p) {
		this.role="user";
		this.email=email;
		this.password=p;
	}
	
	public String getLastRole() {
		return lastRole;
	}

	public void setLastRole(String lastRole) {
		this.lastRole = lastRole;
	}

	public int getLastCourse() {
		return lastCourse;
	}

	public void setLastCourse(int lastCourse) {
		this.lastCourse = lastCourse;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public void setStatus(int status) {
		this.status = status;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}
	public int getStatus() {
		return status;
	}
	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String toString() {
		return this.id+" "+this.email +" "+this.password+" "+this.createdAt;
	}
	
}

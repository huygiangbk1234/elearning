package com.hust.elearning.entity;

import java.util.List;

public class GroupCourse {
	private int id;
	private String name;
	private long createdAt;
	private int idCourse;
	private List<CourseMember> members;
	
	public List<CourseMember> getMembers() {
		return members;
	}
	public void setMembers(List<CourseMember> members) {
		this.members = members;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public int getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	
}

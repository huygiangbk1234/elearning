package com.hust.elearning.entity;

import java.util.List;

public class AssignmentSubmit {
	private int id;
	private int idAssignment;
	private CourseMember member;
	private List<Document> files;
	private int root;
	private long createdAt;
	private long updatedAt;
	private GroupCourse group;
	
	public GroupCourse getGroup() {
		return group;
	}
	public void setGroup(GroupCourse group) {
		this.group = group;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public long getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public List<Document> getFiles() {
		return files;
	}
	public void setFiles(List<Document> files) {
		this.files = files;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdAssignment() {
		return idAssignment;
	}
	public void setIdAssignment(int idAssignment) {
		this.idAssignment = idAssignment;
	}
	
	public CourseMember getMember() {
		return member;
	}
	public void setMember(CourseMember member) {
		this.member = member;
	}
	public int getRoot() {
		return root;
	}
	public void setRoot(int root) {
		this.root = root;
	}
	
	
}

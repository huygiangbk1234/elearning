package com.hust.elearning.entity;

import java.util.List;

public class Post {

	private int id;

	private String title;
	
	private int idCourse;

	private int status;

	private String content;

	private CourseMember creator;
	
	private long createdAt;

	private List<PostReply> comments;
	
	private List<Document> files;
	
	private int priority;
	

	public int getIdCourse() {
		return idCourse;
	}

	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public List<Document> getFiles() {
		return files;
	}

	public void setFiles(List<Document> files) {
		this.files = files;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public CourseMember getCreator() {
		return creator;
	}

	public void setCreator(CourseMember creator) {
		this.creator = creator;
	}

	public List<PostReply> getComments() {
		return comments;
	}

	public void setComments(List<PostReply> comments) {
		this.comments = comments;
	}
	
	

}

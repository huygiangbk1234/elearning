package com.hust.elearning.entity;

public class Message {
	private int id;
	private String content;
	private int idAccount;
	private int idGroup;
	private long createdAt;
	
	public Message() {}

	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}
	public int getIdAccount() {
		return idAccount;
	}


	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}


	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Message(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
}

package com.hust.elearning.entity;

import java.util.List;

public class Lesson {
	private int id;
	private String name;
	private int idCourse;
	private int pos;
	private List<Document> files;
	private List<Document> videos;
	private List<Assignment> assignments;
	
	
	public List<Document> getVideos() {
		return videos;
	}
	public void setVideos(List<Document> videos) {
		this.videos = videos;
	}
	public List<Document> getFiles() {
		return files;
	}
	public void setFiles(List<Document> files) {
		this.files = files;
	}
	public List<Assignment> getAssignments() {
		return assignments;
	}
	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int index) {
		this.pos = index;
	}
	
	
}

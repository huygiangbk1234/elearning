package com.hust.elearning.entity;

public class GroupChatMember {
	private int id;
	private int idGroup;
	private int idAccount;
	private long updatedAt;
	private boolean isHide;
	private boolean isRead;
	 
	public GroupChatMember(int id, int idGroup, int idAccount, long updatedAt) {
		super();
		this.id = id;
		this.idGroup = idGroup;
		this.idAccount = idAccount;
		this.updatedAt = updatedAt;
		this.isHide=false;
		this.isRead=false;
	}
	public GroupChatMember(int idGroup, int idAccount, long updatedAt) {
		super();
		this.idGroup = idGroup;
		this.idAccount = idAccount;
		this.updatedAt = updatedAt;
		this.isHide=false;
		this.isRead=false;
	}
	
	public boolean isRead() {
		return isRead;
	}
	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}
	public long getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(long createdAt) {
		this.updatedAt = createdAt;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isHide() {
		return isHide;
	}
	public void setHide(boolean isHide) {
		this.isHide = isHide;
	}
	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}
	public int getIdGroup() {
		return idGroup;
	}
	public void setGroup(int group) {
		this.idGroup = group;
	}
	public int getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}
	
	
}

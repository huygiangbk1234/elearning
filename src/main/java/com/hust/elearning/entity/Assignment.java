package com.hust.elearning.entity;

import java.util.List;

public class Assignment {
	
	private int id;
	private int idCourse;
	private String title;
	private String content;
	private long createdAt;
	private long timeEnd;
	private int idLesson;
	private List<Document> files;
	private boolean groupSubmit;
	
	public int getIdLesson() {
		return idLesson;
	}
	public List<Document> getFiles() {
		return files;
	}
	public void setFiles(List<Document> files) {
		this.files = files;
	}
	public void setIdLesson(int idLesson) {
		this.idLesson = idLesson;
	}
	public boolean isGroupSubmit() {
		return groupSubmit;
	}
	public void setGroupSubmit(boolean groupSubmit) {
		this.groupSubmit = groupSubmit;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public long getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(long timeEnd) {
		this.timeEnd = timeEnd;
	}
	
	
}

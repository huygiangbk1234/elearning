package com.hust.elearning.entity;

public class CourseMember {

	private int id;

	private Account member;

	private String role;

	private String alias;

	private int status;
	
	private int idCourse;
	
	private int idGroup;
	
	public CourseMember() {}
	
	public CourseMember(int id) {this.id= id;}
	
	public CourseMember(Account a,int idCourse) {
		this.member=a;
		this.idCourse=idCourse;
	}

	
	public CourseMember(int id, String role, String alias,int status, int idCourse,int idAccount) {
		super();
		this.id = id;
		this.member = new Account(idAccount);
		this.role = role;
		this.alias = alias;
		this.status = status;
		this.idCourse = idCourse;
	}
	
	public CourseMember(int id, String role, String alias,int status, int idCourse,int idAccount,String email,String fullname,String nickname,String avatar) {
		this.id = id;
		this.member = new Account(idAccount);
		this.member.setAvatar(avatar);
		this.member.setEmail(email);
		this.member.setFullname(fullname);
		this.member.setNickname(nickname);
		this.role = role;
		this.alias = alias;
		this.status = status;
		this.idCourse = idCourse;
	}


	public int getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}

	public int getIdCourse() {
		return idCourse;
	}

	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Account getMember() {
		return member;
	}

	public void setMember(Account member) {
		this.member = member;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	

}

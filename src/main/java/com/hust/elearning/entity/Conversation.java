package com.hust.elearning.entity;

public class Conversation {

	private int id;

	private long time;

	private int status;
	
	private Account user1;
	
	private Account user2;

	public Account getUser1() {
		return user1;
	}

	public void setUser1(Account user1) {
		this.user1 = user1;
	}

	public Account getUser2() {
		return user2;
	}

	public void setUser2(Account user2) {
		this.user2 = user2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	

}

package com.hust.elearning.services;

import java.net.URISyntaxException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.hust.elearning.dto.MessageTemplateDto;
import com.hust.elearning.entity.Account;
import com.hust.elearning.util.constants.URL;


@Service
public class EmailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	Environment environment;

	private final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

	public void sendEmail(final MessageTemplateDto messageTemplate) {

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					
					MimeMessage mimeMessage = javaMailSender.createMimeMessage();
					MimeMessageHelper mailMsg = new MimeMessageHelper(mimeMessage, true);
				//	SimpleMailMessage mailMsg = new SimpleMailMessage(); 
					mailMsg.setTo(messageTemplate.getReceiver());
					mailMsg.setSubject(messageTemplate.getSubject());
					mailMsg.setText(messageTemplate.getContent());
					javaMailSender.send(mimeMessage);
				} catch (Exception e) {
					LOGGER.error(e.getMessage());
				}
			}
		}).start();

	}

	public void sendResetPassword(String receiver, String confirmationUrl) {
		MessageTemplateDto emailMessage = new MessageTemplateDto();

		String content = "Please click the link to reset password:\n" + confirmationUrl + " ";
		String subject = "Reset password Honegori";
		emailMessage.setContent(content);
		emailMessage.setReceiver(receiver);
		emailMessage.setSubject(subject);
		sendEmail(emailMessage);
	}
	
	public void sendResetEmail(String receiver, String confirmationUrl) {
		MessageTemplateDto emailMessage = new MessageTemplateDto();

		String content = "Please click the link to reset password:\n" + confirmationUrl + " ";
		String subject = "Reset password Honegori";
		emailMessage.setContent(content);
		emailMessage.setReceiver(receiver);
		emailMessage.setSubject(subject);
		sendEmail(emailMessage);
	}

	public void sendRegisterEmail(String receiver, String confirmationUrl) {
		MessageTemplateDto emailMessage = new MessageTemplateDto();

		String content = "Please click the link to confirm your registration.:\n" + confirmationUrl + " ";
		String subject = "Register Confirm";
		emailMessage.setContent(content);
		emailMessage.setReceiver(receiver);
		emailMessage.setSubject(subject);
		sendEmail(emailMessage);
	}
	public void sendConfirmEmail(String receiver, String confirmationUrl) {
		MessageTemplateDto emailMessage = new MessageTemplateDto();

		String content = "Please click the link to confirm your Change Email.:\n" + confirmationUrl + " ";
		String subject = "Change Email Confirm";
		emailMessage.setContent(content);
		emailMessage.setReceiver(receiver);
		emailMessage.setSubject(subject);
		sendEmail(emailMessage);
	}
	
	
	public void sendRegisterEmail(Account a, String validationToken, String serverAddress, int serverPort){
		String baseUrl = "http://" + serverAddress + ":" + serverPort + URL.verify_account;
		String uri = null;
		try {
			URIBuilder uriBuilder = new URIBuilder(baseUrl);
			uriBuilder.addParameter("token", validationToken);
			uri = uriBuilder.toString();
		} catch (URISyntaxException e) {
			// TODO: handle exception
		}
		this.sendRegisterEmail(a.getEmail(), uri);
	}

}
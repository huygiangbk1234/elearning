package com.hust.elearning.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.hust.elearning.entity.Assignment;
import com.hust.elearning.entity.AssignmentSubmit;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Document;
import com.hust.elearning.entity.GroupCourse;
import com.hust.elearning.entity.Lesson;
import com.hust.elearning.entity.Post;
import com.hust.elearning.entity.ViewVideo;
import com.hust.elearning.repository.AssignmentRepository;
import com.hust.elearning.repository.CourseMemberRepository;
import com.hust.elearning.repository.CourseRepository;
import com.hust.elearning.repository.DocumentRepository;
import com.hust.elearning.repository.GroupCourseRepository;
import com.hust.elearning.repository.LessonRepository;
import com.hust.elearning.repository.PostRepository;
import com.hust.elearning.util.DateUtils;
import com.hust.elearning.util.constants.Constants;

@Service
public class CourseService {
	
	@Autowired
	CourseRepository courseRepository;
	@Autowired
	CourseMemberRepository courseMemberRepository;
	@Autowired
    private MessageSource messageSource;
	@Autowired
	private LessonRepository lessonRepository;
	@Autowired
	private AssignmentRepository assignmentRepository;
	@Autowired
	private DocumentRepository documentRepository;
	@Autowired
	private GroupCourseRepository groupCourseRepository;
	@Autowired
	private PostRepository postRepository;
	
	public String checkCourse(CourseMember m,Model model) {
		if(m==null) {
			model.addAttribute("error",messageSource.getMessage("course.error.notpermit", null, LocaleContextHolder.getLocale()));
			return "error/error_course";
		}
		if(m.getStatus()==0) {
			model.addAttribute("error",messageSource.getMessage("course.error.lockmember", null, LocaleContextHolder.getLocale()));
			return "error/error_course";
		}
		return "";
	}
	public void createMember(CourseMember c) {
		courseMemberRepository.createMember(c);
	}
	public void createCourse(Course c) {
		courseRepository.createCourse(c);
	}
	
	public Course getCourseById(int id) {
		return courseRepository.getCourseById(id);
	}
	
	public List<Course> getAllCourseUser(int idUser) {
		return courseMemberRepository.countAllCourseOfUser(idUser);
	}
	
	public CourseMember getMember(int idUser, int idCourse) {
		return courseMemberRepository.getMember(idUser,idCourse);
	}
	public List<CourseMember> getMemberLikeName(int idCourse,String name) {
		return courseMemberRepository.getMemberLikeName(idCourse,"%"+name+"%");
	}
	
	public int countTotalMember(int idCourse) {
		return courseMemberRepository.countAllMemberOfCourse(idCourse);
	}
	public List<CourseMember> getAllMember(int idCourse){
		return courseMemberRepository.getAllMemberOfCourse(idCourse);
	}
	public List<CourseMember> getSomeMember(int idCourse,int from,int limit) {
		return courseMemberRepository.getSomeMember(idCourse,from,limit);
	}
	public void deleteMember(int idMember) {
		courseMemberRepository.deleteMember(idMember);
	}

	public List<Lesson> getAllLesson(int idCourse){
		List<Lesson> list= lessonRepository.getLessonOfCourse(idCourse);
		for(Lesson l :list) {
			int id=l.getId();
			l.setAssignments(assignmentRepository.getAssignmentOfLesson(id));
			l.setFiles(documentRepository.getDocumentInParent(Constants.PRE_LESSON+id));
			l.setVideos(documentRepository.getVideoInParent(Constants.PRE_LESSON+id));
		}
		return list;
	}
	public void createLession(Lesson l) {
		lessonRepository.createLesson(l);
	}
	public void updateIndexLesson(int idLesson, int pos) {
		lessonRepository.updateIndexLesson(idLesson, pos);
	}
	public void updateNameLesson(int id,String name) {
		lessonRepository.updateNameLesson(id, name);
	}
	
	
	
	public List<Document> createDocument(HttpServletRequest request,int idCourse,int idAccount,String parent) {
		List<Document> list=new ArrayList<>();
		List<Part> fileParts = null;
		try {
			fileParts = request.getParts().stream().filter(part -> "files".equals(part.getName())).collect(Collectors.toList());
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ServletException e1) {
			e1.printStackTrace();
		}
		long time=DateUtils.getTime();
	    for (Part filePart : fileParts) {
	    	Document d=new Document();
			d.setCreatedAt(time);
			d.setIdCourse(idCourse);
			d.setIdCreator(idAccount);
			d.setStatus(1);
			d.setPrivate(false);
			d.setParent(parent);
	        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
	        d.setRawName(fileName);
	        String ext=fileName.substring(fileName.lastIndexOf(".") + 1);
	        fileName=DateUtils.randomName(ext);
	        ext=ext.toUpperCase();
	        if(ext.equals("MP4")) d.setType(2);
	        else d.setType(1);
	        d.setName(fileName);
	        InputStream fileContent = null;
			try {
				fileContent = filePart.getInputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
	        File targetFile = new File(Constants.PATH_FILE_UPLOAD+fileName);
	        try {
				java.nio.file.Files.copy(
						fileContent, 
				  targetFile.toPath(), 
				  StandardCopyOption.REPLACE_EXISTING);
				list.add(d);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     
	        IOUtils.closeQuietly(fileContent);
	    }
	    return list;
	}
	public void createAssignment(Assignment assignment) {
		assignmentRepository.createAssignment(assignment);
	}
	public List<Assignment> getAllAssignment(int idCourse) {
		List<Assignment> list= assignmentRepository.getAssignmentOfCourse(idCourse);
		return list;
	}
	public Assignment getAssignment(int idAssignment,boolean isFull) {
		Assignment a= assignmentRepository.getAssignmentById(idAssignment);
		if(isFull)a.setFiles(documentRepository.getDocumentInParent(Constants.PRE_ASSIGNMENT+a.getId()));
		return a;
	}
	public void deleteAssignment(int idAssignment) {
		assignmentRepository.deleteAssignment(idAssignment);
	}
	public void updateAssignment(Assignment assignment) {
		assignmentRepository.updateAssignment(assignment);
	}
	public List<AssignmentSubmit> getSubmitOfAssignment(int idAssignment) {
		return assignmentRepository.getUserSubmitAssignment(idAssignment);
	}
	public List<CourseMember> getUserNotSubmitAssignment(int idAssignment,int idCourse) {
		return assignmentRepository.getUserNotSubmitAssignment(idAssignment,idCourse);
	}
	public List<GroupCourse> getAllGroup(int idCourse) {
		return groupCourseRepository.getGroupCourseOfCourse(idCourse);
	}
	public void createGroup(GroupCourse g) {
		groupCourseRepository.createGroupCourse(g);
	}
	public void updateGroup(int idGroup, String name) {
		groupCourseRepository.updateNameGroup(idGroup, name);
	}
	public void changeMemberGroup(int idGroup, int id) {
		courseMemberRepository.joinGroup(id,idGroup);
	}
	public AssignmentSubmit getDefaultSubmitAssignmentOfUser(int idAssignment, int id) {
		return assignmentRepository.getAssignmentSubmitOfUser(idAssignment, id);
	}
	public AssignmentSubmit getSubmitAssignmentOfUser(int idAssignment, int id) {
		AssignmentSubmit a=assignmentRepository.getAssignmentSubmitOfUser(idAssignment, id);
		if(a!=null) {
			if(a.getRoot()!=0) {
				a=assignmentRepository.getAssignmentSubmitById(a.getRoot());
			}
		}
		return a;
	}
	public GroupCourse getGroupCourseById(int idGroup) {
		GroupCourse g= groupCourseRepository.getGroupCourseById(idGroup);
		return g;
	}
	public void submitAssignment(AssignmentSubmit as) {
		assignmentRepository.createAssignmentSubmit(as);
	}
	public void updateAssignmentSubmit(int id, long time) {
		assignmentRepository.updateAssignmentSubmit(id, time);
	}
	public List<Document> getAllFileLessonsOfCourse(int idCourse) {
		List<Document> list=new ArrayList<>();
		list.addAll(documentRepository.getDocumentLikeParent(idCourse,Constants.PRE_LESSON+"%"));
		return list;
	}
	public List<Document> getAllFileOthersOfCourse(int idCourse, int idMember) {
		List<Document> list=new ArrayList<>();
		list.addAll(documentRepository.getDocumentLikeParent(idCourse,Constants.PRE_ASSIGNMENT+"%"));
		list.addAll(documentRepository.getDocumentLikeParent(idCourse,Constants.PRE_POST+"%"));
		list.addAll(documentRepository.getDocumentLikeParent(idCourse,Constants.PRE_POST_REPLY+"%"));
		for(Assignment a:assignmentRepository.getAssignmentOfCourse(idCourse)) {
			for(AssignmentSubmit as : assignmentRepository.getAssignmentSubmitOfUserInCourse(a.getId(), idMember)) {
				list.addAll(documentRepository.getDocumentInParent(Constants.PRE_ASSIGNMENT_SUBMIT+as.getId()));
			}
		}
		return list;
	}
	public void deleteGroupCourse(int idGroup) {
		courseMemberRepository.deleteGroup(idGroup);
		groupCourseRepository.deleteGroupCourse(idGroup);
	}
	public Document getVideo(int idVideo) {
		return documentRepository.getVideoById(idVideo);
	}
	public List<Document> getAllVideosOfCourse(int idCourse) {
		return documentRepository.getVideoOfCourse(idCourse);
	}
	public void updateViewVideoLength(int idViewVideo, String length) {
		documentRepository.updateLengthViewVideo(idViewVideo, length);
	}
	public void updateViewVideoAnswers(int idViewVideo, String answers) {
		documentRepository.updateAnswersViewVideo(idViewVideo, answers);
	}
	public ViewVideo getViewVideoMember(int idVideo, int idMember) {
		return documentRepository.getViewVideoOfUser(idVideo, idMember);
	}
	public void createViewVideo(ViewVideo v) {
		documentRepository.createViewVideo(v);
	}
	public int countAllPostPrOfUser(int idMember) {
		int n=0;
		Object obj=postRepository.countCommentOfUser(idMember);
		if(obj!=null) n+=Integer.parseInt(obj.toString());
		obj=postRepository.countPostOfUser(idMember);
		if(obj!=null) n+=Integer.parseInt(obj.toString());
		return n;
	}
	public List<Document> getAllFilesOfUser(int id) {
		return documentRepository.getDocumentOfUser(id);
	}
	public List<Document> getAllVideosOfUser(int id) {
		return documentRepository.getVideoOfUser(id);
	}
	public void updateAlias(int idMember, String alias) {
		courseMemberRepository.updateAlias(idMember,alias);
	}
	public List<Assignment> getAssignmentNotSubmit(int idCourse) {
		return assignmentRepository.getAssignmentAfterTime(idCourse, DateUtils.getTime());
	}
	public List<Assignment> getAssignmentAfterTime(int id, long time1, long time2) {
		return assignmentRepository.getAssignmentBetweenTime(id,time1, time2);
	}
	public void updateCourse(Course c) {
		courseRepository.updateCourse(c);
	}
	public List<CourseMember> filterMemberByAlias(String alias) {
		return courseMemberRepository.filterMemberByAlias(alias+"%");
	}
}

package com.hust.elearning.services;

import org.mozilla.universalchardet.UniversalDetector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hust.elearning.entity.Document;
import com.hust.elearning.repository.DocumentRepository;

@Service
public class UploadServices {
	@Autowired
	DocumentRepository uploadFileRepsitory;
	
	public static String guessEncoding(byte[] bytes) {
	    String DEFAULT_ENCODING = "UTF-8";
	    UniversalDetector detector =
	        new UniversalDetector(null);
	    detector.handleData(bytes, 0, bytes.length);
	    detector.dataEnd();
	    String encoding = detector.getDetectedCharset();
	    detector.reset();
	    if (encoding == null) {
	        encoding = DEFAULT_ENCODING;
	    }
	    return encoding;
	}
	public void savePDFFile(Document document){
		uploadFileRepsitory.savePDFFile(document);
	}
	public int countNumsFile(int id) {
		Object n= uploadFileRepsitory.countDocumentUser(id);
		return (n==null)?0:Integer.parseInt(n.toString());
	}
}	

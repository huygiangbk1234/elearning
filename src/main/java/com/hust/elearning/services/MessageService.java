package com.hust.elearning.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.hust.elearning.dto.MessageAjax;
import com.hust.elearning.dto.MessageUpdateAjax;
import com.hust.elearning.dto.NotificationAjax;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.GroupChat;
import com.hust.elearning.entity.GroupChatMember;
import com.hust.elearning.entity.Message;
import com.hust.elearning.entity.MessageRecipient;
import com.hust.elearning.repository.GroupChatRepository;
import com.hust.elearning.repository.MessageRepository;
import com.hust.elearning.util.DateUtils;

@Service
public class MessageService {
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	@Autowired
	private GroupChatRepository groupRepository;
	@Autowired
	private MessageRepository messageRepository;
	
	public GroupChat getGroupChat(int idGroup) {
		return groupRepository.getGroupById(idGroup);
	}
	
	public int updateGroupChatMember(GroupChatMember gm) {
		return groupRepository.updateGroupChatMember(gm);
	}
	
	public int updateReadMessage(int idGroup,int idUser,long time) {
		groupRepository.updateMemberGroupRead(true, idGroup, idUser);
		return messageRepository.updateReadMessage(idGroup, idUser,time);
	}
	
	public List<Message> getLastMessageOfGroup(int idGroup) {
		return messageRepository.getSomeMessage(idGroup, 0, 15);
	}
	
	public int saveMessage(Message m) {
		 messageRepository.createMessage(m);
		 return m.getId();
	}
	public List<Account> getMemberGroup(int idGroup){
		return groupRepository.getMemberGroup(idGroup);
	}
	public void saveMessageRecipient(MessageRecipient mr) {
		messageRepository.createMessageRecipient(mr);
	}
	public void sendMessageRealTime(MessageAjax data, int idGroup,int idMessage,long time) {
		List<Account> l=this.getMemberGroup(idGroup);
		MessageRecipient mr=new MessageRecipient();
		mr.setUpdatedAt(time);
		mr.setIdMessage(idMessage);
		data.setIdMessage(idMessage);
		groupRepository.updateTimeGroup(time, idGroup);
		Map<String,Object> map =  new HashMap<String,Object>();
		map.put("type", "message");
		
		for(Account a : l) {
			if(a.getId()==data.getFrom()) {
				messagingTemplate.convertAndSendToUser(a.getEmail(), "/queue/notify", data,map);
				continue;
			}
			mr.setIdAccount(a.getId());
			groupRepository.updateMemberGroupRead(false, idGroup, a.getId());
			this.saveMessageRecipient(mr);
			messagingTemplate.convertAndSendToUser(a.getEmail(), "/queue/notify", data,map);
		}
	}
	public void sendNotificationRealTime(NotificationAjax data, String email) {
		Map<String,Object> map =  new HashMap<String,Object>();
		map.put("type", "notification");
		messagingTemplate.convertAndSendToUser(email, "/queue/notify", data,map);
	}
	public int isExistGroupChatPrivate(int idUser1,int idUser2) {
		GroupChat gr=groupRepository.getExistGroupPrivate(idUser1, idUser2);
		if(gr!=null) {
			groupRepository.updateMemberGroupHide(false, gr.getId(), idUser1);
			return gr.getId();
		}
		return -1;
	}
	public int createNewGroupPrivate(int idUser1,int idUser2) {
		GroupChat g=new GroupChat("",true);
		long time=DateUtils.getTime();
		g.setCreatedAt(time);
		g.setUpdatedAt(time);
		groupRepository.createGroup(g);
		int id=g.getId();
		GroupChatMember gm=new GroupChatMember(id,idUser1,time);
		gm.setRead(true);
		groupRepository.createGroupMember(gm);
		gm.setRead(false);
		gm.setIdAccount(idUser2);
		groupRepository.createGroupMember(gm);
		return id;
	}
	public Boolean isReadGroup(int idUser,int idGroup) {
		return groupRepository.isReadGroup(idGroup, idUser);
	}
	public boolean isMemberOfGroup(int idUser,int idGroup) {
		if(groupRepository.isMemberOfGroup(idUser, idGroup)!=null) return true;
		return false;
	}

	public List<Message> getSomeMessage(int idGroup,int from,int limit){
		return messageRepository.getSomeMessage(idGroup, from, limit);
	}

	public List<GroupChat> getSomeGroupChat(int idUser,int from, int limit){
		return groupRepository.getSomeGroupChat(idUser,from,limit);
	}
}

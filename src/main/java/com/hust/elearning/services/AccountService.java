package com.hust.elearning.services;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.hust.elearning.config.auth.UserDetailServiceImpl;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.TokenEntity;
import com.hust.elearning.repository.AccountRepository;
import com.hust.elearning.repository.TokenRepository;

@Service
public class AccountService{
	@Autowired
	UserDetailServiceImpl userDetailServiceImpl;
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	TokenRepository tokenRepository;
	public String checkAccount(Model model,int status) {
		if(status==-1) {
			model.addAttribute("isNotVerify", true);
			return "user/user_warn";
		}else if(status==-2) {
			model.addAttribute("isNotProfile", true);
			return "user/user_warn";
		}else if(status==0) {
			model.addAttribute("isNotActived", true);
			return "user/user_warn";
		}
		return null;
	}
	public Account getUserByName(String name) {return null;};
	
	public Account getUserByEmail(String email) {
		return accountRepository.getUserByEmail(email);
	}
	public void test() {accountRepository.test();}

	public void updateAccount(Account account){
		Calendar cal = Calendar.getInstance();
		account.setUpdatedAt(cal.getTimeInMillis());
		accountRepository.updateAccount(account);
	}
	public void createAccount(Account a,TokenEntity token) {
		this.accountRepository.createAccount(a);
		token.setIdAccount(a.getId());
		this.tokenRepository.save(token);
	}
	public Account getUserById(int id) {
		return accountRepository.getUserById(id);
	}
	public TokenEntity getTokenConfirmRegister(String token) {
		return tokenRepository.findByToken(token);
	}
	 @Resource
	  private ProviderManager authenticationManager;
	public void autoLogin(Account a,HttpServletRequest request,String pass) {
		String email = a.getEmail();
        try {
        	UserDetails  user=userDetailServiceImpl.loadUserByUsername(email);
        	 UsernamePasswordAuthenticationToken t = new UsernamePasswordAuthenticationToken(user,pass);
        	 Authentication authentication = authenticationManager.authenticate(t);
        	 
         	SecurityContextHolder.getContext().setAuthentication(authentication);
        }catch(Exception e) {
        	 e.printStackTrace();
        }
	}

	public List<Account> filterUserByEmail(String email) {
		return accountRepository.filterUserByEmail(email+"%");
	}

	public void updateLastCourse(int id, int idCourse) {
		accountRepository.updateLastCourse(id,idCourse);
	}

	public void deleteToken(long id) {
		tokenRepository.deleteByToken(id);
	}
}

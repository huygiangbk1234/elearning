package com.hust.elearning.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.hust.elearning.dto.NotificationAjax;
import com.hust.elearning.entity.Assignment;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Message;
import com.hust.elearning.entity.Notification;
import com.hust.elearning.entity.NotificationRecipient;
import com.hust.elearning.entity.Post;
import com.hust.elearning.entity.PostReply;
import com.hust.elearning.repository.NotificationRepository;
import com.hust.elearning.util.constants.Constants;


@Service
public class NotificationService {
	 @Autowired
	 private SimpMessagingTemplate messagingTemplate;
	 @Autowired
	 private NotificationRepository notificationRepository;
	 @Autowired
	 private MessageSource messageSource;
	 @Autowired
	 private PostService postService;
	 @Autowired
	 private CourseService courseService;
	 public void notisfy(Message notification, String username) {
	    messagingTemplate.convertAndSendToUser(
	      username, 
	      "/queue/notify", 
	      notification
	    );
	  }
	/*  @Scheduled(fixedRate=3600000)
	  public void processNewDBData() {
	     System.out.println("a");
	  }*/
	public List<NotificationAjax> getNotification(int idAccount, int from) {
		List<NotificationRecipient> list= notificationRepository.getNotificationOfUser(idAccount, from);
		List<NotificationAjax> la=new ArrayList<>();
		for(NotificationRecipient nr:list) {
			Notification n =nr.getNotification();
			NotificationAjax na=null;
			String parent=n.getParent();
			CourseMember cm=n.getCreator();
			
			boolean isRead=false;
			isRead=nr.getCreatedAt()<DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH).getTime();
			if(parent.indexOf(Constants.PRE_POST)!=-1) {
				int idPost=Integer.parseInt(parent.substring(Constants.PRE_POST.length()));
				na=createNotificationPost(idPost, cm, isRead);
			}else if(parent.indexOf(Constants.PRE_POST_REPLY)!=-1) {
				int idPostReply=Integer.parseInt(parent.substring(Constants.PRE_POST_REPLY.length()));
				na=createNotificationPostReply(idPostReply, cm, isRead);
			}else if(parent.indexOf(Constants.PRE_ASSIGNMENT)!=-1) {
				int idAssignment=Integer.parseInt(parent.substring(Constants.PRE_ASSIGNMENT.length()));
				na=createNotificationAssingment(idAssignment, cm, isRead);
			}
			if(na!=null) la.add(na);
		}
		return la;
	}    
	public NotificationAjax createNotificationPost(int idPost,CourseMember cm,boolean isRead) {
		NotificationAjax na=new NotificationAjax();
		Post p=postService.getPostById(idPost);
		if(p==null) return null;
		Course c=courseService.getCourseById(p.getIdCourse());
		if(c==null) return null;
		String content="<b>"+cm.getAlias()+"</b> "+messageSource.getMessage("pn_notification.notification.post", null, LocaleContextHolder.getLocale())+
				" "+c.getName();
		String link="/course/"+c.getId()+"/forum/"+p.getId();
		na.setContent(content);
		na.setLink(link);
		na.setRead(isRead);
		return na;
	}
	public NotificationAjax createNotificationPostReply(int idPostReply,CourseMember cm,boolean isRead) {
		NotificationAjax na=new NotificationAjax();
		PostReply pr=postService.getPostReplyById(idPostReply);
		if(pr==null) return null;
		Post p=postService.getPostById(pr.getIdPost());
		if(p==null) return null;
		Course c=courseService.getCourseById(p.getIdCourse());
		if(c==null) return null;
		String content="";
		String link="";
		if(pr.getParent()==-1) {
			content="<b>"+cm.getAlias()+"</b> "+messageSource.getMessage("pn_notification.notification.post_reply1", null, LocaleContextHolder.getLocale())+
					" "+c.getName();
			link="/course/"+c.getId()+"/forum/"+p.getId()+"?idPost="+idPostReply;
		}else {
			content="<b>"+cm.getAlias()+"</b> "+messageSource.getMessage("pn_notification.notification.post_reply2", null, LocaleContextHolder.getLocale())+
					" "+c.getName();
			link="/course/"+c.getId()+"/forum/"+p.getId()+"?idPost="+pr.getParent()+"&idComment="+idPostReply;
		}
		na.setContent(content);
		na.setLink(link);
		na.setRead(isRead);
		return na;
	}
	public NotificationAjax createNotificationAssingment(int idAssignment,CourseMember cm,boolean isRead) {
		NotificationAjax na=new NotificationAjax();
		Assignment a=courseService.getAssignment(idAssignment, false);
		if(a==null) return null;
		Course c=courseService.getCourseById(a.getIdCourse());
		if(c==null) return null;
		String content=messageSource.getMessage("pn_notification.notification.assignment", null, LocaleContextHolder.getLocale())+
				" "+c.getName();
		String link="/course/"+c.getId()+"/assignment/"+a.getId();
		na.setContent(content);
		na.setLink(link);
		na.setRead(isRead);
		return na;
	}
	public void createNotification(Notification n) {
		notificationRepository.createNotification(n);
	}
	public void createNotificationRecipient(NotificationRecipient nr) {
		notificationRepository.createNotificationRecipient(nr);
	}
	
}

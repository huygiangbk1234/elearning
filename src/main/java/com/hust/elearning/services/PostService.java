package com.hust.elearning.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hust.elearning.repository.PostRepository;
import com.hust.elearning.util.constants.Constants;
import com.hust.elearning.entity.Document;
import com.hust.elearning.entity.Post;
import com.hust.elearning.entity.PostReply;
import com.hust.elearning.repository.DocumentRepository;

@Service
public class PostService {
	
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private DocumentRepository documentRepository;
	public List<Post> getAllPost(int idCourse){
		return postRepository.getPostOfCourse(idCourse);
	}
	public void updatePostReply(PostReply pr) {
		postRepository.updatePostReply(pr);
	}
	public void updatePost(Post p) {
		postRepository.updatePost(p);
	}
	public void createPost(Post p) {
		postRepository.createPost(p);
	}
	public void createPostReply(PostReply p) {
		postRepository.createPostReply(p);
	}
	public void createListDocument(List<Document> list) {
		documentRepository.uploadListDocument(list);
	}
	
	public int countCommentPost(int idPost) {
		Object o=  postRepository.countCommentPost(idPost);
		return (o!=null)?Integer.parseInt(o.toString()):0;
	}
	public int countSubCommentPost(int idPostReply) {
		Object o= postRepository.countSubCommentPost(idPostReply);
		return (o!=null)?Integer.parseInt(o.toString()):0;
	}
	
	public Post getPostById(int idPost) {
		return postRepository.getPostById(idPost);
	}
	public List<PostReply> getPostReply(int idPost) {
		return postRepository.getPostReply(idPost);
	}
	public List<PostReply> getSubPostReply(int parent,int from) {
		return postRepository.getSubPostReply(parent,from);
	}
	
	public List<Document> getDocumentPost(int idPost){
		return documentRepository.getDocumentInParent(Constants.PRE_POST+idPost);
	}
	public List<Document> getDocumentPostReply(int idPostReply){
		return documentRepository.getDocumentInParent(Constants.PRE_POST_REPLY+idPostReply);
	}
	public void deleteFilePost(ArrayList<Integer> fileDelete) {
		documentRepository.deleteListFileById(fileDelete);
	}
	public void deletePost(int idPost) {
		postRepository.delete(idPost);
		documentRepository.deleteFileByParent(Constants.PRE_POST+idPost);
	}
	public void deleteComment(int idPostReply) {
		postRepository.deleteComment(idPostReply);
		documentRepository.deleteFileByParent(Constants.PRE_POST_REPLY+idPostReply);
	}
	public void pinPost(int idPost,int priority) {
		postRepository.pinPost(idPost,priority);
	}
	public PostReply getPostReplyById(int idPostReply) {
		return postRepository.getPostReplyById(idPostReply);
	}
}

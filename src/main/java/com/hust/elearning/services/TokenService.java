package com.hust.elearning.services;

import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.TokenEntity;
import com.hust.elearning.repository.TokenRepository;

@Service
public class TokenService {


	@Autowired
	private TokenRepository tokenRepository;
	@Autowired
	private EmailService emailService;
	

	@Autowired
	Environment environment;
	
	
	public TokenEntity generateToken() {
		String token = UUID.randomUUID().toString();
		return new TokenEntity(token);
	}


	public String generateToken(String token) {
		final TokenEntity myToken = new TokenEntity(token);
		String generateToken = token + myToken.hashCode();
		return generateToken;
	}

	/**
	 * Create Password Reset Token for User
	 * 
	 * @param user
	 * @param token
	 *            This is token generated
	 */
	public void createPasswordResetToken(final Account customer, final String token) {
		final TokenEntity myToken = new TokenEntity(token, customer.getId());
		tokenRepository.save(myToken);
	}

	
	public void sentResetPasswordLink(Account customer, String token,String serverAddress,int serverPort) {
		// Generate token
	
		String baseUrl = "http://" + serverAddress + ":" + serverPort + "/resetpassword";
		String uri = null;
		try {
			URIBuilder uriBuilder = new URIBuilder(baseUrl);
			uriBuilder.addParameter("id", String.valueOf(customer.getId()));
			uriBuilder.addParameter("token", token);
			uri = uriBuilder.toString();
		} catch (URISyntaxException e) {
		}
		emailService.sendResetPassword(customer.getEmail(), uri);

	}
	
	public void sentResetEmailLink(Account a, String token,String serverAddress,int serverPort) {
		// Generate token
	
		String baseUrl = "http://" + serverAddress + ":" + serverPort + "/setEmail";
		String uri = null;
		try {
			URIBuilder uriBuilder = new URIBuilder(baseUrl);
			uriBuilder.addParameter("id", String.valueOf(a.getId()));
			uriBuilder.addParameter("token", token);
			uri = uriBuilder.toString();
		} catch (URISyntaxException e) {
		}
		emailService.sendResetEmail(a.getEmail(), uri);

	}
	public void sentForgotPasswordLink(Account a, String token,String serverAddress,int serverPort) {
		// Generate token
	
		String baseUrl = "http://" + serverAddress + ":" + serverPort + "/setPassword";
		String uri = null;
		try {
			URIBuilder uriBuilder = new URIBuilder(baseUrl);
			uriBuilder.addParameter("id", String.valueOf(a.getId()));
			uriBuilder.addParameter("token", token);
			uri = uriBuilder.toString();
		} catch (URISyntaxException e) {
		}
		emailService.sendResetEmail(a.getEmail(), uri);

	}
	public void sentSuccessEmail(Account a, String token,String email,String serverAddress,int serverPort) {
		// Generate token
	
		String baseUrl = "http://" + serverAddress + ":" + serverPort + "/confirmEmail";
		String uri = null;
		try {
			URIBuilder uriBuilder = new URIBuilder(baseUrl);
			uriBuilder.addParameter("id", String.valueOf(a.getId()));
			uriBuilder.addParameter("token", token);
			uri = uriBuilder.toString();
		} catch (URISyntaxException e) {
		}
		emailService.sendConfirmEmail(email, uri);

	}
	
	public boolean validatePasswordResetToken(Long id, String token) {

		final TokenEntity passToken = tokenRepository.findByToken(token);
		if ((passToken == null)||( passToken.getIdAccount() != id) ) {
			return false;
		}
		else {
			final Calendar currentTime = Calendar.getInstance();
			boolean isExpired = (passToken.getExpiryDate() - currentTime.getTime().getTime()) <= 0;
			if (isExpired) {
				deleteByToken(passToken.getId());
				return false;
			}
		}
		return true;
	}

	public void deleteByToken(long id ) {
		tokenRepository.deleteByToken(id);
	}

	public void save(TokenEntity token) {
		tokenRepository.save(token);
	}

	public void deleteAll() {
		tokenRepository.deleteAll();
	}

	public void deleteByExpiryDate(long now) {
		tokenRepository.deleteByExpiryDate(now);
	}


}

package com.hust.elearning.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hust.elearning.dto.SubjectAjax;
import com.hust.elearning.entity.Question;
import com.hust.elearning.entity.Subject;
import com.hust.elearning.repository.QuestionRepository;
import com.hust.elearning.util.constants.Constants;

@Service
public class QuestionService {
	@Autowired
	private QuestionRepository questionRepository;
	public void createSubjectCourse(int idCourse,int idSubject) {
		questionRepository.insertSubjectCourse(idCourse, idSubject);
	}
	public int createSubject(Subject s) {
		questionRepository.insertSubject(s);
		return s.getId();
	}
	public void importQuestion(Question q) {
		questionRepository.createQuestion(q);
		questionRepository.createAnswer(q);
		questionRepository.updateTotalSubject(q.getIdSubject());
	}
	public Question getQuestionById(int idQuestion) {
		return questionRepository.getQuestionById(idQuestion);
	}
	public List<Question> getListQuestionLikeName(int idSubject, List<String> arr){
		String name="%";
		for(String n:arr) {
			name=name+n+"%";
		}
		return questionRepository.getListQuestionLikeName(idSubject,name);
	}
	public List<Question> getListQuestion(List<Integer> list){
		return questionRepository.getListQuestion(list);
	}
	
	public List<Integer> getListIdQuestionEasy(int idSubject){
		return questionRepository.getListIdQuestion(idSubject, 0);
	}
	public List<Integer> getListIdQuestionNormal(int idSubject){
		return questionRepository.getListIdQuestion(idSubject, 1);
	}
	public List<Integer> getListIdQuestionHard(int idSubject){
		return questionRepository.getListIdQuestion(idSubject, 2);
	}
	public List<SubjectAjax> getInfoSubjectOfCourse(int idCourse) {
		List<Subject> l=questionRepository.getSubjectOfCourse(idCourse);
		List<SubjectAjax> list=new ArrayList<SubjectAjax>();
		for(Subject s:l) {
			int idSubject=s.getId();
			SubjectAjax sa=new SubjectAjax();
			sa.setName(s.getName());
			int total=s.getTotal();
			sa.setId(idSubject);
			sa.setTotal(total);
			sa.setTotalEasy(questionRepository.getTotalQuestion(idSubject, 0));
			sa.setTotalNormal(questionRepository.getTotalQuestion(idSubject, 1));
			sa.setTotalHard(questionRepository.getTotalQuestion(idSubject, 2));
			sa.setMax(total>Constants.LIMIT_NUMS_QUESTION_TEST?Constants.LIMIT_NUMS_QUESTION_TEST:total);
			list.add(sa);
		}
		return list;
	}
	public List<Subject> getAllLibraryUser(int idAccount){
		 return questionRepository.getListSubjectOfUser(idAccount);
	}
	public List<SubjectAjax> getAllLibraryCourse(int idAccount,int idCourse) {
		List<Subject> l=questionRepository.getListSubjectOfUser(idAccount);
		List<Integer> listId=questionRepository.getListIdSujectOfCourse(idCourse);
		List<SubjectAjax> list=new ArrayList<SubjectAjax>();
		for(Subject s:l) {
			int idSubject=s.getId();
			SubjectAjax sa=new SubjectAjax();
			sa.setName(s.getName());
			int total=s.getTotal();
			sa.setId(idSubject);
			sa.setTotal(total);
			sa.setOfCourse(listId.contains(idSubject));
			list.add(sa);
		}
		return list;
	}
	public void removeLibraryCourse(int idCourse, int idSubject) {
		questionRepository.deleteSubjectCourse(idCourse, idSubject);
	}
	public void addLibraryCourse(int idCourse, int idSubject) {
		questionRepository.insertSubjectCourse(idCourse, idSubject);
	}
	public void deleteQuestion(int id,int idSubject) {
		questionRepository.deleteQuestionById(id);
		questionRepository.updateTotalDescSubject(idSubject);
	}
	public Subject getInfoSubjectVideo(String name) {
		return questionRepository.getSubjectVideo(name);
	}
	public List<Question> getListQuestionOfSubject(int id) {
		return questionRepository.getListQuestionLikeNameFull(id,"%");
	}
	public void updateNameSubject(int idSubject, String name) {
		questionRepository.updateNameSubject(idSubject,name);
	}
}

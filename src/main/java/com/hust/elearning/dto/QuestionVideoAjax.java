package com.hust.elearning.dto;

import java.util.List;

import com.hust.elearning.entity.Question;
import com.hust.elearning.entity.ViewVideo;

public class QuestionVideoAjax {
	private List<Question> questions;
	private List<Integer> times;
	private ViewVideo v;
	private int idSubject;
	
	public int getIdSubject() {
		return idSubject;
	}
	public void setIdSubject(int idSubject) {
		this.idSubject = idSubject;
	}
	public ViewVideo getV() {
		return v;
	}
	public void setV(ViewVideo v) {
		this.v = v;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public List<Integer> getTimes() {
		return times;
	}
	public void setTimes(List<Integer> times) {
		this.times = times;
	}
	
	
}

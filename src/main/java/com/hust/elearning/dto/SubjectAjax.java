package com.hust.elearning.dto;

public class SubjectAjax {
	private int id;
	private String name;
	private int total;
	private int max;
	private int totalEasy;
	private int totalNormal;
	private int totalHard;
	private boolean ofCourse;
	
	
	
	public boolean isOfCourse() {
		return ofCourse;
	}
	public void setOfCourse(boolean ofCourse) {
		this.ofCourse = ofCourse;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getTotalEasy() {
		return totalEasy;
	}
	public void setTotalEasy(int totalEasy) {
		this.totalEasy = totalEasy;
	}
	public int getTotalNormal() {
		return totalNormal;
	}
	public void setTotalNormal(int totalNormal) {
		this.totalNormal = totalNormal;
	}
	public int getTotalHard() {
		return totalHard;
	}
	public void setTotalHard(int totalHard) {
		this.totalHard = totalHard;
	}
	
}

package com.hust.elearning.dto;

import com.hust.elearning.entity.CourseMember;

public class MemberProcessAjax {
	private CourseMember member;
	private int post;
	private String video;
	private String assignment;
	public CourseMember getMember() {
		return member;
	}
	public void setMember(CourseMember member) {
		this.member = member;
	}
	public int getPost() {
		return post;
	}
	public void setPost(int post) {
		this.post = post;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	public String getAssignment() {
		return assignment;
	}
	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}
	
}

package com.hust.elearning.dto;

import java.util.List;

import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.GroupChat;
import com.hust.elearning.entity.Message;

public class GroupChatAjax {
	private int id;
	private String name;
	private long createdAt;
	private long updatedAt;
	private boolean isPrivate;
	private List<Account> listMember;
	private List<Message> listMessage;
	private String avatar;
	private boolean isRead;
	public GroupChatAjax(GroupChat c) {
		this.id=c.getId();
		this.name=c.getName();
		this.createdAt=c.getCreatedAt();
		this.updatedAt=c.getUpdatedAt();
		this.isPrivate=c.getIsPrivate();
		this.avatar="group-icon.png";
	}
	
	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}



	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	
	public List<Message> getListMessage() {
		return listMessage;
	}

	public void setListMessage(List<Message> listMessage) {
		this.listMessage = listMessage;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public long getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	public List<Account> getListMember() {
		return listMember;
	}
	public void setListMember(List<Account> listMember) {
		this.listMember = listMember;
	}
	
}

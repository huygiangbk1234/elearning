package com.hust.elearning.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class ConfigSecurity extends WebSecurityConfigurerAdapter{
	
	
	@Autowired
    private UserDetailServiceImpl userDetailsService;
	@Autowired
	CustomLogoutSuccessHandler customLogoutSuccessHandler;
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/custom/**", "/lib/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
        .authorizeRequests()
        	.antMatchers("/register").permitAll()
        	.antMatchers("/logout").permitAll()
        	.antMatchers("/about").permitAll()
        	.antMatchers("/contact").permitAll()
        	.antMatchers("/home").permitAll()
        	.anyRequest().authenticated()
	        .and()
	    .rememberMe()
	    	.key("huygiang1995")
			.rememberMeCookieName("remeconhg")
			.tokenValiditySeconds(24 * 60 * 60)
			.and()
        .formLogin()
            .loginPage("/home?errorlogin")
            .loginProcessingUrl("/login")
            .permitAll()
            .usernameParameter("email")
            .passwordParameter("password")
            .defaultSuccessUrl("/overview")
            .failureUrl("/home?errorlogin")
            .and()
        .logout()
            .logoutUrl("/logout")
            .logoutSuccessHandler(customLogoutSuccessHandler)
            .deleteCookies("JSESSIONID")
            .permitAll()
            .and()
        .exceptionHandling()
            .accessDeniedPage("/common/403");
	    return;
	}

	
}

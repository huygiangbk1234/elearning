package com.hust.elearning.config.auth;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hust.elearning.entity.Account;
import com.hust.elearning.repository.AccountRepository;
import com.hust.elearning.util.MyLog;

@Service
public class UserDetailServiceImpl implements UserDetailsService{
    @Autowired
    private AccountRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account user = userRepository.getUserByEmail(email);
        if (user == null) {
        	MyLog.in("User not found");
            throw new UsernameNotFoundException("not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword() , AuthorityUtils.createAuthorityList(user.getRole()));
    }
  
}

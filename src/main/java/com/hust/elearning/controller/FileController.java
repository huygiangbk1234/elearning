package com.hust.elearning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.util.constants.URL;

@Controller
public class FileController {
	@Autowired
	AccountService accountService;
	@Autowired
	CourseService courseService;
	
	
	@RequestMapping(URL.course_file)
	public String getFileMember(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a = accountService.getUserByEmail(auth.getName());
		CourseMember m = courseService.getMember(a.getId(), idCourse);
		String check = courseService.checkCourse(m, model);
		if (check != "")
			return check;
		Course c = courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course", c);
		model.addAttribute("member",m);
		
		model.addAttribute("fileLessons", courseService.getAllFileLessonsOfCourse(idCourse));
		model.addAttribute("fileOthers", courseService.getAllFileOthersOfCourse(idCourse,m.getId()));
		return "course/files";
	}
	@RequestMapping(URL.course_video)
	public String getVideoCourse(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a = accountService.getUserByEmail(auth.getName());
		CourseMember m = courseService.getMember(a.getId(), idCourse);
		String check = courseService.checkCourse(m, model);
		if (check != "")
			return check;
		Course c = courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course", c);
		model.addAttribute("member",m);
		
		model.addAttribute("videos", courseService.getAllVideosOfCourse(idCourse));
		return "course/videos";
	}
	@RequestMapping(URL.user_file)
	public String getFileUser(Model model,Authentication auth) {
		Account a = accountService.getUserByEmail(auth.getName());
		model.addAttribute("user", a);
		
		model.addAttribute("files", courseService.getAllFilesOfUser(a.getId()));
		return "user/files";
	}
	@RequestMapping(URL.user_video)
	public String getVideoUser(Model model,Authentication auth) {
		Account a = accountService.getUserByEmail(auth.getName());
		model.addAttribute("user", a);
		
		model.addAttribute("videos", courseService.getAllVideosOfUser(a.getId()));
		return "user/videos";
	}
}

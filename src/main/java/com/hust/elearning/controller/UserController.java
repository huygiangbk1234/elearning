package com.hust.elearning.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Document;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.services.QuestionService;
import com.hust.elearning.services.UploadServices;
import com.hust.elearning.util.MyLog;
import com.hust.elearning.util.constants.URL;

@Controller
public class UserController {
	@Autowired
	AccountService accountService;
	@Autowired
	CourseService courseService;
	@Autowired
	UploadServices uploadService;
	@Autowired
	QuestionService questionService;
	
	@PostMapping(URL.user_filter_by_email)
	@ResponseBody
	public List<Account> filterUserByEmail(@RequestParam("email")String email) {
		return accountService.filterUserByEmail(email);
	}
	
	@GetMapping(URL.user_home)
	public String getUserHome(Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		model.addAttribute("user",a);
		if(a.getRole().equals("admin")) return "redirect:/admin/home";
		int status=a.getStatus();
		String check=accountService.checkAccount(model, status);
		if(check!=null) return check;
		List<Course> l=courseService.getAllCourseUser(a.getId());
		int n=0;
		for(Course c:l) if(c.getStatus()==2) n++;
		model.addAttribute("numsCourse",l.size());
		model.addAttribute("numsCoursePass",n);
		model.addAttribute("numsFile",uploadService.countNumsFile(a.getId()));
		return "user/overview";
	}
	@GetMapping(URL.user_course)
	public String getUserCourse(Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		model.addAttribute("user",a);
		int status=a.getStatus();
		String check=accountService.checkAccount(model, status);
		if(check!=null) return check;
		List<Course> l=courseService.getAllCourseUser(a.getId());
		model.addAttribute("courses",l);
		return "user/allcourse";
	}
	@PostMapping(URL.user_update)
	public String updateUser(@RequestParam("avatar") MultipartFile file,@RequestParam Map<String,String> params,Model model,Authentication auth,HttpServletRequest request) {
		Account a=accountService.getUserByEmail(auth.getName());
		if(a.getStatus()==-2)a.setStatus(1);
		if(params.containsKey("fullname")) a.setFullname(params.get("fullname"));
		if(params.containsKey("nickname")) a.setNickname(params.get("nickname"));
		if(params.containsKey("tel")) a.setTel(params.get("tel"));
		if(!file.isEmpty()){
			String nameFile = String.valueOf(System.currentTimeMillis())+"."+file.getOriginalFilename().split("\\.")[1];
			byte[] bytes;
			FileOutputStream fos=null;
			try {
				bytes = file.getBytes();
				String fileLocation = new File("src\\main\\resources\\static\\custom\\img").getAbsolutePath() + "\\" + nameFile;
				fos= new FileOutputStream(fileLocation);
				fos.write(bytes);
				a.setAvatar(nameFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		accountService.updateAccount(a);
		String referer = request.getHeader("Referer");
	    return "redirect:"+ referer;
	}
	@GetMapping(URL.user_profile)
	public String getUserProfile(Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		model.addAttribute("user",a);
		int status=a.getStatus();
		String check=accountService.checkAccount(model, status);
		if(check!=null) return check;
		List<Course> l=courseService.getAllCourseUser(a.getId());
		List<CourseMember> m=new ArrayList<CourseMember>();
		for(Course c :l) {
			m.add(courseService.getMember(a.getId(), c.getId()));
		}
		model.addAttribute("courses",l);
		model.addAttribute("members",m);
		return "user/profile";
	}
	@GetMapping(URL.user_question_bank)
	public String getQuestionBank(Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		if(!a.getRole().equals("teacher")) return "";
		a.setPassword("");
		model.addAttribute("user",a);
		int status=a.getStatus();
		String check=accountService.checkAccount(model, status);
		if(check!=null) return check;
		model.addAttribute("subjects",questionService.getAllLibraryUser(a.getId()));
		return "user/question_bank";
	}
	@PostMapping(URL.user_update_alias)
	@ResponseBody
	public void updateAlias(@RequestParam("idMember") int idMember,@RequestParam("alias") String alias) {
		courseService.updateAlias(idMember,alias);
	}
}

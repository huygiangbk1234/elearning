package com.hust.elearning.controller;


import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.annotations.Param;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.jcodec.scale.ColorUtil;
import org.jcodec.scale.RgbToBgr;
import org.jcodec.scale.Transform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.support.AbstractResourceBasedMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hust.elearning.entity.Message;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.NotificationService;
@Controller
public class HomeController {
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private AccountService accountServices;
	@Autowired
	private NotificationService notificationService;
	
	@RequestMapping("/getMessage/{user}/{data}")
	@ResponseBody
	public ResponseEntity<?> gm(@PathVariable("data") String data,@PathVariable("user") String user) throws IOException{
	
		return new ResponseEntity<>(HttpStatus.OK);
	}
	@RequestMapping("/a")
	public String index(){return "simpleMessage";}
	@RequestMapping("/record")
	public String record(){return "recordStream";}
	@RequestMapping("/playVideo")
	public String playVideo(){return "openVideoFromServer";}
	@RequestMapping("playImage")
	public String playImg(){return "openImageFromServer";}
	@RequestMapping("/changeLanguage")
	public boolean changeLanguage(){
		((AbstractResourceBasedMessageSource) messageSource).setBasename("i18n/messages_jp");
		return true;
	}
	@RequestMapping("/getImage")
	public @ResponseBody byte[] getImg(HttpServletResponse response){
		 
		File file = null;
		try {
			file = new ClassPathResource("static/test.webm").getFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Picture p=null;
		try{
			p=FrameGrab.getFrameFromFile(file, 100);
		}catch(JCodecException e){}catch (IOException e) {e.printStackTrace();}
		BufferedImage bufferImage=AWTUtil.toBufferedImage(p);
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		try {
			ImageIO.write(bufferImage, "png", baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		try {
//			response.getOutputStream().write(baos.toByteArray());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//return Base64.encode(baos.toByteArray());
		return baos.toByteArray();
	}
	
	@RequestMapping("/getVideo")
	public void get(HttpServletResponse response){
	        ServletOutputStream out = null;
			try {
				out = response.getOutputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			File file = null;
			try {
				file = new ClassPathResource("static/test.webm").getFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			
			FileInputStream in = null;
			try {
				in = new FileInputStream(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        int bufferSize = 4096;
	        byte[] buffer = new byte[bufferSize];
	        int read;

	        try {
				while ((read = in.read(buffer, 0, bufferSize)) != -1) {
				    out.write(buffer, 0, read);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				try {
					out.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}
}

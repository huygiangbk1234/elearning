package com.hust.elearning.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hust.elearning.dto.GroupChatAjax;
import com.hust.elearning.dto.MessageAjax;
import com.hust.elearning.dto.MessageUpdateAjax;
import com.hust.elearning.dto.NotificationAjax;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Assignment;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.GroupChat;
import com.hust.elearning.entity.GroupChatMember;
import com.hust.elearning.entity.Message;
import com.hust.elearning.entity.NotificationRecipient;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.services.MessageService;
import com.hust.elearning.services.NotificationService;
import com.hust.elearning.util.DateUtils;
import com.hust.elearning.util.constants.Constants;
import com.hust.elearning.util.constants.URL;

@RestController
public class MessageController {
	
	@Autowired
	AccountService accountService;
	@Autowired
	MessageService messageService;
	@Autowired
	NotificationService notificationService;
	@Autowired
	CourseService courseService;
	
	public GroupChatAjax createGroupChatAjax(GroupChat group,Account user) {
		GroupChatAjax g=new GroupChatAjax(group);
		List<Account> la=messageService.getMemberGroup(group.getId());
		g.setListMember(la);
		if(g.isPrivate()) {
			for(Account a:la) {
				if(a.getId()!=user.getId()) {
					g.setAvatar(a.getAvatar());
					g.setName(a.getNickname());
				}
			}
		}
		g.setRead(messageService.isReadGroup(user.getId(), group.getId()));
		g.setListMessage(messageService.getLastMessageOfGroup(group.getId()));
		return g;
	}
	@PostMapping(URL.get_group_chat_single)
	public GroupChatAjax getGroupChatSingle(Authentication auth,@RequestParam("id") int idGroup) {
		if(auth==null) return null;
		Account user=accountService.getUserByEmail(auth.getName());
		GroupChat g=messageService.getGroupChat(idGroup);
		return this.createGroupChatAjax(g, user);
	}
	
	@PostMapping(URL.get_group_chat)
	public List<GroupChatAjax> getGroupChat(Authentication auth,@PathVariable("p") int p) {
		if(auth==null) return null;
		Account user=accountService.getUserByEmail(auth.getName());
		List<GroupChat> listGroup=messageService.getSomeGroupChat(user.getId(), p, Constants.LIMIT_ROW_GROUP_CHAT);
		List<GroupChatAjax> list=new ArrayList<GroupChatAjax>();
		for(GroupChat group : listGroup) {
			GroupChatAjax g=this.createGroupChatAjax(group, user);
			list.add(g);
		}
		return list;
	}
	
	@PostMapping(URL.get_message)
	public List<Message> getMessage(Authentication auth,@PathVariable("idGroup") int idGroup,@PathVariable("p") int p) {
		if(auth==null) return null;
		Account user1=accountService.getUserByEmail(auth.getName());
		if(!messageService.isMemberOfGroup(user1.getId(), idGroup)) return null;
		List<Message> l=messageService.getSomeMessage(idGroup, p, Constants.LIMIT_ROW_MESSAGE);
		return l;
	}
	
	@PostMapping(URL.send_message)
	public ResponseEntity<?> sendMessage(@RequestBody MessageAjax data,Authentication auth) {
		if(auth==null) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		if(data.getContent().trim()=="") return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		Account user1=accountService.getUserByEmail(auth.getName());
		if(data.getTo()==user1.getId())return null;
		data.setFrom(user1.getId());
		int idGroup=data.getIdGroup();
		if(idGroup==-1) {
			idGroup=messageService.isExistGroupChatPrivate(user1.getId(), data.getTo());
			if(idGroup==-1) {
				idGroup=messageService.createNewGroupPrivate(user1.getId(), data.getTo());
			}
		}
		data.setIdGroup(idGroup);
		long time=DateUtils.getTime();
		Message m= new Message(data.getContent());
		m.setIdGroup(idGroup);
		m.setIdAccount(user1.getId());
		m.setCreatedAt(time);
		int idMessage=messageService.saveMessage(m);
		messageService.sendMessageRealTime(data, idGroup,idMessage,time);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping(URL.message_recipient_read)
	public ResponseEntity<?> confirmReadMessage(Authentication auth,@RequestBody Map<String,Integer> map){
		if(auth==null||!map.containsKey("idGroup")) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		Account user=accountService.getUserByEmail(auth.getName());
		int n=messageService.updateReadMessage(map.get("idGroup"), user.getId(),DateUtils.getTime());
		if(n>0) return new ResponseEntity<>(HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping(URL.get_notification)
	public List<NotificationAjax> getNotification(Authentication auth,@RequestParam("from") int from) {
		Account a=accountService.getUserByEmail(auth.getName());
		List<NotificationAjax> list=notificationService.getNotification(a.getId(),from);
		return list;
	}
	@GetMapping(URL.get_event)
	public List<Assignment> getEvent(Authentication auth,@RequestParam("from") int from) {
		Account a=accountService.getUserByEmail(auth.getName());
		List<Course> lc= courseService.getAllCourseUser(a.getId());
		List<Assignment> list=new ArrayList<>();
		long time=org.apache.commons.lang3.time.DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH).getTime()+10*24*60*60*1000;
		for(Course c: lc) {
			list.addAll(courseService.getAssignmentAfterTime(c.getId(),DateUtils.getTime(),time));
		}
		Collections.sort(list, new Comparator<Assignment>() {
			 @Override
			    public int compare(Assignment a1, Assignment a2) {
			        return a1.getTimeEnd() > a2.getTimeEnd()  ? 1 : (a1.getTimeEnd() < a2.getTimeEnd()?-1:0);
			    }
		}); 
		return list;
	}
}

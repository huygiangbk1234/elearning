package com.hust.elearning.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.hust.elearning.dto.ServerResponseDto;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.TokenEntity;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.EmailService;
import com.hust.elearning.services.TokenService;
import com.hust.elearning.util.constants.Constants;
import com.hust.elearning.util.constants.URL;

@Controller
public class BaseController{
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private AccountService accountService;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private EmailService emailService;
	
	
	
	
	@RequestMapping(URL.home)
	public String getHomePage(Model model,Authentication auth,HttpServletRequest request) {
		if(auth!=null) {
			Account a=accountService.getUserByEmail(auth.getName());
			a.setPassword("");
			model.addAttribute("auth", a);
			String check=accountService.checkAccount(model, a.getStatus());
			if(check!=null) return check;
		}
		return "user/home";
	}
	@RequestMapping("/login")
	public String login() {
		return "redirect:/home";
	}
	@PostMapping(URL.register)
	public RedirectView register(@RequestParam("email") String email,@RequestParam("password") String pwd,@RequestParam("confirmpassword") String rpwd,
			Model model,HttpServletRequest request,RedirectAttributes redirectAttributes) {
		RedirectView r= new RedirectView("/overview");
		if(!pwd.equals(rpwd)) {
			r.setUrl("/home?errorRegister");
			r.addStaticAttribute("errorConfirmPassword", "Confirm password not match!");;
			return r;
		}
		Account account=accountService.getUserByEmail(email);
		if(account!=null) {
			r.setUrl("/home?errorRegister");
			r.addStaticAttribute("errorUserName", "The email was registered");
			return r;
		}
		Account a=new Account(email,passwordEncoder.encode(pwd));
		
		TokenEntity token = tokenService.generateToken();
		a.setStatus(-1);
		accountService.createAccount(a,token);
		accountService.autoLogin(a, request,pwd);
        
		emailService.sendRegisterEmail(a, token.getToken(), request.getServerName(),request.getServerPort());
		return r;
		
	   
	}
	@GetMapping(URL.verify_account)
	public String registerConfirmPage(@RequestParam("token") String token,HttpSession session,Model model,HttpServletRequest request) {
		TokenEntity t=accountService.getTokenConfirmRegister(token);
		if(t==null) {
			return "redirect:/home";
		}
		if(!t.checkDate()) {
			model.addAttribute("message", "Invalid token");
		}else {
			Account a=accountService.getUserById(t.getIdAccount());
			a.setStatus(-2);
			accountService.deleteToken(t.getId());
			accountService.updateAccount(a);
		//	accountService.autoLogin(a, request);
		}
		return "redirect:/overview";
	}
	
	@GetMapping(URL.forgot_password)
	public String viewForgotPasswordForm() {
		return "user/forgotPassword";
	}

	
	@PostMapping(URL.change_password)
	@ResponseBody
	public ServerResponseDto changePassword(Authentication authentication,
			@RequestParam("password") String currentPassword,
			@RequestParam("newpassword") String newPassword,
			@RequestParam("confirmpassword") String confirmNewPassword) {
		Account account = accountService.getUserByName(authentication.getName());
		if (!passwordEncoder.matches(currentPassword, account.getPassword())){
			return new ServerResponseDto(Constants.RESP_STATUS_ERROR, "Current password not right!", null);
		} else if (currentPassword.equals(newPassword)) {
			return new ServerResponseDto(Constants.RESP_STATUS_ERROR, "New password and current password must different!", null);
		} else {
			accountService.updateAccount(account);
			return new ServerResponseDto(Constants.RESP_STATUS_SUCCESS, "Your Password has been changed", null);
		}
	}
	
	@PostMapping(URL.forgot_password)
	@ResponseBody
	ServerResponseDto forgotPassword(@RequestParam("email") String email, HttpSession session,
			HttpServletRequest request) {
		return null;
//		session.setAttribute("email", email);
//		BookingUserDetailEntity customerEntity = customerService.findByEmail(email);
//		if (customerEntity == null) {
//			return new ServerResponseDto(Constants.RESP_STATUS_ERROR, "This email does not belong to any accounts!",
//					null);
//		} else {
//			final String token = UUID.randomUUID().toString();
//			String generateToken = tokenService.generateToken(token);
//			tokenService.createPasswordResetToken(customerEntity, generateToken);
//			tokenService.sentForgotPasswordLink(customerEntity, generateToken, request.getServerName(),
//					request.getServerPort());
//			return new ServerResponseDto(Constants.RESP_STATUS_SUCCESS,
//					"We sent a link confirm to your email. THis link effective in 10 minute", null);
//		}

	}
	@RequestMapping(URL.about)
	public String getAboutPage(Model model,@RequestParam Map<String,Integer> allRequestParams,Authentication auth) {
		if(auth!=null) {
			Account a=accountService.getUserByEmail(auth.getName());
			model.addAttribute("auth", a);
		}
		return "user/about";
	}
	@RequestMapping(URL.contact)
	public String getContactPage(Model model,@RequestParam Map<String,Integer> allRequestParams,Authentication auth) {
		if(auth!=null) {
			Account a=accountService.getUserByEmail(auth.getName());
			model.addAttribute("auth", a);
		}
		
		return "user/contact";
	}
}	

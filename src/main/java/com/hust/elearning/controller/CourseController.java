package com.hust.elearning.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.primitives.Ints;
import com.hust.elearning.dto.MemberProcessAjax;
import com.hust.elearning.dto.NotificationAjax;
import com.hust.elearning.dto.QuestionVideoAjax;
import com.hust.elearning.dto.SubjectAjax;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Assignment;
import com.hust.elearning.entity.AssignmentSubmit;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Document;
import com.hust.elearning.entity.GroupCourse;
import com.hust.elearning.entity.Lesson;
import com.hust.elearning.entity.Notification;
import com.hust.elearning.entity.NotificationRecipient;
import com.hust.elearning.entity.Question;
import com.hust.elearning.entity.Subject;
import com.hust.elearning.entity.ViewVideo;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.services.MessageService;
import com.hust.elearning.services.NotificationService;
import com.hust.elearning.services.PostService;
import com.hust.elearning.services.QuestionService;
import com.hust.elearning.services.UploadServices;
import com.hust.elearning.util.DateUtils;
import com.hust.elearning.util.constants.Constants;
import com.hust.elearning.util.constants.URL;
import com.sun.glass.ui.View;

@Controller
public class CourseController {
	@Autowired
	AccountService accountService;
	@Autowired
	CourseService courseService;
	@Autowired
	QuestionService questionService;
	@Autowired
	UploadServices uploadService;
	@Autowired
	NotificationService notificationService; 
	@Autowired
	PostService postService;
	@Autowired
	MessageService messageService;
	
	@RequestMapping(URL.search)
	public String search(Model model,@RequestParam("q") String str,HttpServletRequest request) {
		if(str.replaceAll("\\s+","").equals("")) {
			String referer = request.getHeader("Referer");
		    return "redirect:"+ referer;
		}
		String s=str.replaceAll("\\s+","");
		try {
			int id=Integer.parseInt(s);
			Course c=courseService.getCourseById(id);
			if(c!=null) return "redirect:/course/"+id;
		}catch(NumberFormatException e) {}
		return "";
	}
	@GetMapping(URL.course_home)
	public String getCourseHome(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		a.setPassword("");
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		String f="dd/MM/yyyy";
		model.addAttribute("timeStart",DateUtils.covertDate(c.getTimeStart(),f));
		model.addAttribute("timeEnd",DateUtils.covertDate(c.getTimeEnd(),f));
		model.addAttribute("percent",DateUtils.calPercent(c.getTimeStart(), c.getTimeEnd()));
		accountService.updateLastCourse(a.getId(),idCourse);
		return "course/overview";
	}
	@PostMapping(URL.course_update)
	@ResponseBody
	public int updateCourse(@RequestBody Course c,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(), c.getId());
		if(!m.getRole().equals("admin")) return -1;
		courseService.updateCourse(c);
		return c.getId();
	}
	@PostMapping(URL.user_filter_by_alias)
	@ResponseBody
	public List<CourseMember> filterAlias(@RequestParam("alias") String alias,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		return courseService.filterMemberByAlias(alias);
	}
	@PostMapping(URL.user_course_create)
	@ResponseBody
	public int createCourse(@RequestBody Course c,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		c.setIdCreator(a.getId());
		c.setPrivate(true);
		c.setStatus(1);
		courseService.createCourse(c);
		
		CourseMember m=new CourseMember();
		m.setAlias(a.getNickname());
		m.setIdCourse(c.getId());
		m.setIdGroup(-1);
		m.setRole("admin");
		m.setStatus(1);
		m.setMember(a);
		courseService.createMember(m);
		return c.getId();
	}
	@PostMapping(URL.course_filter_get_user_by_name)
	@ResponseBody
	public List<CourseMember> getMemberLikeName(Model model,Authentication auth,@PathVariable("id") int idCourse,@RequestParam("name") String name){
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return null;
		return courseService.getMemberLikeName(idCourse, name);
	}
	@GetMapping(URL.course_member_all)
	public String getAllCourseMember(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		m.setMember(a);
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		int total=courseService.countTotalMember(idCourse);
		model.addAttribute("total", total);
		
		int limit=Constants.LIMIT_ROW_MEMBER;
		int p=total/limit;
		if(total%limit!=0) p+=1;
		int[] pages=new int[p];
		for(int i=0;i<p;i++) pages[i]=i;
		model.addAttribute("pages", pages);
		return "course/members";
	}
	@GetMapping(URL.course_member_page)
	@ResponseBody
	public List<CourseMember> getPageCourseMember(@PathVariable("id") int idCourse,@PathVariable("p") int page,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return null;
		int limit=Constants.LIMIT_ROW_MEMBER;
		List<CourseMember> l=courseService.getSomeMember(idCourse,page*limit,limit);
		return l;
	}
	@PostMapping(URL.course_add_list_member)
	@ResponseBody
	public void addMembers(@PathVariable("id") int idCourse,Authentication auth,@RequestBody List<CourseMember> list) {
		for(CourseMember i:list) {
			CourseMember c=courseService.getMember(i.getMember().getId(), idCourse);
			if(c==null) {
				courseService.createMember(i);
			}
		}
	}
	@PostMapping(URL.course_del_member)
	@ResponseBody
	public void delMember(@PathVariable("id") int idCourse,Authentication auth,@RequestParam("id") int idMember) {
		courseService.deleteMember(idMember);
	}
	
	@GetMapping(URL.course_question_bank)
	public String getPageQuestionBank(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		m.setMember(a);
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		
		List<SubjectAjax> list=questionService.getInfoSubjectOfCourse(idCourse);
		model.addAttribute("subjects",list);
		return "course/question_bank";
	}
	
	
	@GetMapping(URL.course_lessons)
	public String getLessons(@PathVariable("id") int idCourse,Model model,Authentication auth){
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		if(!m.getRole().equals("admin"))return "";
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		
		model.addAttribute("lessons", courseService.getAllLesson(idCourse));
		return "course/lessons";
	}
	@PostMapping(URL.course_lesson_get_all)
	@ResponseBody
	public List<Lesson> getAllLesson(@PathVariable("id") int idCourse){
		return courseService.getAllLesson(idCourse);
	}
	@PostMapping(URL.course_lesson_create)
	@ResponseBody
	public Lesson createLesson(@RequestBody Lesson l) {
		courseService.createLession(l);
		return l;
	}
	@RequestMapping(URL.course_lesson_update)
	@ResponseBody
	public void updateLesson(@PathVariable("idLesson") int idLesson,@PathVariable("id") int idCourse,
			@RequestParam("fileDelete") ArrayList<Integer> fileDelete,
			HttpServletRequest request,Authentication auth) {
		
		Account a=accountService.getUserByEmail(auth.getName());
		
		if(fileDelete.size()>0)postService.deleteFilePost(fileDelete);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_LESSON+idLesson);
		 if(list.size()>0)postService.createListDocument(list);
	}
	
	@RequestMapping(URL.course_lesson_update_pos)
	@ResponseBody
	public void updatePosLesson(@PathVariable("idLesson") int idLesson,@RequestParam("pos") int pos) {
		courseService.updateIndexLesson(idLesson,pos);
	}
	@RequestMapping(URL.course_lesson_update_name)
	@ResponseBody
	public String updatePosLesson(@PathVariable("idLesson") int idLesson,@RequestParam("name") String name) {
		courseService.updateNameLesson(idLesson,name);
		return name;
	}
	@GetMapping(URL.course_lesson_video_open)
	public String openVideo(@PathVariable("id") int idCourse,@PathVariable("idLesson") int idLesson,
			@PathVariable("idVideo") int idVideo,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		
		Document video=courseService.getVideo(idVideo);
		Subject s=questionService.getInfoSubjectVideo(idVideo+Constants.PRE_VIDEO+"%");
		QuestionVideoAjax q=new QuestionVideoAjax();
		q.setIdSubject(s.getId());
		q.setQuestions(questionService.getListQuestionOfSubject(s.getId()));
		String x=s.getName().split("_")[1];
		x=x.substring(1, x.length()-1);
		String[] arr=x.split(",");
		ArrayList<Integer> times=new ArrayList<>();
		for(String str:arr) {
			times.add(Integer.parseInt(str.trim()));
		}
		q.setTimes(times);
		q.setV(courseService.getViewVideoMember(idVideo,m.getId()));
		model.addAttribute("question",q);
		model.addAttribute("video", video);
		return "course/video";
	}
	
	@GetMapping(URL.course_assignment_page)
	public String getAssignmentPage(@PathVariable("id") int idCourse,@PathVariable("idAssignment") int idAssignment,Authentication auth,Model model) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		model.addAttribute("assignment",courseService.getAssignment(idAssignment,true));
		if(m.getRole().equals("admin")) {
			List<AssignmentSubmit> list=courseService.getSubmitOfAssignment(idAssignment);
			model.addAttribute("listSubmit",list);
			model.addAttribute("userNotSubmit", courseService.getUserNotSubmitAssignment(idAssignment, idCourse));
			model.addAttribute("groups", courseService.getAllGroup(idCourse));
		}else {
			model.addAttribute("submit",courseService.getSubmitAssignmentOfUser(idAssignment,m.getId()));
			model.addAttribute("group", courseService.getGroupCourseById(m.getIdGroup()));
		}
		
		return "course/assignment";
	}
	@GetMapping(URL.course_assignment)
	public String getAssignmentAll(@PathVariable("id") int idCourse,Authentication auth,Model model) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		return "course/assignments";
	}
	@GetMapping(URL.course_assignment_create_page)
	public String getAssignmentCreatePage(@RequestParam Map<String,String> map,@PathVariable("id") int idCourse,Authentication auth,Model model,HttpServletRequest request) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		if(map.containsKey("idLesson")) {
			model.addAttribute("idLesson", map.get("idLesson"));
			model.addAttribute("lastUrl", request.getHeader("referer"));
		}else if(map.containsKey("edit")) {
			model.addAttribute("assignment",courseService.getAssignment(Integer.parseInt(map.get("edit")),true));
			model.addAttribute("lastUrl", request.getHeader("referer"));
		}
		return "course/assignments_create";
	}
	@PostMapping(URL.course_assignment_create)
	@ResponseBody
	public int createAssignment(@PathVariable("id") int idCourse,
			@RequestParam("title") String title,@RequestParam("content") String content,@RequestParam("idLesson") int idLesson,
			@RequestParam("timeEnd") long timeEnd,
			@RequestParam("groupSubmit") boolean groupSubmit,
			Authentication auth,HttpServletRequest request) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		if(!m.getRole().equals("admin")) return -1;
		if(content.trim()==""||title.trim()==""||idLesson==-1)return -1;
		long time=DateUtils.getTime();
		Assignment assignment=new Assignment();
		
		assignment.setContent(content);
		assignment.setGroupSubmit(groupSubmit);
		assignment.setIdCourse(idCourse);
		assignment.setIdLesson(idLesson);
		assignment.setTimeEnd(timeEnd);
		assignment.setTitle(title);
		assignment.setCreatedAt(time);
		courseService.createAssignment(assignment);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_ASSIGNMENT+assignment.getId());
		if(list.size()>0)postService.createListDocument(list);
		
		
		Notification n = new Notification();
		n.setCreatedAt(time);
		n.setCreator(m);
		n.setParent(Constants.PRE_ASSIGNMENT + assignment.getId());
		notificationService.createNotification(n);
		List<CourseMember> lm = courseService.getAllMember(idCourse);
		NotificationRecipient nr = new NotificationRecipient();
		nr.setCreatedAt(time);
		nr.setNotification(n);
		nr.setRead(false);
		for (CourseMember c : lm) {
			if (c.getId() != m.getId()) {
				nr.setIdAccount(c.getMember().getId());
				notificationService.createNotificationRecipient(nr);
				NotificationAjax na= notificationService.createNotificationAssingment(assignment.getId(), m, false);
				messageService.sendNotificationRealTime(na, c.getMember().getEmail());
			}
		}
		return assignment.getId();
	}
	@PostMapping(URL.course_assignment_get)
	@ResponseBody
	public List<Assignment> getAssignmentOfCourse(@PathVariable("id") int idCourse) {
		return courseService.getAllAssignment(idCourse);
	}
	@PostMapping(URL.course_assignment_delete)
	@ResponseBody
	public void deleteAssignment(@PathVariable("id") int idCourse,@PathVariable("idAssignment") int idAssignment,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		if(m.getRole().equals("admin")) {
			courseService.deleteAssignment(idAssignment);
		}
	}
	@RequestMapping(URL.course_assignment_update)
	@ResponseBody
	public void updateAssignment(@PathVariable("idAssignment") int idAssignment,@PathVariable("id") int idCourse,
			@RequestParam("fileDelete") ArrayList<Integer> fileDelete,
			@RequestParam("title") String title,@RequestParam("content") String content,@RequestParam("idLesson") int idLesson,
			@RequestParam("timeEnd") long timeEnd,
			@RequestParam("groupSubmit") boolean groupSubmit,
			HttpServletRequest request,Authentication auth) {
		
		Account a=accountService.getUserByEmail(auth.getName());
		Assignment assignment=new Assignment();
		assignment.setId(idAssignment);
		assignment.setContent(content);
		assignment.setGroupSubmit(groupSubmit);
		assignment.setIdCourse(idCourse);
		assignment.setIdLesson(idLesson);
		assignment.setTimeEnd(timeEnd);
		assignment.setTitle(title);
		courseService.updateAssignment(assignment);
		
		if(fileDelete.size()>0)postService.deleteFilePost(fileDelete);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_ASSIGNMENT+idAssignment);
		if(list.size()>0)postService.createListDocument(list);
	}
	@PostMapping(URL.course_assignment_submit)
	@ResponseBody
	public void submitAssignment(@PathVariable("id") int idCourse,
			@RequestParam("idAssignment") int idAssignment,Authentication auth,HttpServletRequest request) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(), idCourse);
		long time=DateUtils.getTime();
		Assignment assignment=courseService.getAssignment(idAssignment,false);
		AssignmentSubmit as=courseService.getSubmitAssignmentOfUser(idAssignment, m.getId());
		if(as!=null) return;
		as=new AssignmentSubmit();
		as.setIdAssignment(idAssignment);
		as.setMember(m);
		as.setRoot(0);
		as.setCreatedAt(time);
		as.setUpdatedAt(time);
		courseService.submitAssignment(as);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_ASSIGNMENT_SUBMIT+as.getId());
		if(list.size()>0)postService.createListDocument(list);
		int id=as.getId();
		
		if(assignment.isGroupSubmit()) {
			GroupCourse g=courseService.getGroupCourseById(m.getIdGroup());
			as.setRoot(id);
			for(CourseMember c : g.getMembers()) {
				if(c.getId()==m.getId())continue;
				as.setMember(c);
				courseService.submitAssignment(as);
			}
		}
	}
	@RequestMapping(URL.course_assignment_submit_update)
	@ResponseBody
	public void updateSubmitAssignment(@PathVariable("idAssignmentSubmit") int idAs,@PathVariable("id") int idCourse,
			@RequestParam("fileDelete") ArrayList<Integer> fileDelete,
			HttpServletRequest request,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		long time =DateUtils.getTime();
		courseService.updateAssignmentSubmit(idAs,time);
		if(fileDelete.size()>0)postService.deleteFilePost(fileDelete);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_ASSIGNMENT_SUBMIT+idAs);
		if(list.size()>0)postService.createListDocument(list);
	}
	@GetMapping(URL.course_assignment_get_notsubmit)
	@ResponseBody
	public List<Assignment> getAssignmentNotSubmit(@PathVariable("id") int idCourse) {
		return courseService.getAssignmentNotSubmit(idCourse);
	}
	
	@RequestMapping(URL.course_group)
	public String getGroupPage(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		List<GroupCourse> list=courseService.getAllGroup(idCourse);
		List<CourseMember> listMember=courseService.getAllMember(idCourse);
		model.addAttribute("list", list);
		model.addAttribute("listMember", listMember);
		return "course/groups";
	}
	@PostMapping(URL.course_group_create)
	@ResponseBody
	public void createListGroup(@PathVariable("id") int idCourse,@RequestBody List<GroupCourse> list) {
		for(GroupCourse g : list) {
			courseService.createGroup(g);
		}
	}
	@PostMapping(URL.course_group_add_list)
	@ResponseBody
	public void addListMemberGroup(@PathVariable("id") int idCourse,@PathVariable("idGroup") int idGroup,@RequestBody List<Integer> listIdMember) {
		for(int i:listIdMember) {
			courseService.changeMemberGroup(idGroup,i);
		}
	}
	@RequestMapping(URL.course_group_update)
	@ResponseBody
	public void updateNameGroup(@PathVariable("id") int idCourse,@RequestParam("idGroup")int idGroup,@RequestParam("name")String name) {
		courseService.updateGroup(idGroup,name);
	}
	@RequestMapping(URL.course_group_delete)
	@ResponseBody
	public void deleteGroup(@PathVariable("id") int idCourse,@RequestParam("idGroup")int idGroup) {
		courseService.deleteGroupCourse(idGroup);
	}
	@RequestMapping(URL.course_group_member_update)
	@ResponseBody
	public void updateMemberGroup(@RequestParam("idGroup")int idGroup,@RequestParam("idUser")int idMember) {
		courseService.changeMemberGroup(idGroup,idMember);
	}

	@GetMapping(URL.course_video_create)
	public String getVideoCreate(HttpServletRequest request,@PathVariable("id") int idCourse,Model model,Authentication auth,@RequestParam("idLesson") int idLesson) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		if(!m.getRole().equals("admin"))return "";
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		model.addAttribute("idLesson", idLesson);
		return "course/video_create";
	}
	@PostMapping(URL.course_video_submit)
	@ResponseBody
	public int createVideo(@PathVariable("id") int idCourse,
			@RequestParam("idLesson") int idLesson,
			Authentication auth,HttpServletRequest request) {
		Account a=accountService.getUserByEmail(auth.getName());
		
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_LESSON+idLesson);
		if(list.size()>0)postService.createListDocument(list);
		return list.get(0).getId();
	}
	@PostMapping(URL.course_video_submit_question)
	@ResponseBody
	public void createQuestionVideo(@PathVariable("id") int idCourse,
			@RequestParam("idVideo") String idVideo,@RequestBody QuestionVideoAjax q,@RequestParam("length") String length,
			Authentication auth,HttpServletRequest request) {
		
		Subject s=new Subject();
		s.setIdAccount(1);
		s.setName(idVideo+Constants.PRE_VIDEO+"_"+Arrays.toString((Ints.toArray(q.getTimes()))));
		s.setTotal(0);
		questionService.createSubject(s);
		for(Question question:q.getQuestions()) {
			question.setIdSubject(s.getId());
			questionService.importQuestion(question);
		}
		List<CourseMember> l=courseService.getAllMember(idCourse);
		ViewVideo v=new ViewVideo();
		v.setLength("0-"+length);
		String answers="";
		for(int i=0;i<q.getQuestions().size();i++) {
			answers+="0";
			if(i<q.getQuestions().size()-1)answers+="_";
		}
		v.setAnswers(answers);
		v.setIdVideo(Integer.parseInt(idVideo.trim()));
		for(CourseMember c: l) {
			if(!c.getRole().equals("admin")) {
				v.setIdMember(c.getId());
				courseService.createViewVideo(v);
			}
		}
	}
	@PostMapping(URL.course_video_update_length)
	@ResponseBody
	public void updateViewVideoLength(@PathVariable("id") int idCourse,
			@PathVariable("idViewVideo") int idViewVideo,@RequestParam("length") String length,
			Authentication auth) {
			courseService.updateViewVideoLength(idViewVideo,length);
	}
	@PostMapping(URL.course_video_update_answers)
	@ResponseBody
	public void updateViewVideoAnswers(@PathVariable("id") int idCourse,
			@PathVariable("idViewVideo") int idViewVideo,@PathVariable("a") String answers,
			Authentication auth) {
			courseService.updateViewVideoAnswers(idViewVideo,answers);
	}
	@PostMapping(URL.course_video_update_questions)
	@ResponseBody
	public void updateQuestionsVideo(@PathVariable("id") int idCourse,
			@PathVariable("idSubject") int idSubject,@RequestParam("name") String name,
			Authentication auth) {
			questionService.updateNameSubject(idSubject,name);
	}
	@GetMapping(URL.course_score)
	public String getScore(HttpServletRequest request,@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
//		if(!m.getRole().equals("admin"))return "";
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		
		return "course/score";
	}
	@GetMapping(URL.course_progress)
	public String getProgess(HttpServletRequest request,@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return check;
		if(!m.getRole().equals("admin"))return "";
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		
		return "course/progress";
	}
	@PostMapping(URL.course_progress)
	@ResponseBody
	public List<MemberProcessAjax> getDataProgess(HttpServletRequest request,@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return null;
		if(!m.getRole().equals("admin"))return null;
		
		List<MemberProcessAjax> list=new ArrayList<>();
		List<CourseMember> l=courseService.getAllMember(idCourse);
		List<Assignment> la= courseService.getAllAssignment(idCourse);
		int n=la.size();
		for(CourseMember cm :l) {
			if(cm.getRole().equals("admin")) continue;
			MemberProcessAjax mp=new MemberProcessAjax();
			mp.setMember(cm);
			int c=0;
			for(Assignment ag:la) {
				if(courseService.getDefaultSubmitAssignmentOfUser(ag.getId(), cm.getId())!=null) c++;
			}
			mp.setAssignment(c+"/"+n);
			mp.setPost(courseService.countAllPostPrOfUser(cm.getId()));
			List<Document> ld=courseService.getAllVideosOfCourse(idCourse);
			int n1=ld.size();
			float t=0;
			for(Document d:ld) {
				ViewVideo v= courseService.getViewVideoMember(d.getId(), cm.getId());
				if(v==null)continue;
				String len=v.getLength();
				String[] tok=len.split("-");
				if(tok.length==2) {
					t+=Float.parseFloat(tok[0])/Float.parseFloat(tok[1]);
				}
			}
			if(n1>0)mp.setVideo(Math.round((100*t/n1)*10.0/10.0)+"%"+" - "+n1);
			else mp.setVideo("0% - 0");
			list.add(mp);
		}
		return list;
	}
	@PostMapping(URL.course_progress_detail)
	@ResponseBody
	public Map<String,Object> getDetailProcess(@PathVariable("id") int idCourse,@PathVariable("idMember") int idMember) {
		Map<String,Object> map=new HashMap<String, Object>();
		List<Document> ld=courseService.getAllVideosOfCourse(idCourse);
		List<Assignment> la=courseService.getAllAssignment(idCourse);
		for(Assignment ag:la) {
			ag.setCreatedAt(0);
			AssignmentSubmit as= courseService.getDefaultSubmitAssignmentOfUser(ag.getId(), idMember);
			if(as!=null) ag.setCreatedAt(as.getUpdatedAt());
		}
		List<Integer> percents=new ArrayList<>();
		for(Document d:ld) {
			ViewVideo v= courseService.getViewVideoMember(d.getId(), idMember);
			if(v==null) {
				percents.add(0);
			};
			String len=v.getLength();
			String[] tok=len.split("-");
			if(tok.length==2) {
				int x=(int)(100*Float.parseFloat(tok[0])/Float.parseFloat(tok[1]));
				if(x>=98)x=100;
				percents.add(x);
			}
		}
		map.put("videos", ld);
		map.put("percents", percents);
		map.put("assignments", la);
		return map;
	}
	@GetMapping(URL.course_progress_detail_user)
	public String getDetailProcessUser(@PathVariable("id") int idCourse,Authentication auth,Model model) {
		Account a=accountService.getUserByEmail(auth.getName());
		a.setPassword("");
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		if(m==null) return null;
		if(m.getRole().equals("admin")) return null;
		Course co=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",co);
		model.addAttribute("member",m);
		
		List<Document> ld=courseService.getAllVideosOfCourse(idCourse);
		List<Assignment> la=courseService.getAllAssignment(idCourse);
		int n=la.size();
		int c=0;
		for(Assignment ag:la) {
			ag.setCreatedAt(0);
			AssignmentSubmit as= courseService.getDefaultSubmitAssignmentOfUser(ag.getId(), m.getId());
			if(as!=null) {
				ag.setCreatedAt(as.getUpdatedAt());
				c++;
			}
		}
		List<Integer> percents=new ArrayList<>();
		int n1=ld.size();
		float t=0;
		for(Document d:ld) {
			ViewVideo v= courseService.getViewVideoMember(d.getId(), m.getId());
			if(v==null) {
				percents.add(0);
				continue;
			};
			String len=v.getLength();
			String[] tok=len.split("-");
			if(tok.length==2) {
				int x=(int)(100*Float.parseFloat(tok[0])/Float.parseFloat(tok[1]));
				if(x>=98)x=100;
				percents.add(x);
				t+=Float.parseFloat(tok[0])/Float.parseFloat(tok[1]);
			}
		}
		model.addAttribute("videos", ld);
		model.addAttribute("percents", percents);
		model.addAttribute("assignments", la);
		model.addAttribute("post", courseService.countAllPostPrOfUser(m.getId()));
		model.addAttribute("p_assignments", c+"/"+n);	
		if(n1>0)model.addAttribute("p_videos",Math.round((100*t/n1)*10.0/10.0)+"%"+" - "+n1);
		else model.addAttribute("p_videos","0% - 0");

		return "course/progress_user";
	}
}

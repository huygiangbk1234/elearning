package com.hust.elearning.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hust.elearning.dto.SubjectAjax;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Question;
import com.hust.elearning.entity.Subject;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.services.QuestionService;
import com.hust.elearning.services.UploadServices;
import com.hust.elearning.util.DataExample;
import com.hust.elearning.util.constants.URL;

@Controller
public class TestController {
	@Autowired
	AccountService accountService;
	@Autowired
	CourseService courseService;
	@Autowired
	UploadServices uploadService;
	@Autowired
	QuestionService questionService;

	@PostMapping(URL.course_filter_get_question_by_name)
	@ResponseBody
	public List<Question> getFilterQuestion(@RequestParam("idSubject") int idSubject,@RequestBody List<String> arr){
		return questionService.getListQuestionLikeName(idSubject, arr);
	}
	@PostMapping(URL.course_get_question)
	@ResponseBody
	public Question getQuestionByID(@RequestParam("id") int idQuestion){
		return questionService.getQuestionById(idQuestion);
	}
	@GetMapping(URL.course_test_delete_question)
	@ResponseBody
	public void deleteQuestion(@RequestParam("id") int idQuestion,@RequestParam("idSubject") int idSubject){
		 questionService.deleteQuestion(idQuestion, idSubject);
	}
	@GetMapping(URL.course_subject_create)
	@ResponseBody
	public void createSubject(Authentication auth,@RequestParam("name") String name){
		Subject s=new Subject();
		s.setIdAccount(accountService.getUserByEmail(auth.getName()).getId());
		s.setName(name);
		s.setTotal(0);
		questionService.createSubject(s);
	}
	@GetMapping(URL.course_subject_update)
	@ResponseBody
	public void updateSubject(@RequestParam("id") int idSubject,@RequestParam("name") String name){
		questionService.updateNameSubject(idSubject, name);
	}
	@PostMapping(URL.course_subject_import)
	@ResponseBody
	public void importQuestionSubject(@RequestParam("id") int idSubject,@RequestBody List<Question> list){
		for(Question q:list)
		questionService.importQuestion(q);
	}
	
	@PostMapping(URL.course_test)
	public String getCourseTestPage(@PathVariable("id") int idCourse, Model model, Authentication auth,
			@RequestParam("nums") int nums,@RequestParam("numsEasy") int numsEasy,@RequestParam("numsHard") int numsHard,
			@RequestParam("numsNormal") int numsNormal,@RequestParam("idSubject") int idSubject) {
		Account a = accountService.getUserByEmail(auth.getName());
		CourseMember m = courseService.getMember(a.getId(), idCourse);
		String check = courseService.checkCourse(m, model);
		if (check != "")
			return check;
		Course c = courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course", c);
		model.addAttribute("member",m);
		
//		List<Subject> list = DataExample.getSubject();
//		for (int i=0;i<list.size();i++) {
//			Subject s=list.get(i);
//			questionService.createSubject(s);
//			List<Question> l = DataExample.getQuestion(s.getId());
//			for (int j=0;j<l.size();j++) {
//				Question q=l.get(j);
//				questionService.importQuestion(q);
//			}
//		}
		
		List<Integer> l1 = questionService.getListIdQuestionEasy(idSubject);
		List<Integer> l2 = questionService.getListIdQuestionNormal(idSubject);
		List<Integer> l3 = questionService.getListIdQuestionHard(idSubject);

		model.addAttribute("listEasy", l1);
		model.addAttribute("listNormal", l2);
		model.addAttribute("listHard", l3);
		model.addAttribute("nums",nums);
		model.addAttribute("numsEasy",numsEasy);
		model.addAttribute("numsHard",numsHard);
		model.addAttribute("numsNormal",numsNormal);
		return "course/test";
	}
	@PostMapping(URL.course_test_getlistquestion)
	@ResponseBody
	public List<Question> getCourseTestPage(@PathVariable("id") int idCourse,@RequestBody List<Integer> listId, Model model, Authentication auth) {
		return questionService.getListQuestion(listId);
	}
	@PostMapping(URL.course_question_bank_library)
	@ResponseBody
	public List<SubjectAjax> getLibraryQuestionBankCourse(@PathVariable("id") int idCourse,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		return questionService.getAllLibraryCourse(a.getId(),idCourse);
	}
	@PostMapping(URL.course_test_update_question)
	@ResponseBody
	public void updateQuestionBankCourse(@PathVariable("id") int idCourse,Authentication auth,@RequestBody Question q) {
		questionService.deleteQuestion(q.getId(),q.getIdSubject());
		questionService.importQuestion(q);
	}
	@PostMapping(URL.course_question_bank_library_update)
	@ResponseBody
	public void updateLibraryQuestionBankCourse(@PathVariable("id") int idCourse,Authentication auth,
			@RequestParam("idSubject") int idSubject,@RequestParam("delete") boolean b) {
		if(b) questionService.removeLibraryCourse(idCourse,idSubject);
		else questionService.addLibraryCourse(idCourse,idSubject);
	}
}

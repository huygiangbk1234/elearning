package com.hust.elearning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.hust.elearning.entity.Account;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.services.QuestionService;
import com.hust.elearning.services.UploadServices;
import com.hust.elearning.util.constants.URL;

@Controller
public class AdminController {

	@Autowired
	AccountService accountService;
	@Autowired
	CourseService courseService;
	@Autowired
	UploadServices uploadService;
	@Autowired
	QuestionService questionService;
	
	@GetMapping(URL.admin_home)
	public String home(Authentication auth,Model model) {
		Account a=accountService.getUserByEmail(auth.getName());
		if(!a.getRole().equals("admin"))return "redirect:/overview";
		model.addAttribute("user",a);
		return "/admin/home";
	}
}

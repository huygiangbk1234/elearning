package com.hust.elearning.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hust.elearning.entity.Document;
import com.hust.elearning.services.UploadServices;

@Controller
public class UploadController {
	@Autowired
	UploadServices uploadService;
	
	@GetMapping("/uploadPDF")
	public String shwowViewUploadPDF(){
		return "file/uploadPDF";
	}
	@PostMapping("/uploadPDF")
	public String  upLoadPDF(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			HttpServletResponse response) throws IOException {
		if(file.isEmpty()){
			return "file/error";
		}
		else{
			
			String nameFile = file.getOriginalFilename();
			byte[] bytes = file.getBytes();
			Path path = Paths.get("D:/pdf/" + nameFile);
			Files.write(path, bytes);
			Document document = new Document();
			document.setName("Bai1");
			document.setType((short) 1);
			document.setIdCreator(1);
			document.setIdCourse(1);
			uploadService.savePDFFile(document);
			return "file/success";
		}
	}
}

package com.hust.elearning.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hust.elearning.dto.NotificationAjax;
import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Document;
import com.hust.elearning.entity.Notification;
import com.hust.elearning.entity.NotificationRecipient;
import com.hust.elearning.entity.Post;
import com.hust.elearning.entity.PostReply;
import com.hust.elearning.services.AccountService;
import com.hust.elearning.services.CourseService;
import com.hust.elearning.services.MessageService;
import com.hust.elearning.services.NotificationService;
import com.hust.elearning.services.PostService;
import com.hust.elearning.util.DateUtils;
import com.hust.elearning.util.constants.Constants;
import com.hust.elearning.util.constants.URL;


@Controller
public class PostController {
	@Autowired
	AccountService accountService;
	@Autowired
	CourseService courseService;
	
	@Autowired
	PostService postService;
	@Autowired
	NotificationService notificationService; 
	@Autowired
	MessageService messageService;
	
	@GetMapping(URL.course_forum_all)
	public String getPostPage(@PathVariable("id") int idCourse,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return null;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		List<Post> list=postService.getAllPost(idCourse);
		model.addAttribute("posts", list);
		return "course/posts";
	}
	@PostMapping(URL.course_forum_pin_post)
	@ResponseBody
	public void pinPost(@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Authentication auth,@RequestParam("priority") int priority) {
		Account a=accountService.getUserByEmail(auth.getName());
		postService.pinPost(idPost,priority);
	}
	@PostMapping(URL.course_forum_delete_post)
	@ResponseBody
	public void deletePost(@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		postService.deletePost(idPost);
	}
	@PostMapping(URL.course_forum_delete_comment)
	@ResponseBody
	public void deleteComment(@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Authentication auth,
			@RequestParam("idPostReply") int idPostReply) {
		postService.deleteComment(idPostReply);
	}
	@PostMapping(URL.course_forum_update_post)
	@ResponseBody
	public void updatePost(@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Authentication auth,
			HttpServletRequest request,@RequestParam("content") String text,@RequestParam("title") String title,
			@RequestParam("fileDelete") ArrayList<Integer> fileDelete) {
		Account a=accountService.getUserByEmail(auth.getName());
		Post p=new Post();
		p.setContent(text);
		p.setTitle(title);
		p.setId(idPost);
		
		postService.updatePost(p);
		if(fileDelete.size()>0)postService.deleteFilePost(fileDelete);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_POST+idPost);
		 if(list.size()>0)postService.createListDocument(list);
	}
	@PostMapping(URL.course_forum_update_comment)
	@ResponseBody
	public void updateComment(@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Authentication auth,
			HttpServletRequest request,@RequestParam("content") String text,@RequestParam("idPostReply") int idPostReply,
			@RequestParam("fileDelete") ArrayList<Integer> fileDelete) {
		Account a=accountService.getUserByEmail(auth.getName());
		PostReply pr=new PostReply();
		pr.setId(idPostReply);
		pr.setContent(text);
		
		postService.updatePostReply(pr);
		if(fileDelete.size()>0)postService.deleteFilePost(fileDelete);
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_POST_REPLY+idPostReply);
		 if(list.size()>0)postService.createListDocument(list);
	}
	@PostMapping(URL.course_forum_send_post)
	@ResponseBody
	public void sendPost(@PathVariable("id") int idCourse,Authentication auth,
			HttpServletRequest request,@RequestParam("content") String text,@RequestParam("title") String title) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		long time=DateUtils.getTime();
		Post p=new Post();
		p.setContent(text);
		p.setTitle(title);
		p.setIdCourse(idCourse);
		p.setPriority(0);
		p.setStatus(1);
		p.setCreator(m);
		p.setCreatedAt(time);
		
		postService.createPost(p);
		int id=p.getId();
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_POST+id);
		 if(list.size()>0)postService.createListDocument(list);
		 
		Notification n = new Notification();
		n.setCreatedAt(time);
		n.setCreator(m);
		n.setParent(Constants.PRE_POST + id);
		notificationService.createNotification(n);
		List<CourseMember> lm = courseService.getAllMember(idCourse);
		NotificationRecipient nr = new NotificationRecipient();
		nr.setCreatedAt(time);
		nr.setNotification(n);
		nr.setRead(false);
		for (CourseMember c : lm) {
			if (c.getId() != m.getId()) {
				nr.setIdAccount(c.getMember().getId());
				notificationService.createNotificationRecipient(nr);
				NotificationAjax na= notificationService.createNotificationPost(id, m, false);
				messageService.sendNotificationRealTime(na, c.getMember().getEmail());
			}
		}
	}
	@PostMapping(URL.course_forum_send_subcomment)
	@ResponseBody
	public void sendSubCommentPost(@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Authentication auth,
			HttpServletRequest request,@RequestParam("content") String text,@RequestParam("parent") int parent) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		long time=DateUtils.getTime();
		PostReply pr=new PostReply();
		pr.setContent(text);
		pr.setCreator(m);
		pr.setIdPost(idPost);
		pr.setParent(parent);
		pr.setStatus(1);
		pr.setCreatedAt(time);
		postService.createPostReply(pr);
		int id=pr.getId();
		List<Document> list=courseService.createDocument(request, idCourse, a.getId(),Constants.PRE_POST_REPLY+id);
		if(list.size()>0)postService.createListDocument(list);
		 
		Notification n = new Notification();
		n.setCreatedAt(time);
		n.setCreator(m);
		n.setParent(Constants.PRE_POST_REPLY + id);
		notificationService.createNotification(n);
		NotificationRecipient nr=new NotificationRecipient();
		nr.setCreatedAt(time);
		nr.setNotification(n);
		nr.setRead(false);
		if(parent!=-1) {
			PostReply p=postService.getPostReplyById(parent);
			CourseMember author=p.getCreator();
			if(author.getId()!=m.getId()) {
				nr.setIdAccount(author.getMember().getId());
				notificationService.createNotificationRecipient(nr);
				NotificationAjax na= notificationService.createNotificationPostReply(id, m, false);
				messageService.sendNotificationRealTime(na, author.getMember().getEmail());
			}
		}else {
			List<CourseMember> lm = courseService.getAllMember(idCourse);
			for (CourseMember c : lm) {
				if (c.getId() != m.getId()) {
					nr.setIdAccount(c.getMember().getId());
					notificationService.createNotificationRecipient(nr);
					NotificationAjax na= notificationService.createNotificationPostReply(id, m, false);
					messageService.sendNotificationRealTime(na, c.getMember().getEmail());
				}
			}
		}
		
	}

	@PostMapping(URL.course_forum_count_comment)
	@ResponseBody
	public int getCountComment(@PathVariable("idPost") int idPost) {
		return postService.countCommentPost(idPost);
	}
	@PostMapping(URL.course_forum_count_subcomment)
	@ResponseBody
	public int getCountSubComment(@RequestParam("idPostReply") int idPostReply) {
		return postService.countSubCommentPost(idPostReply);
	}
	@PostMapping(URL.course_forum_get_comment)
	@ResponseBody
	public List<PostReply> getCommentPost(@PathVariable("idPost") int idPost) {
		List<PostReply> list= postService.getPostReply(idPost);
		for(PostReply pr:list) {
			pr.setFiles(postService.getDocumentPostReply(pr.getId()));
		}
		return list;
	}
	@PostMapping(URL.course_forum_get_subcomment)
	@ResponseBody
	public List<PostReply> getSubCommentPost(@RequestParam("parent") int parent,@RequestParam("from") int from) {
		List<PostReply> list =postService.getSubPostReply(parent,from);
		for(PostReply pr:list) {
			pr.setFiles(postService.getDocumentPostReply(pr.getId()));
		}
		return list;
	}
	
	@GetMapping(URL.course_forum)
	public String getForum(@RequestParam Map<String,String> map,@PathVariable("id") int idCourse,@PathVariable("idPost") int idPost,Model model,Authentication auth) {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		if(check!="") return null;
		Course c=courseService.getCourseById(idCourse);
		model.addAttribute("user", a);
		model.addAttribute("course",c);
		model.addAttribute("member",m);
		
		Post p=postService.getPostById(idPost);
		p.setFiles(postService.getDocumentPost(idPost));
		model.addAttribute("post",p);
		
		if(map.containsKey("idPost")) {
			model.addAttribute("idPost_notification", map.get("idPost"));
		}
		if(map.containsKey("idComment")) {
			model.addAttribute("idComment_notification", map.get("idComment"));
		}
		return "course/post";
	}
//	@GetMapping("custom/document/{name}")
//	@ResponseBody
//	public FileSystemResource  a(HttpServletRequest request) {
//		response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT); response.setHeader("Location", newURL);
//	}
	@GetMapping(URL.course_download_file)
	@ResponseBody
	public FileSystemResource requestdownload(HttpServletResponse response,@PathVariable("id") int idCourse,@RequestParam("name") String name,Authentication auth,Model model) throws FileNotFoundException, IOException {
		Account a=accountService.getUserByEmail(auth.getName());
		CourseMember m=courseService.getMember(a.getId(),idCourse);
		String check=courseService.checkCourse(m, model);
		//if(check!="") return null;
		return new FileSystemResource(new ClassPathResource("static/custom/document/"+name).getFile());
		
	}
}

package com.hust.elearning;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

@SpringBootApplication
//@EnableScheduling
public class ELearningApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        // Customize the application or call application.sources(...) to add sources
	        // Since our example is itself a @Configuration class (via @SpringBootApplication)
	        // we actually don't need to override this method.
	        return application;
	    }
	public static void main(String[] args) {
		/* String password = "123456"; 
		 org.springframework.security.crypto.password.PasswordEncoder encoder
		   = new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();  
		    System.out.print(encoder.encode("123456"));*/
		 configureApplication(new SpringApplicationBuilder()).run(args);
		//SpringApplication.run(ELearningApplication.class, args);
	}
	private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
        return builder.sources(ELearningApplication.class).bannerMode(Banner.Mode.OFF);
    }
}

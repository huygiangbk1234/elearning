package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.hust.elearning.entity.GroupCourse;

@Mapper
public interface GroupCourseRepository {
	public void createGroupCourse(GroupCourse g);
	public void updateNameGroup(@Param("id") int id,@Param("name") String name);
	public void deleteGroupCourse(@Param("id") int id);
	public List<GroupCourse> getGroupCourseOfCourse(@Param("idCourse") int idCourse);
	public GroupCourse getGroupCourseById(@Param("id")int id);
}

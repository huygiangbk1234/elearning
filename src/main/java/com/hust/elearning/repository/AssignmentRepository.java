package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.hust.elearning.entity.Assignment;
import com.hust.elearning.entity.AssignmentSubmit;
import com.hust.elearning.entity.CourseMember;

@Mapper
public interface AssignmentRepository {
	public void createAssignment(Assignment a);
	public void createAssignmentSubmit(AssignmentSubmit a);
	public void updateAssignment(Assignment a);
	public void updateLessonAssignment(@Param("id") int id,@Param("idLesson") int idLesson);
	public void updateAssignmentSubmit(@Param("id") int id,@Param("updatedAt") long updatedAt);
	public void updateTimeEndAssignment(@Param("id") int id,@Param("timeEnd") long timeEnd);
	
	public void deleteAssignment(@Param("id") int id);
	public List<Assignment> getAssignmentBeforeTime(@Param("idCourse") int idCourse,@Param("time") long time);
	public List<Assignment> getAssignmentOfLesson(@Param("idLesson") int idLesson);
	public AssignmentSubmit getAssignmentSubmitOfUser(@Param("idAssignment") int idAssignment,@Param("idMember") int idMember);
	public List<AssignmentSubmit> getUserSubmitAssignment(@Param("idAssignment") int idAssignment);
	public List<CourseMember> getUserNotSubmitAssignment(@Param("idAssignment") int idAssignment,@Param("idCourse") int idCourse);
	public void getUserSubmitAssignmentBeforeTime(@Param("idAssignment") int idAssignment,@Param("time") long time);
	public void getUserSubmitAssignmentAfterTime(@Param("idAssignment") int idAssignment,@Param("time") long time);
	public List<Assignment> getAssignmentOfCourse(@Param("idCourse")int idCourse);
	public Assignment getAssignmentById(@Param("id")int idAssignment);
	public AssignmentSubmit getAssignmentSubmitById(@Param("id") int root);
	public List<AssignmentSubmit> getAssignmentSubmitOfUserInCourse(@Param("idAssignment")int idAssignment,@Param("idMember") int idMember);
	public List<Assignment> getAssignmentAfterTime(@Param("idCourse")int idCourse,@Param("time") long time);
	public List<Assignment> getAssignmentBetweenTime(@Param("idCourse")int idCourse,@Param("time1") long time1,@Param("time2") long time2);
}

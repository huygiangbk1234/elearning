package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hust.elearning.entity.Account;

@Mapper
public interface AccountRepository {

	public void createAccount(Account a);

	public void updateAccount(Account a);

	public Account getUserById(@Param("id")int id);
	
	@ResultMap("AccountEntryMapping")
    @Select("SELECT * FROM account WHERE email= #{email}")
	public Account getUserByEmail(@Param("email")String email);
	
	@Update("update account set password='$2a$10$bu.4RyjMvfIddPyM9V.bV.L5izzh8urGhWNaj2Sk0kthHuRTqpUx6'")
	public void test();

	public Account getUserByName(Account a);

	public List<Account> filterUserByEmail(@Param("email") String email);
	
	@Update("update account set last_course=#{idCourse} where id=#{id}")
	public void updateLastCourse(@Param("id")int id,@Param("idCourse") int idCourse);

}

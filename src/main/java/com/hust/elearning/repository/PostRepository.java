package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.hust.elearning.entity.Post;
import com.hust.elearning.entity.PostReply;

@Mapper
public interface PostRepository {
	
	public void createPost(Post p);
	public void createPostReply(PostReply p);
	public Object countCommentPost(@Param("idPost") int idPost);
	public Object countSubCommentPost(@Param("parent") int parent);
	public List<Post> getPostOfCourse(@Param("idCourse") int idCourse);
	public Post getPostById(@Param("idPost") int idPost);
	public List<PostReply> getPostReply(@Param("idPost") int idPost);
	public List<PostReply> getSubPostReply(@Param("parent") int parent,@Param("from") int from);
	public void updatePost(Post p);
	public void updatePostReply(PostReply pr);
	public void delete(@Param("idPost")int idPost);
	public void deleteComment(@Param("idPostReply")int idPost);
	public void pinPost(@Param("idPost")int idPost,@Param("priority") int priority);
	public Object countCommentOfUser(@Param("idMember")int idMember);
	public Object countPostOfUser(@Param("idMember")int idMember);
	public PostReply getPostReplyById(@Param("id")int idPostReply);
}

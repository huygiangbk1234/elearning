package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.Subject;

@Mapper
public interface CourseRepository {

	@ResultMap("CourseMapping")
	public Course getCourseById(int id);

	public void createCourse(Course c);

	public void updateCourse(@Param("c") Course c);

	public void deleteCourse(int id);

}

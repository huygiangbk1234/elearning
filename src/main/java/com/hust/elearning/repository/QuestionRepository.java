package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hust.elearning.entity.Question;
import com.hust.elearning.entity.Subject;

@Mapper
public interface QuestionRepository {
	@ResultMap("QuestionMapping")
	@Select("select * from question q where q.id=#{idQuestion}")
	public Question getQuestionById(@Param("idQuestion") int idQuestion);
	public List<Integer> getListIdQuestion(@Param("idSubject") int idSubject,@Param("type") int type);
	public int getTotalQuestion(@Param("idSubject") int idSubject,@Param("type") int type);
	
	@ResultMap("QuestionShortMapping")
	@Select("select * from question q where q.subject_id=#{idSubject} and q.question like #{name} limit 10")
	public List<Question> getListQuestionLikeName(@Param("idSubject") int idSubject,@Param("name")String name);
	public List<Question> getListQuestion(@Param("list")List<Integer> list);

	@Options(useGeneratedKeys=true,keyProperty="id")
	@Insert("insert into subject (name,account_id,total) values(#{name},#{idAccount},#{total})")
	public int insertSubject(Subject s);
	@Options(useGeneratedKeys=true,keyProperty="id")
	@Insert("insert into course_subject (course_id,subject_id) values(#{idCourse},#{idSubject})")
	public int insertSubjectCourse(@Param("idCourse") int idCourse,@Param("idSubject") int idSubject);
	
	public int updateTotalSubject(@Param("id")int id);
	public int updateTotalDescSubject(@Param("id")int id);
	public void createQuestion(Question q);
	public void createAnswer(@Param("q")Question q);

	@ResultMap("SubjectMapping")
	@Select("select s.* from subject s,course_subject c where s.id=c.subject_id and c.course_id=#{idCourse}")
	public List<Subject> getSubjectOfCourse(@Param("idCourse") int idCourse);
	
	@Select("select s.id from subject s,course_subject c where s.id=c.subject_id and c.course_id=#{idCourse}")
	public List<Integer> getListIdSujectOfCourse(@Param("idCourse") int idCourse);
	
	@ResultMap("SubjectMapping")
	@Select("select * from subject where account_id=#{idAccount}")
	public List<Subject> getListSubjectOfUser(@Param("idAccount") int idAccount);
	
	@Delete("delete from course_subject where course_id=#{idCourse} and subject_id=#{idSubject}")
	public void deleteSubjectCourse(@Param("idCourse") int idCourse,@Param("idSubject") int idSubject);
	@Delete("delete from question where id=#{id}")
	public void deleteQuestionById(@Param("id")int id);
	@ResultMap("SubjectMapping")
	@Select("select * from subject where name like #{name}")
	public Subject getSubjectVideo(@Param("name") String name);
	
	@ResultMap("QuestionMapping")
	@Select("select * from question q where q.subject_id=#{idSubject} and q.question like #{name} limit 10")
	public List<Question> getListQuestionLikeNameFull(@Param("idSubject") int idSubject,@Param("name")String name);
	@Update("update subject set name=#{name} where id=#{idSubject}")
	public void updateNameSubject(@Param("idSubject")int idSubject,@Param("name") String name);
}

package com.hust.elearning.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.hust.elearning.entity.Document;
import com.hust.elearning.entity.ViewVideo;

@Mapper
public interface DocumentRepository {
	
	@ResultMap("DocumentMapping")
	@Insert(value = "INSERT INTO document (id,link,name,type,status,account_id,course_id) VALUES(1,#{link},#{name},#{type},#{status},#{idCreator},#{idCourse})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void savePDFFile(Document document);
	
	@Select("Select count(*) from document where account_id=#{id} group by account_id" )
	public Object countDocumentUser(int id);
	
	public void uploadListDocument(@Param("list")List<Document> list);
	public List<Document> getDocumentInParent(@Param("parent") String parent);
	public List<Document> getVideoInParent(@Param("parent") String parent);
	public List<Document> getDocumentLikeParent(@Param("idCourse") int idCourse,@Param("parent") String parent);

	public void deleteListFileById(@Param("list")ArrayList<Integer> fileDelete);

	public void deleteFileByParent(@Param("parent")String parent);

	public Document getVideoById(@Param("id")int id);

	public List<Document> getVideoOfCourse(@Param("idCourse")int idCourse);
	
	public ViewVideo getViewVideoOfUser(@Param("idVideo") int idVideo,@Param("idMember") int idMember);
	public void createViewVideo(ViewVideo v);
	public void updateLengthViewVideo(@Param("id") int id,@Param("length") String length);
	public void updateAnswersViewVideo(@Param("id") int id,@Param("answers") String answers);

	@ResultMap("DocumentMapping")
	@Select("Select * from document where account_id=#{idUser} and type=1" )
	public List<Document> getDocumentOfUser(@Param("idUser") int id);
	@ResultMap("DocumentMapping")
	@Select("Select * from document where account_id=#{idUser} and type=2" )
	public List<Document> getVideoOfUser(@Param("idUser") int id);
}

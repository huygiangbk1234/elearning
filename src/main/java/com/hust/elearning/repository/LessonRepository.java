package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.hust.elearning.entity.Lesson;

@Mapper
public interface LessonRepository {
	public void createLesson(Lesson l);
	public void updateIndexLesson(@Param("id") int id,@Param("pos") int pos);
	public void updateNameLesson(@Param("id") int id,@Param("name") String name);
	public void deleteLesson(@Param("id") int id);
	public List<Lesson> getLessonOfCourse(@Param("idCourse") int idCourse);
}

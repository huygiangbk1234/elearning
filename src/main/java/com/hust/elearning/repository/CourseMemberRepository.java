package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;

@Mapper
public interface CourseMemberRepository {

	public List<Integer> getAllCourseIdOfUser(int id);

	@ResultMap("MemberMapping")
	public List<CourseMember> getAllMemberOfCourse(@Param("idCourse") int id);

	public void createMember(CourseMember m);

	public void updateMember(CourseMember m);

	public void deleteMember(@Param("id")int id);
	
	@ResultMap("com.hust.elearning.repository.CourseRepository.CourseMapping")
	@Select("Select c.* from course_member m RIGHT JOIN course c  on m.course_id=c.id where m.account_id=#{id} and m.status=1")
	public List<Course> countAllCourseOfUser(int id);
	
	@Select("Select count(*) from course_member where course_id=#{idCourse} and role='member' group by course_id")
	public int countAllMemberOfCourse(@Param("idCourse") int idCourse);
	
	public CourseMember getMember(@Param("idUser") int idUser,@Param("idCourse") int idC);

	@ResultMap("MemberMapping")
	public List<CourseMember> getSomeMember(@Param("idCourse") int idCourse,@Param("from") int from,@Param("limit") int limit);
	@ResultMap("MemberMapping")
	public List<CourseMember> getMemberLikeName(@Param("idCourse") int idCourse,@Param("name") String name);

	public void joinGroup(@Param("id")int id,@Param("idGroup") int idGroup);

	public void deleteGroup(@Param("idGroup")int idGroup);

	@Update("update course_member set alias=#{alias} where id=#{id}")
	public void updateAlias (@Param("id")int id,@Param("alias") String alias);

	@ResultMap("MemberMapping")
	@Select("Select * from course_member where alias like #{alias} limit 10")
	public List<CourseMember> filterMemberByAlias(@Param("alias")String string);
	
}

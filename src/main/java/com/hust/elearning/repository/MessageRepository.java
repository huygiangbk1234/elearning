package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hust.elearning.dto.MessageUpdateAjax;
import com.hust.elearning.entity.Message;
import com.hust.elearning.entity.MessageRecipient;

@Mapper
public interface MessageRepository {
	public int createMessage(Message m);
	public int createMessageRecipient(MessageRecipient mr);
	
	@Update("update message_recipient set is_read=true,updated_at=#{time} where message_id in (select id from message where group_id=#{idGroup}) and is_read=false and account_id=#{idUser}")
	public int updateReadMessage(@Param("idGroup") int idGroup,@Param("idUser") int idUser,@Param("time") long time);
	
	@ResultMap("MessageEntryMapping")
	@Select("select * from message where group_id=#{idGroup} ORDER BY created_at DESC LIMIT #{from},#{limit}")
	public List<Message> getSomeMessage(@Param("idGroup") int idGroup,@Param("from") int from,@Param("limit") int limit);	
}

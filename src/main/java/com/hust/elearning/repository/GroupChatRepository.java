package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.GroupChat;
import com.hust.elearning.entity.GroupChatMember;

@Mapper
public interface GroupChatRepository {
	public int createGroup(GroupChat g);
	public int createGroupMember(GroupChatMember gm);
	public int updateGroupChatMember(GroupChatMember gc);
	@Update("update group_chat set updated_at=#{time} where id=#{idGroup}")
	public void updateTimeGroup(@Param("time") long time,@Param("idGroup") int idGroup);
	@Update("update group_chat_member set is_hide=#{isHide} where group_chat_id=#{idGroup} and account_id=#{idUser}")
	public void updateMemberGroupHide(@Param("isHide") boolean b,@Param("idGroup") int idGroup,@Param("idUser") int idUser);
	@Update("update group_chat_member set is_read=#{isRead} where group_chat_id=#{idGroup} and account_id=#{idUser}")
	public void updateMemberGroupRead(@Param("isRead") boolean b,@Param("idGroup") int idGroup,@Param("idUser") int idUser);
	@Select("select is_read from group_chat_member where account_id = #{idUser} and group_chat_id=#{idGroup}")
	public Boolean isReadGroup(@Param("idGroup") int idGroup,@Param("idUser") int idUser);
	//lấy các thành viên 
	@ResultMap("com.hust.elearning.repository.AccountRepository.AccountEntryMappingMessage")
	@Select("select a.id,a.email,a.fullname,a.nickname,a.avatar from account a left join group_chat_member g on a.id=g.account_id where g.group_chat_id=#{idGroup}")
	public List<Account> getMemberGroup(@Param("idGroup") int idGroup);
	
	@ResultMap("GroupChatEntryMapping")
	@Select("select * from group_chat where id = #{id}")
	public GroupChat getGroupById(@Param("id") int id);
	
	@ResultMap("GroupChatEntryMapping")
	@Select("SELECT gc.* FROM mydatabase.group_chat gc,mydatabase.group_chat_member g1 , mydatabase.group_chat_member g2 "
			+ " where  g1.group_chat_id=g2.group_chat_id and g1.group_chat_id=gc.id and"
			+ " g1.account_id=#{idUser1} and g2.account_id=#{idUser2} and gc.is_private=1;")
	public GroupChat getExistGroupPrivate(@Param("idUser1") int idUser1,@Param("idUser2") int idUser2);

	@Select("select id from group_chat_member where account_id = #{idUser} and group_chat_id=#{idGroup}")
	public Object isMemberOfGroup(@Param("idUser") int idUser,@Param("idGroup") int idGroup);

	// Lấy các group hiện của user
	@ResultMap("GroupChatEntryMapping")
	@Select("select gc.* from group_chat gc, group_chat_member gm where gc.id=gm.group_chat_id and gm.account_id = #{idUser} and gm.is_hide=0 ORDER BY gc.updated_at DESC LIMIT #{from},#{limit}")
	public List<GroupChat> getSomeGroupChat(@Param("idUser") int idUser,@Param("from") int from,@Param("limit") int limit);
}

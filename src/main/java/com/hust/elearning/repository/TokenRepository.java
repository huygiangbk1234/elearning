package com.hust.elearning.repository;

import java.util.Date;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.hust.elearning.entity.TokenEntity;

@Mapper
public interface TokenRepository {
	@Insert(value = "INSERT INTO resettoken (expiryDate,token,account_id) VALUES(#{expiryDate},#{token},#{idAccount})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void save(TokenEntity token);
	
	@ResultMap("TokenMapping")
	@Select("SELECT * FROM resettoken WHERE token = #{token}")
	public TokenEntity findByToken(@Param("token")String token);

	@Delete("DELETE FROM resettoken where id = #{id}")
	public void deleteByToken(@Param("id")long id);
	
	@Delete("DELETE FROM resettoken where expiryDate <= #{now}")
	public void deleteByExpiryDate(long now);

	@Delete("DELETE FROM resettoken")
	public void deleteAll();
}
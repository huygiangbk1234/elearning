package com.hust.elearning.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.hust.elearning.entity.Notification;
import com.hust.elearning.entity.NotificationRecipient;

@Mapper
public interface NotificationRepository {
	public void createNotification(Notification n);
	public void createNotificationRecipient(NotificationRecipient nr);
	public List<NotificationRecipient> getNotificationOfUser(@Param("idAccount") int idAccount,@Param("from") int from);
	public void updateReadNotification(@Param("idAccount") int idAccount);
}

package com.hust.elearning.endpoint;


import org.nextrtc.signalingserver.NextRTCConfig;
import org.nextrtc.signalingserver.domain.InternalMessage;
import org.nextrtc.signalingserver.domain.Signal;
import org.nextrtc.signalingserver.domain.SignalResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;
@Configuration
@Import(NextRTCConfig.class)
public class EndpointConfig {
	@Bean
    public MyEndPoint myEndpoint() {
        return new MyEndPoint();
    }
	@Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
//	 @Autowired
//	    private SignalResolver resolver;
//	    @Bean
//	    public Signal addCustomNextRTCHandlers(){
//	        Signal upperCase = Signal.fromString("upperCase");
//	        resolver.addCustomSignal(upperCase, (msg)-> {
//	        	System.out.println(msg.getContent().toUpperCase()+"A");
//	            InternalMessage.create()
//	                    .to(msg.getFrom())
//	                    .content(msg.getContent().toUpperCase())
//	                    .signal(upperCase)
//	                    .build()
//	                    .send();
//	        });
//	        return upperCase;
//	    }
}

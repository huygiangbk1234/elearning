package com.hust.elearning.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtils {
	public static String randomName(String ext) {
		int n=(int)(Math.random()*100);
		return n+"_"+getTime()+"."+ext;
	}
	public static long getTime() {
		return new Date().getTime();
	}
	public static String covertDate(long i,String f) {
		Date d=new Date(i);
		return new SimpleDateFormat(f).format(d);
	}
	public static int calPercent(long from, long to) {
		long n=new Date().getTime();
		long delta=to-from;
		int p=delta>0?(int)((n-from)*100.0/delta):0;
		return p>100?100:p;
	}
	public static List<Date> getDayOfMonth(int month,int year) {

		List<Date> dates = null;
		Calendar time = null;
		boolean isInSameMonth = true;

		dates = new ArrayList<Date>();
		time = Calendar.getInstance();
        int currentMonth = time.get(Calendar.MONTH) + 1;
        
		if (month == currentMonth) {
			time.setTime(new Date());
		}else{
			time.set(Calendar.DAY_OF_MONTH, 1);
			time.set(Calendar.MONTH,month-1);
			time.set(Calendar.YEAR, year);
		}

		while (isInSameMonth) {
			dates.add(time.getTime());
			time.add(Calendar.DAY_OF_MONTH, 1);
			isInSameMonth = (month == (time.get(Calendar.MONTH)+1)) ? true : false;
		}
		return dates;
	}

	
}

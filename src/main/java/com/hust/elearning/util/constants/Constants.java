package com.hust.elearning.util.constants;

public class Constants {
	public static final String PATH_FILE_UPLOAD= "src/main/resources/static/custom/document/";
	
	public static final String RESP_STATUS_SUCCESS = "SUCCESS";
	public static final String RESP_STATUS_ERROR = "ERROR";
	public static final int TOKEN_EXPIRATION = 10;
	public static final int USER_STATUS_INACTIVE = 0;
	public static final int USER_STATUS_ACTIVE = 1;
	public static final int USER_STATUS_PENDING_APPROVAL = 2;
	
	
	public static final int LIMIT_ROW_MEMBER=10;
	public static final int LIMIT_ROW_MESSAGE=15;
	public static final int LIMIT_ROW_GROUP_CHAT=10;
	
	public static final int LIMIT_NUMS_QUESTION_TEST=100;
	public static final int MIN_NUMS_QUESTION_TEST=10;
	
	public static final String PRE_POST="post-";
	public static final String PRE_POST_REPLY="post_reply-";
	public static final String PRE_ASSIGNMENT="assigment-";
	public static final String PRE_ASSIGNMENT_SUBMIT="assigment_submit-";
	public static final String PRE_LESSON="lesson-";

	public static final String PRE_VIDEO = "-video";
}

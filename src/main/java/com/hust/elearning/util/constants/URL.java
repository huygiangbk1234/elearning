package com.hust.elearning.util.constants;

public class URL {
	
	
	public static final String logout="/logout";
	public static final String register="/register";
	public static final String register_confirm="/account/verify";
	public static final String forgot_password="/password/forgot";
	public static final String change_password="/password/change";
	public static final String verify_account="/token/verify";
	
	
	public static final String home="/home";
	public static final String about="/about";
	public static final String contact="/contact";
	public static final String search="/search";
	
	public static final String user_home="/overview";
	public static final String user_video="/user/video";
	public static final String user_file="/user/file";
	public static final String user_course="/user/course";
	public static final String user_course_create="/user/course/create";
	public static final String user_profile="/user/profile";
	public static final String user_account="/user/profile/account";
	public static final String user_update="/user/profile/update";
	public static final String user_update_alias="/user/profile/update/alias";
	public static final String user_question_bank="/user/question_bank";
	
	public static final String user_filter_by_email="/user/filter";
	public static final String user_filter_by_alias="/member/filter";
	
	public static final String course_home="/course/{id}";
	public static final String course_update="/course/{id}/update";
	public static final String course_member_all="/course/{id}/members";
	public static final String course_member_page="/course/{id}/members/page/{p}";
	public static final String course_member="/course/{id}/member";
	public static final String course_add_member="/course/{id}/addMember";
	public static final String course_add_list_member="/course/{id}/addListMember";
	public static final String course_del_member="/course/{id}/members/delete";
	public static final String course_filter_get_user_by_name="/course/{id}/members/filter";
	
	
	public static final String course_filter_get_question_by_name="/course/{id}/questions/filter";
	public static final String course_get_question="/course/{id}/question/get";
	public static final String course_question_bank="/course/{id}/question_bank";
	public static final String course_forum_all="/course/{id}/forums";
	public static final String course_question_bank_library = "/course/{id}/question_bank/library";
	public static final String course_question_bank_library_update = "/course/{id}/question_bank/updatelibrary";
	public static final String course_test_update_question="/course/{id}/question_bank/updatequestion";
	public static final String course_test_delete_question="/question/delete";
	public static final String course_subject_create="/subject/create";
	public static final String course_subject_update="/subject/update";
	public static final String course_subject_import="/subject/import";
	
	public static final String course_video="/course/{id}/video";
	public static final String course_video_create="/course/{id}/video/create";
	public static final String course_video_submit="/course/{id}/video/submit";
	public static final String course_video_submit_question="/course/{id}/video/submit/question";
	public static final String course_video_update_length="/course/{id}/video/{idViewVideo}/update/length";
	public static final String course_video_update_answers="/course/{id}/video/{idViewVideo}/update/answers/{a}";
	public static final String course_video_update_questions="/course/{id}/video/{idSubject}/update/questions";
	public static final String course_file="/course/{id}/file";
	
	public static final String course_assignment="/course/{id}/assignments";
	public static final String course_assignment_get_notsubmit="/course/{id}/assignments/get/notsubmit";
	public static final String course_assignment_get="/course/{id}/assignments/get";
	public static final String course_assignment_create_page="/course/{id}/assignments/create/page";
	public static final String course_assignment_create="/course/{id}/assignments/create";
	public static final String course_assignment_page="/course/{id}/assignment/{idAssignment}";
	public static final String course_assignment_update="/course/{id}/assignment/{idAssignment}/update";
	public static final String course_assignment_delete = "/course/{id}/assignment/{idAssignment}/delete";
	public static final String course_assignment_submit = "/course/{id}/assignment/{idAssignment}/submit";
	public static final String course_assignment_submit_update = "/course/{id}/assignment/{idAssignmentSubmit}/submit/update";
	
	public static final String course_group="/course/{id}/groups";
	public static final String course_group_create="/course/{id}/groups/create";
	public static final String course_group_update="/course/{id}/groups/update";
	public static final String course_group_member_update="/course/{id}/groups/updatemember";
	public static final String course_group_delete="/course/{id}/groups/delete";
	public static final String course_group_add="/course/{id}/groups/addmember";
	public static final String course_group_add_list="/course/{id}/groups/{idGroup}/addlist";
	public static final String course_group_del="/course/{id}/groups/deletemember";
	
	public static final String course_test="/course/{id}/test";
	public static final String course_test_getlistquestion="/course/{id}/test/get_question";
	
	public static final String upload="/upload";
	public static final String download="/download/{idCousre}";
	
	public static final String get_group_chat_single="/groupchat/get";
	public static final String get_group_chat="/groupchat/get/{p}";
	public static final String message_recipient_read="/message/read";
	public static final String send_message="/message/send";
	public static final String get_message="/message/get/{idGroup}/{p}";
	
	public static final String get_notification="/notification/get";
	public static final String get_event="/events/get";
	
	public static final String course_forum="/course/{id}/forum/{idPost}";
	public static final String course_forum_count_comment="/course/{id}/forum/{idPost}/count_commemt";
	public static final String course_forum_count_subcomment="/course/{id}/forum/{idPost}/count_subcommemt";
	public static final String course_forum_get_comment="/course/{id}/forum/{idPost}/get_commemt";
	public static final String course_forum_get_subcomment="/course/{id}/forum/{idPost}/get_subcommemt";
	public static final String course_forum_send_subcomment="/course/{id}/forum/{idPost}/send_subcommemt";
	public static final String course_forum_send_post="/course/{id}/forums/send_post";
	public static final String course_forum_update_comment="/course/{id}/forum/{idPost}/update_commemt";
	public static final String course_forum_update_post="/course/{id}/forums/{idPost}/update_post";
	public static final String course_forum_delete_comment="/course/{id}/forum/{idPost}/delete_commemt";
	public static final String course_forum_delete_post="/course/{id}/forums/{idPost}/delete_post";
	public static final String course_forum_pin_post="/course/{id}/forums/{idPost}/pin_post";
	
	public static final String course_lessons="/course/{id}/lessons";
	public static final String course_lesson_get_all="/course/{id}/lesson/get";
	public static final String course_lesson_get_info="/course/{id}/lesson/{idLesson}/get_info";
	public static final String course_lesson_create="/course/{id}/lesson/create";
	public static final String course_lesson_update="/course/{id}/lesson/{idLesson}/update";
	public static final String course_lesson_update_pos="/course/{id}/lesson/{idLesson}/updatepos";
	public static final String course_lesson_update_name="/course/{id}/lesson/{idLesson}/updatename";
	public static final String course_lesson_video_open="/course/{id}/lesson/{idLesson}/video/{idVideo}";
	public static final String course_download_file="/course/{id}/download";
	
	public static final String course_progress="/course/{id}/progress";
	public static final String course_progress_detail_user="/course/{id}/progress/detail";
	public static final String course_progress_detail="/course/{id}/progress/detail/{idMember}";
	public static final String course_score="/course/{id}/score";
	
	public static final String admin_home="/admin/home";
}

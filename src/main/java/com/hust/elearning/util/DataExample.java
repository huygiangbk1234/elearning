package com.hust.elearning.util;

import java.util.ArrayList;
import java.util.List;


import com.hust.elearning.entity.Account;
import com.hust.elearning.entity.Course;
import com.hust.elearning.entity.CourseMember;
import com.hust.elearning.entity.Post;
import com.hust.elearning.entity.PostReply;
import com.hust.elearning.entity.Question;
import com.hust.elearning.entity.QuestionAnswer;
import com.hust.elearning.entity.Subject;

public class DataExample {
	public static String pass="$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu";

	public static List<Account> getAccount() {
		List<Account> list=new ArrayList<Account>();
		for(int i=0;i<50;i++) {
			Account a=new Account();
			a.setPassword(pass);
			a.setEmail("huygiang"+i+"@gmail.com");
			a.setAvatar("user.png");
			a.setTel("09783560"+i);
			long time=DateUtils.getTime();
			a.setCreatedAt(time);
			a.setStatus(1);
			a.setNickname("HuyGiang "+i);
			a.setRole("user");
			a.setUpdatedAt(time);
			if(i<25) a.setFullname("Nguyễn Văn "+String.valueOf((char)(i+65)).toUpperCase());
			else a.setFullname("Nguyễn Huy "+String.valueOf((char)(i+30)).toUpperCase());
			list.add(a);
		}
		return list;
	}
	public static List<CourseMember> getMember(){
		 List<CourseMember> list=new ArrayList<CourseMember>();
		 //103 142;
		 
		 //1 22
		 for(int i=1;i<23;i++) {
			 ArrayList<Integer> arr=new ArrayList<>();
			 int n=(int)(Math.random()*10)+10;
			 for(int j=0;j<n;){
				 int id=(int)(Math.random()*39+103);
				 if(arr.contains(id))continue;
				 arr.add(id);
				 j++;
			 }
			 int k=0;
			 for(int j:arr) {
				 CourseMember c=new CourseMember();
				 c.setIdCourse(i);
				 c.setAlias("Nguyễn Văn "+String.valueOf((char)(k+65)).toUpperCase());
				 c.setRole("member");
				 c.setStatus(1);
				 c.setMember(new Account(j));
				 k++;
				 list.add(c);
			 }
		 }
		 return list;
	}
	public static List<Course> getCourse() {
		List<Course> list=new ArrayList<Course>();
		for(int i=0;i<20;i++) {
			long time=DateUtils.getTime();
			Course c=new Course();
			c.setIdCreator(93+(int)(Math.random()*10));
			c.setName("Khóa học "+String.valueOf((char)(i+65)).toUpperCase()+ " -Năm Học 20"+(i+10));
			c.setInfo("Miêu tả về khóa học!");
			c.setPrivate(Math.random()*10>3?false:true);
			int n=(int)(Math.random()*10);
			if(n<3) n=-1;
			else if(n<5)n=0;
			else n=1;
			c.setStatus(n);
			long t=time+(long)(Math.random()*1000*60*60*24*20);
			c.setTimeStart(t);
			t+=1000*60*60*24*30;
			c.setTimeEnd(t);
			list.add(c);
		}
		return list;
	}
	
	public static List<Subject> getSubject() {
		List<Subject> list=new ArrayList<>();
		for(int i=0;i<3;i++) {
			Subject s=new Subject();
			s.setName("Môn "+String.valueOf((char)(i+65)).toUpperCase());
			s.setIdAccount(93+(int)(Math.random()*10));
			s.setTotal(0);
			list.add(s);
		}
		return list;
	}
	public static List<Question> getQuestion(int idSubject){
		List<Question> list=new ArrayList<>();
		for(int i=0;i<100;i++) {
			Question q=new Question();
			q.setIdSubject(idSubject);
			q.setQuestion("Câu hỏi dễ số "+i);
			q.setType(0);
			
			List<QuestionAnswer> l=new ArrayList<>();
			int n=Math.random()*3>1?1:2;
			for(int j=1;j<5;j++) {
				QuestionAnswer a=new QuestionAnswer();
				a.setAnswer("Đáp án thứ "+j);
				if(j==1)a.setTrue(true);
				else a.setTrue(false);
				if(n==2) if(j==2) a.setTrue(true);
				l.add(a);
			}
			q.setAnswers(l);
			list.add(q);
		}
		for(int i=0;i<100;i++) {
			Question q=new Question();
			q.setIdSubject(idSubject);
			q.setQuestion("Câu hỏi trung bình số "+i);
			q.setType(1);
			int n=Math.random()*3>1?1:2;
			List<QuestionAnswer> l=new ArrayList<>();
			for(int j=1;j<5;j++) {
				QuestionAnswer a=new QuestionAnswer();
				a.setAnswer("Đáp án thứ "+j);
				if(j==1)a.setTrue(true);
				else a.setTrue(false);
				if(n==2) if(j==2) a.setTrue(true);
				l.add(a);
			}
			q.setAnswers(l);
			list.add(q);
		}
		for(int i=0;i<100;i++) {
			Question q=new Question();
			q.setIdSubject(idSubject);
			q.setQuestion("Câu hỏi khó số "+i);
			q.setType(2);
			int n=Math.random()*3>1?1:2;
			List<QuestionAnswer> l=new ArrayList<>();
			for(int j=1;j<5;j++) {
				QuestionAnswer a=new QuestionAnswer();
				a.setAnswer("Đáp án thứ "+j);
				if(j==1)a.setTrue(true);
				else a.setTrue(false);
				if(n==2) if(j==2) a.setTrue(true);
				l.add(a);
			}
			q.setAnswers(l);
			list.add(q);
		}
		return list;
	}
	/*		Create data     
	List<Post> list=DataExample.getPost();
	for(Post p:list) {
		
		postService.createPost(p);
		List<PostReply> l=DataExample.getListPostReply(p.getId(),-1);
		for(PostReply pr : l) {
			postService.createPostReply(pr);
			List<PostReply> sub=DataExample.getListPostReply(p.getId(),pr.getId());
			for(PostReply s : sub) {
				postService.createPostReply(s);
			}
		}
	}*/
	
	public static List<Post> getPost(){
		List<Post> list=new ArrayList<Post>();
		String[] titles= {
				"Hỏi đáp bài tập số 2",
				"Chủ đề thảo luận số 2",
				"Chủ đề được ghim ",
		};
		int[] id= {33,124,112};
		for(int i=1;i<4;i++) {
			for(int j=0;j<3;j++) {
				Post p=new Post();
				p.setTitle(titles[j]);
				p.setCreatedAt(DateUtils.getTime());
				p.setCreator(new CourseMember(id[(int)(Math.random()*id.length)]));
				p.setContent("Nội dung thảo luận ... có thể đính kèm file...");
				if(j==2)p.setPriority(1);
				else p.setPriority(0);
				p.setStatus(1);
				p.setIdCourse(i);
				list.add(p);
			}
		}
		return list;
	}
	public static List<PostReply> getListPostReply(int idPost,int idParent) {
		int[] id= {33,124,112};
		List<PostReply> l=new ArrayList<PostReply>();
		int n=(int)(Math.random()*4)+2;
		for(int k=1;k<n;k++) {
			PostReply pr=new PostReply();
			pr.setContent("Bình luận số "+k);
			pr.setCreator(new CourseMember(id[(int)(Math.random()*id.length)]));
			pr.setParent(idParent);
			pr.setIdPost(idPost);
			pr.setStatus(1);
			pr.setCreatedAt(DateUtils.getTime());
			l.add(pr);
		}
		return l;
	}
}

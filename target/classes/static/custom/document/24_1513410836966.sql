-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mydatabase
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0: verifi 1:active 2:lock',
  `fullname` varchar(45) DEFAULT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `last_role` varchar(45) DEFAULT NULL,
  `last_course` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (33,'huygiang1995@gmail.com','$2a$10$bu.4RyjMvfIddPyM9V.bV.L5izzh8urGhWNaj2Sk0kthHuRTqpUx6',1512151804784,1512151804784,1,'Nguyen Huy Giang','Giang Bk','user.png','0978356099','user','0',1),(34,'huygiang1994@gmail.com','$2a$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512151804784,1512151804784,1,'Nguyen Huy Giang 1','Giang Bk 1','user.png','0978356099','user','0',0),(93,'huygiang0@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn A','HuyGiang 0','user.png','097835600','user',NULL,0),(94,'huygiang1@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn B','HuyGiang 1','user.png','097835601','user',NULL,0),(95,'huygiang2@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn C','HuyGiang 2','user.png','097835602','user',NULL,0),(96,'huygiang3@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn D','HuyGiang 3','user.png','097835603','user',NULL,0),(97,'huygiang4@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn E','HuyGiang 4','user.png','097835604','user',NULL,0),(98,'huygiang5@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn F','HuyGiang 5','user.png','097835605','user',NULL,0),(99,'huygiang6@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn G','HuyGiang 6','user.png','097835606','user',NULL,0),(100,'huygiang7@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn H','HuyGiang 7','user.png','097835607','user',NULL,0),(101,'huygiang8@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn I','HuyGiang 8','user.png','097835608','user',NULL,0),(102,'huygiang9@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn J','HuyGiang 9','user.png','097835609','user',NULL,0),(103,'huygiang10@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn K','HuyGiang 10','user.png','0978356010','user',NULL,0),(104,'huygiang11@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn L','HuyGiang 11','user.png','0978356011','user',NULL,0),(105,'huygiang12@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn M','HuyGiang 12','user.png','0978356012','user',NULL,0),(106,'huygiang13@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn N','HuyGiang 13','user.png','0978356013','user',NULL,0),(107,'huygiang14@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn O','HuyGiang 14','user.png','0978356014','user',NULL,0),(108,'huygiang15@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn P','HuyGiang 15','user.png','0978356015','user',NULL,0),(109,'huygiang16@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn Q','HuyGiang 16','user.png','0978356016','user',NULL,0),(110,'huygiang17@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn R','HuyGiang 17','user.png','0978356017','user',NULL,0),(111,'huygiang18@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn S','HuyGiang 18','user.png','0978356018','user',NULL,0),(112,'huygiang19@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn T','HuyGiang 19','user.png','0978356019','user',NULL,0),(113,'huygiang20@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn U','HuyGiang 20','user.png','0978356020','user',NULL,0),(114,'huygiang21@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn V','HuyGiang 21','user.png','0978356021','user',NULL,0),(115,'huygiang22@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn W','HuyGiang 22','user.png','0978356022','user',NULL,0),(116,'huygiang23@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn X','HuyGiang 23','user.png','0978356023','user',NULL,0),(117,'huygiang24@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Văn Y','HuyGiang 24','user.png','0978356024','user',NULL,0),(118,'huygiang25@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy 7','HuyGiang 25','user.png','0978356025','user',NULL,0),(119,'huygiang26@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy 8','HuyGiang 26','user.png','0978356026','user',NULL,0),(120,'huygiang27@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy 9','HuyGiang 27','user.png','0978356027','user',NULL,0),(121,'huygiang28@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy :','HuyGiang 28','user.png','0978356028','user',NULL,0),(122,'huygiang29@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy ;','HuyGiang 29','user.png','0978356029','user',NULL,0),(123,'huygiang30@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy <','HuyGiang 30','user.png','0978356030','user',NULL,0),(124,'huygiang31@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy =','HuyGiang 31','user.png','0978356031','user',NULL,0),(125,'huygiang32@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy >','HuyGiang 32','user.png','0978356032','user',NULL,0),(126,'huygiang33@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy ?','HuyGiang 33','user.png','0978356033','user',NULL,0),(127,'huygiang34@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy @','HuyGiang 34','user.png','0978356034','user',NULL,0),(128,'huygiang35@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy A','HuyGiang 35','user.png','0978356035','user',NULL,0),(129,'huygiang36@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy B','HuyGiang 36','user.png','0978356036','user',NULL,0),(130,'huygiang37@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy C','HuyGiang 37','user.png','0978356037','user',NULL,0),(131,'huygiang38@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy D','HuyGiang 38','user.png','0978356038','user',NULL,0),(132,'huygiang39@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy E','HuyGiang 39','user.png','0978356039','user',NULL,0),(133,'huygiang40@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy F','HuyGiang 40','user.png','0978356040','user',NULL,0),(134,'huygiang41@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy G','HuyGiang 41','user.png','0978356041','user',NULL,0),(135,'huygiang42@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy H','HuyGiang 42','user.png','0978356042','user',NULL,0),(136,'huygiang43@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy I','HuyGiang 43','user.png','0978356043','user',NULL,0),(137,'huygiang44@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy J','HuyGiang 44','user.png','0978356044','user',NULL,0),(138,'huygiang45@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy K','HuyGiang 45','user.png','0978356045','user',NULL,0),(139,'huygiang46@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy L','HuyGiang 46','user.png','0978356046','user',NULL,0),(140,'huygiang47@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy M','HuyGiang 47','user.png','0978356047','user',NULL,0),(141,'huygiang48@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy N','HuyGiang 48','user.png','0978356048','user',NULL,0),(142,'huygiang49@gmail.com','$10$6aOfUI0qiQDGkdxOyHBVCO/wTbn4UcF83TLbsk.r3SmjLsTh/5HFu',1512813861584,1512813861584,1,'Nguyễn Huy O','HuyGiang 49','user.png','0978356049','user',NULL,0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_info`
--

DROP TABLE IF EXISTS `account_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(45) DEFAULT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `displayname` varchar(45) DEFAULT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_account_info_account1_idx` (`account_id`),
  CONSTRAINT `fk_account_info_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_info`
--

LOCK TABLES `account_info` WRITE;
/*!40000 ALTER TABLE `account_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment` (
  `id` int(11) NOT NULL,
  `title` mediumtext,
  `content` longtext,
  `course_id` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `time_end` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asfdsaf_idx` (`course_id`),
  CONSTRAINT `asfdsaf` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment`
--

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_file`
--

DROP TABLE IF EXISTS `assignment_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_file` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ee_idx` (`assignment_id`),
  KEY `bbb_idx` (`file_id`,`assignment_id`),
  KEY `aff_idx` (`file_id`),
  KEY `asdfas_idx` (`assignment_id`),
  CONSTRAINT `aff` FOREIGN KEY (`file_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `asdfas` FOREIGN KEY (`assignment_id`) REFERENCES `assignment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_file`
--

LOCK TABLES `assignment_file` WRITE;
/*!40000 ALTER TABLE `assignment_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_submit`
--

DROP TABLE IF EXISTS `assignment_submit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_submit` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `group_course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ádgas_idx` (`assignment_id`),
  KEY `badf_idx` (`member_id`),
  KEY `df_idx` (`group_course_id`),
  CONSTRAINT `badf` FOREIGN KEY (`member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `df` FOREIGN KEY (`group_course_id`) REFERENCES `group_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ádgas` FOREIGN KEY (`assignment_id`) REFERENCES `assignment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_submit`
--

LOCK TABLES `assignment_submit` WRITE;
/*!40000 ALTER TABLE `assignment_submit` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment_submit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `comment_parent_id` int(11) NOT NULL,
  `course_member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_post1_idx` (`post_id`),
  KEY `fk_comment_comment1_idx` (`comment_parent_id`),
  KEY `fk_comment_course_member1_idx` (`course_member_id`),
  CONSTRAINT `fk_comment_comment1` FOREIGN KEY (`comment_parent_id`) REFERENCES `comment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_course_member1` FOREIGN KEY (`course_member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_post1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `account_id1` int(11) NOT NULL,
  `account_id2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_conversation_account2_idx` (`account_id1`),
  KEY `fk_conversation_account1_idx` (`account_id2`),
  CONSTRAINT `fk_conversation_account1` FOREIGN KEY (`account_id2`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_conversation_account2` FOREIGN KEY (`account_id1`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_reply`
--

DROP TABLE IF EXISTS `conversation_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `isRead` varchar(45) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_conversation_reply_account1_idx` (`creator_id`),
  KEY `fk_conversation_reply_conversation1_idx` (`conversation_id`),
  CONSTRAINT `fk_conversation_reply_account1` FOREIGN KEY (`creator_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_conversation_reply_conversation1` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_reply`
--

LOCK TABLES `conversation_reply` WRITE;
/*!40000 ALTER TABLE `conversation_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversation_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `info` mediumtext,
  `id_creater` int(11) NOT NULL,
  `time_start` bigint(20) DEFAULT NULL,
  `time_end` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `private` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_account1_idx` (`id_creater`),
  CONSTRAINT `fk_course_account1` FOREIGN KEY (`id_creater`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'Kinh tế công nghệ phần mềm','Miêu tả về khóa học!',33,1512151804782,1512661864782,1,NULL),(2,'Giải Tích 2 -2016A','Miêu tả về khóa học!',33,1512151804782,1512571864782,2,NULL),(3,'Khóa học A -Năm Học 2010','Miêu tả về khóa học!',99,1513307472361,1511604505065,-1,0),(4,'Khóa học B -Năm Học 2011','Miêu tả về khóa học!',97,1513159447345,1511456480049,1,1),(5,'Khóa học C -Năm Học 2012','Miêu tả về khóa học!',99,1513529060142,1511826092846,0,1),(6,'Khóa học D -Năm Học 2013','Miêu tả về khóa học!',94,1514304200816,1512601233520,-1,1),(7,'Khóa học E -Năm Học 2014','Miêu tả về khóa học!',98,1513711861624,1512008894328,-1,0),(8,'Khóa học F -Năm Học 2015','Miêu tả về khóa học!',94,1513190434424,1511487467128,1,0),(9,'Khóa học G -Năm Học 2016','Miêu tả về khóa học!',93,1513495236351,1511792269055,-1,1),(10,'Khóa học H -Năm Học 2017','Miêu tả về khóa học!',98,1513761644589,1512058677293,0,1),(11,'Khóa học I -Năm Học 2018','Miêu tả về khóa học!',102,1513884908435,1512181941139,-1,0),(12,'Khóa học J -Năm Học 2019','Miêu tả về khóa học!',97,1513152394922,1511449427626,1,0),(13,'Khóa học K -Năm Học 2020','Miêu tả về khóa học!',99,1513590710842,1511887743546,1,0),(14,'Khóa học L -Năm Học 2021','Miêu tả về khóa học!',101,1514486832282,1512783864986,1,0),(15,'Khóa học M -Năm Học 2022','Miêu tả về khóa học!',97,1513137060261,1511434092965,-1,0),(16,'Khóa học N -Năm Học 2023','Miêu tả về khóa học!',98,1514486307825,1512783340529,1,0),(17,'Khóa học O -Năm Học 2024','Miêu tả về khóa học!',100,1514494912328,1512791945032,0,1),(18,'Khóa học P -Năm Học 2025','Miêu tả về khóa học!',95,1514330853849,1512627886553,0,0),(19,'Khóa học Q -Năm Học 2026','Miêu tả về khóa học!',100,1513055947414,1511352980118,-1,1),(20,'Khóa học R -Năm Học 2027','Miêu tả về khóa học!',93,1514523266003,1512820298707,1,0),(21,'Khóa học S -Năm Học 2028','Miêu tả về khóa học!',100,1513761736637,1512058769341,0,0),(22,'Khóa học T -Năm Học 2029','Miêu tả về khóa học!',96,1513688540979,1511985573683,-1,1);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_folder`
--

DROP TABLE IF EXISTS `course_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `describe` varchar(45) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `course_folder_parent` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_folder_course1_idx` (`course_id`),
  KEY `fk_course_folder_course_folder1_idx` (`course_folder_parent`),
  CONSTRAINT `fk_course_folder_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_folder_course_folder1` FOREIGN KEY (`course_folder_parent`) REFERENCES `course_folder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_folder`
--

LOCK TABLES `course_folder` WRITE;
/*!40000 ALTER TABLE `course_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_log`
--

DROP TABLE IF EXISTS `course_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `describe` varchar(45) DEFAULT NULL,
  `course_member_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_log_course_member1_idx` (`course_member_id`),
  KEY `fk_course_log_course1_idx` (`course_id`),
  CONSTRAINT `fk_course_log_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_log_course_member1` FOREIGN KEY (`course_member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_log`
--

LOCK TABLES `course_log` WRITE;
/*!40000 ALTER TABLE `course_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_member`
--

DROP TABLE IF EXISTS `course_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) DEFAULT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_member_course1_idx` (`course_id`),
  KEY `fk_course_member_account1_idx` (`account_id`),
  CONSTRAINT `fk_course_member_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_member_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_member`
--

LOCK TABLES `course_member` WRITE;
/*!40000 ALTER TABLE `course_member` DISABLE KEYS */;
INSERT INTO `course_member` VALUES (1,'member','Huy GIang',1,1,33),(2,'member','Hihi',1,2,33),(3,'member','Huy Giang2',1,1,34),(4,'member','Nguyễn Văn A',1,1,138),(5,'member','Nguyễn Văn B',1,1,123),(6,'member','Nguyễn Văn C',1,1,124),(7,'member','Nguyễn Văn D',1,1,114),(8,'member','Nguyễn Văn E',1,1,121),(9,'member','Nguyễn Văn F',1,1,117),(10,'member','Nguyễn Văn G',1,1,104),(11,'member','Nguyễn Văn H',1,1,108),(12,'member','Nguyễn Văn I',1,1,135),(13,'member','Nguyễn Văn J',1,1,105),(14,'member','Nguyễn Văn K',1,1,141),(15,'member','Nguyễn Văn L',1,1,107),(16,'member','Nguyễn Văn M',1,1,125),(17,'member','Nguyễn Văn N',1,1,115),(18,'member','Nguyễn Văn O',1,1,112),(19,'member','Nguyễn Văn A',1,2,126),(20,'member','Nguyễn Văn B',1,2,103),(21,'member','Nguyễn Văn C',1,2,118),(22,'member','Nguyễn Văn D',1,2,125),(23,'member','Nguyễn Văn E',1,2,111),(24,'member','Nguyễn Văn F',1,2,116),(25,'member','Nguyễn Văn G',1,2,115),(26,'member','Nguyễn Văn H',1,2,124),(27,'member','Nguyễn Văn I',1,2,132),(28,'member','Nguyễn Văn J',1,2,108),(29,'member','Nguyễn Văn K',1,2,107),(30,'member','Nguyễn Văn L',1,2,127),(31,'member','Nguyễn Văn M',1,2,112),(32,'member','Nguyễn Văn A',1,3,124),(33,'member','Nguyễn Văn B',1,3,131),(34,'member','Nguyễn Văn C',1,3,110),(35,'member','Nguyễn Văn D',1,3,129),(36,'member','Nguyễn Văn E',1,3,122),(37,'member','Nguyễn Văn F',1,3,103),(38,'member','Nguyễn Văn G',1,3,112),(39,'member','Nguyễn Văn H',1,3,118),(40,'member','Nguyễn Văn I',1,3,106),(41,'member','Nguyễn Văn J',1,3,139),(42,'member','Nguyễn Văn K',1,3,104),(43,'member','Nguyễn Văn L',1,3,141),(44,'member','Nguyễn Văn M',1,3,105),(45,'member','Nguyễn Văn N',1,3,132),(46,'member','Nguyễn Văn O',1,3,137),(47,'member','Nguyễn Văn A',1,4,123),(48,'member','Nguyễn Văn B',1,4,119),(49,'member','Nguyễn Văn C',1,4,139),(50,'member','Nguyễn Văn D',1,4,130),(51,'member','Nguyễn Văn E',1,4,113),(52,'member','Nguyễn Văn F',1,4,134),(53,'member','Nguyễn Văn G',1,4,116),(54,'member','Nguyễn Văn H',1,4,103),(55,'member','Nguyễn Văn I',1,4,108),(56,'member','Nguyễn Văn J',1,4,129),(57,'member','Nguyễn Văn K',1,4,109),(58,'member','Nguyễn Văn L',1,4,120),(59,'member','Nguyễn Văn M',1,4,140),(60,'member','Nguyễn Văn A',1,5,138),(61,'member','Nguyễn Văn B',1,5,117),(62,'member','Nguyễn Văn C',1,5,131),(63,'member','Nguyễn Văn D',1,5,133),(64,'member','Nguyễn Văn E',1,5,129),(65,'member','Nguyễn Văn F',1,5,116),(66,'member','Nguyễn Văn G',1,5,113),(67,'member','Nguyễn Văn H',1,5,112),(68,'member','Nguyễn Văn I',1,5,123),(69,'member','Nguyễn Văn J',1,5,106),(70,'member','Nguyễn Văn K',1,5,141),(71,'member','Nguyễn Văn A',1,6,130),(72,'member','Nguyễn Văn B',1,6,116),(73,'member','Nguyễn Văn C',1,6,118),(74,'member','Nguyễn Văn D',1,6,127),(75,'member','Nguyễn Văn E',1,6,110),(76,'member','Nguyễn Văn F',1,6,126),(77,'member','Nguyễn Văn G',1,6,135),(78,'member','Nguyễn Văn H',1,6,105),(79,'member','Nguyễn Văn I',1,6,123),(80,'member','Nguyễn Văn J',1,6,111),(81,'member','Nguyễn Văn A',1,7,116),(82,'member','Nguyễn Văn B',1,7,117),(83,'member','Nguyễn Văn C',1,7,124),(84,'member','Nguyễn Văn D',1,7,128),(85,'member','Nguyễn Văn E',1,7,120),(86,'member','Nguyễn Văn F',1,7,104),(87,'member','Nguyễn Văn G',1,7,108),(88,'member','Nguyễn Văn H',1,7,114),(89,'member','Nguyễn Văn I',1,7,109),(90,'member','Nguyễn Văn J',1,7,107),(91,'member','Nguyễn Văn K',1,7,118),(92,'member','Nguyễn Văn L',1,7,125),(93,'member','Nguyễn Văn M',1,7,119),(94,'member','Nguyễn Văn N',1,7,131),(95,'member','Nguyễn Văn O',1,7,123),(96,'member','Nguyễn Văn P',1,7,103),(97,'member','Nguyễn Văn Q',1,7,140),(98,'member','Nguyễn Văn A',1,8,116),(99,'member','Nguyễn Văn B',1,8,139),(100,'member','Nguyễn Văn C',1,8,127),(101,'member','Nguyễn Văn D',1,8,110),(102,'member','Nguyễn Văn E',1,8,124),(103,'member','Nguyễn Văn F',1,8,128),(104,'member','Nguyễn Văn G',1,8,132),(105,'member','Nguyễn Văn H',1,8,125),(106,'member','Nguyễn Văn I',1,8,137),(107,'member','Nguyễn Văn J',1,8,112),(108,'member','Nguyễn Văn K',1,8,103),(109,'member','Nguyễn Văn L',1,8,130),(110,'member','Nguyễn Văn M',1,8,109),(111,'member','Nguyễn Văn N',1,8,140),(112,'member','Nguyễn Văn O',1,8,104),(113,'member','Nguyễn Văn A',1,9,129),(114,'member','Nguyễn Văn B',1,9,115),(115,'member','Nguyễn Văn C',1,9,106),(116,'member','Nguyễn Văn D',1,9,109),(117,'member','Nguyễn Văn E',1,9,117),(118,'member','Nguyễn Văn F',1,9,130),(119,'member','Nguyễn Văn G',1,9,113),(120,'member','Nguyễn Văn H',1,9,119),(121,'member','Nguyễn Văn I',1,9,107),(122,'member','Nguyễn Văn J',1,9,124),(123,'member','Nguyễn Văn K',1,9,114),(124,'member','Nguyễn Văn L',1,9,121),(125,'member','Nguyễn Văn M',1,9,105),(126,'member','Nguyễn Văn N',1,9,133),(127,'member','Nguyễn Văn O',1,9,103),(128,'member','Nguyễn Văn P',1,9,127),(129,'member','Nguyễn Văn Q',1,9,116),(130,'member','Nguyễn Văn R',1,9,136),(131,'member','Nguyễn Văn S',1,9,110),(132,'member','Nguyễn Văn A',1,10,135),(133,'member','Nguyễn Văn B',1,10,120),(134,'member','Nguyễn Văn C',1,10,139),(135,'member','Nguyễn Văn D',1,10,112),(136,'member','Nguyễn Văn E',1,10,126),(137,'member','Nguyễn Văn F',1,10,105),(138,'member','Nguyễn Văn G',1,10,106),(139,'member','Nguyễn Văn H',1,10,125),(140,'member','Nguyễn Văn I',1,10,133),(141,'member','Nguyễn Văn J',1,10,121),(142,'member','Nguyễn Văn K',1,10,124),(143,'member','Nguyễn Văn L',1,10,138),(144,'member','Nguyễn Văn M',1,10,115),(145,'member','Nguyễn Văn N',1,10,114),(146,'member','Nguyễn Văn A',1,11,137),(147,'member','Nguyễn Văn B',1,11,120),(148,'member','Nguyễn Văn C',1,11,125),(149,'member','Nguyễn Văn D',1,11,129),(150,'member','Nguyễn Văn E',1,11,135),(151,'member','Nguyễn Văn F',1,11,118),(152,'member','Nguyễn Văn G',1,11,134),(153,'member','Nguyễn Văn H',1,11,116),(154,'member','Nguyễn Văn I',1,11,138),(155,'member','Nguyễn Văn J',1,11,121),(156,'member','Nguyễn Văn K',1,11,122),(157,'member','Nguyễn Văn L',1,11,141),(158,'member','Nguyễn Văn A',1,12,139),(159,'member','Nguyễn Văn B',1,12,114),(160,'member','Nguyễn Văn C',1,12,105),(161,'member','Nguyễn Văn D',1,12,121),(162,'member','Nguyễn Văn E',1,12,108),(163,'member','Nguyễn Văn F',1,12,116),(164,'member','Nguyễn Văn G',1,12,112),(165,'member','Nguyễn Văn H',1,12,118),(166,'member','Nguyễn Văn I',1,12,109),(167,'member','Nguyễn Văn J',1,12,125),(168,'member','Nguyễn Văn K',1,12,122),(169,'member','Nguyễn Văn L',1,12,134),(170,'member','Nguyễn Văn M',1,12,128),(171,'member','Nguyễn Văn N',1,12,106),(172,'member','Nguyễn Văn O',1,12,120),(173,'member','Nguyễn Văn P',1,12,133),(174,'member','Nguyễn Văn A',1,13,126),(175,'member','Nguyễn Văn B',1,13,134),(176,'member','Nguyễn Văn C',1,13,123),(177,'member','Nguyễn Văn D',1,13,137),(178,'member','Nguyễn Văn E',1,13,141),(179,'member','Nguyễn Văn F',1,13,115),(180,'member','Nguyễn Văn G',1,13,138),(181,'member','Nguyễn Văn H',1,13,128),(182,'member','Nguyễn Văn I',1,13,127),(183,'member','Nguyễn Văn J',1,13,117),(184,'member','Nguyễn Văn A',1,14,123),(185,'member','Nguyễn Văn B',1,14,135),(186,'member','Nguyễn Văn C',1,14,126),(187,'member','Nguyễn Văn D',1,14,107),(188,'member','Nguyễn Văn E',1,14,139),(189,'member','Nguyễn Văn F',1,14,111),(190,'member','Nguyễn Văn G',1,14,131),(191,'member','Nguyễn Văn H',1,14,117),(192,'member','Nguyễn Văn I',1,14,116),(193,'member','Nguyễn Văn J',1,14,119),(194,'member','Nguyễn Văn K',1,14,128),(195,'member','Nguyễn Văn L',1,14,110),(196,'member','Nguyễn Văn M',1,14,103),(197,'member','Nguyễn Văn N',1,14,138),(198,'member','Nguyễn Văn O',1,14,130),(199,'member','Nguyễn Văn P',1,14,129),(200,'member','Nguyễn Văn Q',1,14,108),(201,'member','Nguyễn Văn A',1,15,105),(202,'member','Nguyễn Văn B',1,15,118),(203,'member','Nguyễn Văn C',1,15,121),(204,'member','Nguyễn Văn D',1,15,114),(205,'member','Nguyễn Văn E',1,15,131),(206,'member','Nguyễn Văn F',1,15,106),(207,'member','Nguyễn Văn G',1,15,116),(208,'member','Nguyễn Văn H',1,15,138),(209,'member','Nguyễn Văn I',1,15,117),(210,'member','Nguyễn Văn J',1,15,126),(211,'member','Nguyễn Văn K',1,15,128),(212,'member','Nguyễn Văn L',1,15,139),(213,'member','Nguyễn Văn M',1,15,122),(214,'member','Nguyễn Văn N',1,15,110),(215,'member','Nguyễn Văn O',1,15,137),(216,'member','Nguyễn Văn P',1,15,112),(217,'member','Nguyễn Văn Q',1,15,134),(218,'member','Nguyễn Văn A',1,16,133),(219,'member','Nguyễn Văn B',1,16,109),(220,'member','Nguyễn Văn C',1,16,117),(221,'member','Nguyễn Văn D',1,16,104),(222,'member','Nguyễn Văn E',1,16,111),(223,'member','Nguyễn Văn F',1,16,139),(224,'member','Nguyễn Văn G',1,16,140),(225,'member','Nguyễn Văn H',1,16,128),(226,'member','Nguyễn Văn I',1,16,136),(227,'member','Nguyễn Văn J',1,16,132),(228,'member','Nguyễn Văn K',1,16,138),(229,'member','Nguyễn Văn L',1,16,135),(230,'member','Nguyễn Văn M',1,16,134),(231,'member','Nguyễn Văn N',1,16,121),(232,'member','Nguyễn Văn O',1,16,114),(233,'member','Nguyễn Văn P',1,16,127),(234,'member','Nguyễn Văn A',1,17,114),(235,'member','Nguyễn Văn B',1,17,109),(236,'member','Nguyễn Văn C',1,17,135),(237,'member','Nguyễn Văn D',1,17,108),(238,'member','Nguyễn Văn E',1,17,121),(239,'member','Nguyễn Văn F',1,17,124),(240,'member','Nguyễn Văn G',1,17,140),(241,'member','Nguyễn Văn H',1,17,103),(242,'member','Nguyễn Văn I',1,17,132),(243,'member','Nguyễn Văn J',1,17,105),(244,'member','Nguyễn Văn K',1,17,139),(245,'member','Nguyễn Văn L',1,17,115),(246,'member','Nguyễn Văn A',1,18,140),(247,'member','Nguyễn Văn B',1,18,127),(248,'member','Nguyễn Văn C',1,18,130),(249,'member','Nguyễn Văn D',1,18,125),(250,'member','Nguyễn Văn E',1,18,103),(251,'member','Nguyễn Văn F',1,18,136),(252,'member','Nguyễn Văn G',1,18,139),(253,'member','Nguyễn Văn H',1,18,118),(254,'member','Nguyễn Văn I',1,18,117),(255,'member','Nguyễn Văn J',1,18,115),(256,'member','Nguyễn Văn K',1,18,121),(257,'member','Nguyễn Văn L',1,18,110),(258,'member','Nguyễn Văn M',1,18,108),(259,'member','Nguyễn Văn A',1,19,116),(260,'member','Nguyễn Văn B',1,19,108),(261,'member','Nguyễn Văn C',1,19,135),(262,'member','Nguyễn Văn D',1,19,125),(263,'member','Nguyễn Văn E',1,19,137),(264,'member','Nguyễn Văn F',1,19,106),(265,'member','Nguyễn Văn G',1,19,141),(266,'member','Nguyễn Văn H',1,19,126),(267,'member','Nguyễn Văn I',1,19,120),(268,'member','Nguyễn Văn J',1,19,131),(269,'member','Nguyễn Văn K',1,19,128),(270,'member','Nguyễn Văn L',1,19,119),(271,'member','Nguyễn Văn M',1,19,127),(272,'member','Nguyễn Văn N',1,19,112),(273,'member','Nguyễn Văn O',1,19,114),(274,'member','Nguyễn Văn P',1,19,109),(275,'member','Nguyễn Văn Q',1,19,140),(276,'member','Nguyễn Văn R',1,19,122),(277,'member','Nguyễn Văn A',1,20,112),(278,'member','Nguyễn Văn B',1,20,128),(279,'member','Nguyễn Văn C',1,20,106),(280,'member','Nguyễn Văn D',1,20,119),(281,'member','Nguyễn Văn E',1,20,141),(282,'member','Nguyễn Văn F',1,20,103),(283,'member','Nguyễn Văn G',1,20,111),(284,'member','Nguyễn Văn H',1,20,113),(285,'member','Nguyễn Văn I',1,20,104),(286,'member','Nguyễn Văn J',1,20,114),(287,'member','Nguyễn Văn K',1,20,130),(288,'member','Nguyễn Văn L',1,20,117),(289,'member','Nguyễn Văn M',1,20,116),(290,'member','Nguyễn Văn A',1,21,117),(291,'member','Nguyễn Văn B',1,21,120),(292,'member','Nguyễn Văn C',1,21,137),(293,'member','Nguyễn Văn D',1,21,119),(294,'member','Nguyễn Văn E',1,21,115),(295,'member','Nguyễn Văn F',1,21,126),(296,'member','Nguyễn Văn G',1,21,140),(297,'member','Nguyễn Văn H',1,21,114),(298,'member','Nguyễn Văn I',1,21,129),(299,'member','Nguyễn Văn J',1,21,136),(300,'member','Nguyễn Văn K',1,21,141),(301,'member','Nguyễn Văn A',1,22,123),(302,'member','Nguyễn Văn B',1,22,141),(303,'member','Nguyễn Văn C',1,22,109),(304,'member','Nguyễn Văn D',1,22,113),(305,'member','Nguyễn Văn E',1,22,124),(306,'member','Nguyễn Văn F',1,22,122),(307,'member','Nguyễn Văn G',1,22,127),(308,'member','Nguyễn Văn H',1,22,110),(309,'member','Nguyễn Văn I',1,22,138),(310,'member','Nguyễn Văn J',1,22,106),(311,'member','Nguyễn Văn K',1,22,105),(312,'member','Nguyễn Văn L',1,22,112),(313,'member','Nguyễn Văn M',1,22,136),(314,'member','Nguyễn Văn N',1,22,119),(315,'member','Nguyễn Văn O',1,22,134),(316,'member','Nguyễn Văn P',1,22,137),(317,'member','Nguyễn Văn Q',1,22,107),(318,'member','Nguyễn Văn R',1,22,108),(319,'member','Nguyễn Văn S',1,22,121);
/*!40000 ALTER TABLE `course_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,'\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_video_account1_idx` (`account_id`),
  KEY `fk_course_file_course1_idx` (`course_id`),
  CONSTRAINT `fk_course_file_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_video_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_role`
--

DROP TABLE IF EXISTS `document_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `course_member_id` int(11) NOT NULL,
  `role` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file_role_course_file1_idx` (`file_id`),
  KEY `fk_file_role_course_member1_idx` (`course_member_id`),
  CONSTRAINT `fk_file_role_course_file1` FOREIGN KEY (`file_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_file_role_course_member1` FOREIGN KEY (`course_member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_role`
--

LOCK TABLES `document_role` WRITE;
/*!40000 ALTER TABLE `document_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_chat`
--

DROP TABLE IF EXISTS `group_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `is_private` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_chat`
--

LOCK TABLES `group_chat` WRITE;
/*!40000 ALTER TABLE `group_chat` DISABLE KEYS */;
INSERT INTO `group_chat` VALUES (17,'',1512711763386,1512711763386,1),(18,'',1512877819099,1512877819099,1),(19,'',1512878579203,1512878579203,1),(20,'',1512879461396,1512879461409,1);
/*!40000 ALTER TABLE `group_chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_chat_member`
--

DROP TABLE IF EXISTS `group_chat_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_chat_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_chat_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `is_hide` tinyint(1) DEFAULT '0',
  `is_read` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `afff_idx` (`account_id`),
  KEY `gggg_idx` (`group_chat_id`),
  CONSTRAINT `afff` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `gggg` FOREIGN KEY (`group_chat_id`) REFERENCES `group_chat` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_chat_member`
--

LOCK TABLES `group_chat_member` WRITE;
/*!40000 ALTER TABLE `group_chat_member` DISABLE KEYS */;
INSERT INTO `group_chat_member` VALUES (21,17,33,1512711763386,0,1),(22,17,34,1512711763386,0,0),(23,18,33,1512877819099,0,1),(24,18,138,1512877819099,0,0),(25,19,33,1512878579203,0,1),(26,19,123,1512878579203,0,0),(27,20,33,1512879461396,0,1),(28,20,124,1512879461396,0,0);
/*!40000 ALTER TABLE `group_chat_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_course`
--

DROP TABLE IF EXISTS `group_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_course` (
  `id` int(11) NOT NULL,
  `name` varchar(56) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asdfas_idx` (`course_id`),
  CONSTRAINT `dd` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_course`
--

LOCK TABLES `group_course` WRITE;
/*!40000 ALTER TABLE `group_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_course_member`
--

DROP TABLE IF EXISTS `group_course_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_course_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `group_course_id` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `afas_idx` (`member_id`),
  KEY `afasfas_idx` (`group_course_id`),
  CONSTRAINT `afas` FOREIGN KEY (`member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `afasfas` FOREIGN KEY (`group_course_id`) REFERENCES `group_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_course_member`
--

LOCK TABLES `group_course_member` WRITE;
/*!40000 ALTER TABLE `group_course_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_course_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `last_login`
--

DROP TABLE IF EXISTS `last_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `last_login` (
  `id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `last_role` varchar(45) DEFAULT NULL,
  `last_course` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adsad_idx` (`account_id`),
  CONSTRAINT `adsad` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `last_login`
--

LOCK TABLES `last_login` WRITE;
/*!40000 ALTER TABLE `last_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `last_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_download`
--

DROP TABLE IF EXISTS `member_download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `course_member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file_log_file1_idx` (`file_id`),
  KEY `fk_file_log_course_member1_idx` (`course_member_id`),
  CONSTRAINT `fk_file_log_course_member1` FOREIGN KEY (`course_member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_file_log_file1` FOREIGN KEY (`file_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_download`
--

LOCK TABLES `member_download` WRITE;
/*!40000 ALTER TABLE `member_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_download` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asdfdfas_idx` (`account_id`),
  KEY `gre_idx` (`group_id`),
  CONSTRAINT `asdfdfas` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `gre` FOREIGN KEY (`group_id`) REFERENCES `group_chat` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (5,'Helloworld',34,17,1512711763398),(6,' Helloworld  Hello world  Hello world',33,17,1512711811937),(7,'Duong <br/>oc <br/>to',33,17,1512736649605),(8,'hahha',33,17,1512738234225),(9,'sdfa',33,17,1512738373122),(10,'hihie',34,17,1512750817207),(11,'hahaha',34,17,1512752513409),(12,'tesst',34,17,1512752572536),(13,'haha',34,17,1512752650412),(14,'hihe',34,17,1512754017288),(15,'lol',33,17,1512754040380),(16,'lol',33,17,1512754147568),(17,'lala',33,17,1512754180182),(18,'masfasfa',33,17,1512754276186),(19,'abcads',33,17,1512754400593),(20,'asdfas',33,17,1512754417109),(21,'lalsdfa',33,17,1512754438471),(22,'lolasd',33,17,1512754522520),(23,'asfasdfa',33,17,1512755808666),(24,'afasdfsd',33,17,1512755873286),(25,'hfasfhasdbfa',33,17,1512755878814),(26,'asfasfasdfa',33,17,1512756170092),(27,'aaaaaa',33,17,1512756221559),(28,'fasfasfasfsdfa',34,17,1512756828849),(29,'asdafasfasfas',33,17,1512805420960),(30,'gadgsd',33,17,1512809309693),(31,'dfagdfgadfg',33,17,1512811559779),(32,'ấdfa',33,17,1512811782286),(33,'hello boy',33,17,1512877808470),(34,'hello boy',33,18,1512877819109),(35,'hihi',33,17,1512878104136),(36,'ádfasdfasd',33,18,1512878245763),(37,'abc',33,17,1512878545607),(38,'abc nua',33,18,1512878570798),(39,'acccc',33,19,1512878579219),(40,'ádfasdf',33,20,1512879461409);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_recipient`
--

DROP TABLE IF EXISTS `message_recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_read` tinyint(1) DEFAULT NULL,
  `message_id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_recipient_message1_idx` (`message_id`),
  KEY `fasdfsa_idx` (`account_id`),
  CONSTRAINT `fasdfsa` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_recipient_message1` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_recipient`
--

LOCK TABLES `message_recipient` WRITE;
/*!40000 ALTER TABLE `message_recipient` DISABLE KEYS */;
INSERT INTO `message_recipient` VALUES (2,1,5,33,1512711763398,0),(3,0,5,34,1512711763398,0),(4,1,6,33,1512802592199,0),(5,0,6,34,1512711811937,0),(6,1,7,33,1512802592199,0),(7,0,7,34,1512736649605,0),(8,1,8,33,1512802592199,0),(9,0,8,34,1512738234225,0),(10,0,9,34,1512738373122,0),(11,1,10,33,1512802592199,0),(12,1,11,33,1512802592199,0),(13,1,12,33,1512802592199,0),(14,1,13,33,1512802592199,0),(15,1,14,33,1512802592199,0),(16,0,15,34,1512754040380,0),(17,0,16,34,1512754147568,0),(18,0,17,34,1512754180182,0),(19,0,18,34,1512754276186,0),(20,0,19,34,1512754400593,0),(21,0,20,34,1512754417109,0),(22,0,21,34,1512754438471,0),(23,0,22,34,1512754522520,0),(24,0,23,34,1512755808666,0),(25,0,24,34,1512755873286,0),(26,0,25,34,1512755878814,0),(27,0,26,34,1512756170092,0),(28,0,27,34,1512756221559,0),(29,1,28,33,1512802592199,0),(30,0,29,34,1512805420960,0),(31,0,30,34,1512809309693,0),(32,0,31,34,1512811559779,0),(33,0,32,34,1512811782286,0),(34,0,33,34,1512877808470,0),(35,0,34,138,1512877819109,0),(36,0,35,34,1512878104136,0),(37,0,36,138,1512878245763,0),(38,0,37,34,1512878545607,0),(39,0,38,138,1512878570798,0),(40,0,39,123,1512878579219,0),(41,0,40,124,1512879461409,0);
/*!40000 ALTER TABLE `message_recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_idx` (`creator_id`),
  CONSTRAINT `fkd` FOREIGN KEY (`creator_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset`
--

DROP TABLE IF EXISTS `password_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pw` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset`
--

LOCK TABLES `password_reset` WRITE;
/*!40000 ALTER TABLE `password_reset` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `member_creator_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_post_course_member1_idx` (`member_creator_id`),
  KEY `fk_post_course1_idx` (`course_id`),
  CONSTRAINT `fk_post_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_course_member1` FOREIGN KEY (`member_creator_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_question_subject1_idx` (`subject_id`),
  CONSTRAINT `fk_question_subject1` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `is_true` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ádfasdfasdf_idx` (`question_id`),
  CONSTRAINT `ádfasdfasdf` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resettoken`
--

DROP TABLE IF EXISTS `resettoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resettoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(265) DEFAULT NULL,
  `expiryDate` bigint(20) DEFAULT NULL,
  `verified` int(1) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resettoken`
--

LOCK TABLES `resettoken` WRITE;
/*!40000 ALTER TABLE `resettoken` DISABLE KEYS */;
INSERT INTO `resettoken` VALUES (2,'08fdb104-ccca-4e74-8576-0aa3c9190fe1',1512062071859,0,0),(3,'d3f6d4ae-6488-49c7-bec6-ae522b758ebc',1512063275759,0,0),(4,'38658b9c-11a5-44ca-a0f1-7521c7f6bd1b',1512100027305,0,0),(5,'666f27ab-587f-4e13-9a49-6834458fb57e',1512100718873,0,0),(6,'9ced1ee9-cf8a-483c-a594-fd85890769d2',1512100987052,0,0),(7,'93ac8690-540c-45e7-95c6-7f1922d37fe7',1512101112024,0,0),(8,'73b8eba6-98a2-4a24-bb52-101bddf85cf9',1512101517483,0,0),(9,'e208a8df-3f6b-4d32-b1d8-6e95423674ac',1512101802897,0,0),(10,'3efa0c46-3306-44f6-a32a-0dcc064ce583',1512102539572,0,0),(11,'f5722dcb-c069-468f-a2e7-ab6c06b6092e',1512102649679,0,0),(12,'13e54e3d-dd0a-4e7e-aadb-bf5e126a4e90',1512102689190,0,0),(13,'7ff3c970-821e-48c5-9df8-f23a5be2191c',1512102813742,0,0),(14,'9049afc6-5edc-4c3f-a7dd-f108b5e8c7fe',1512102954004,0,0),(15,'7638b0e1-9af4-49ce-9c39-45b65a54c4a4',1512103134512,0,0),(16,'7100e414-69f8-4fe9-8198-dc3dbd2ec73f',1512103259487,0,0),(17,'1b125630-fef4-4e90-99f2-5beb7cda180e',1512103321161,0,0),(18,'f3752b0b-65f6-42bc-8fb7-45a95a1fa880',1512114587260,0,0),(19,'96255f0c-a94c-459c-8d44-806923889d4e',1512114995492,0,0),(20,'3978147c-d11d-4d31-a1c2-81f3ec10b1a8',1512115288868,0,0),(21,'7d02aec6-2c0f-463e-813b-9dd7c4ecb302',1512115365508,0,0),(22,'2e52f701-5390-4e0f-b300-fc194e4782b6',1512115486483,0,0),(23,'ba66c3f1-7f0c-42b9-a526-a1c29b956081',1512115943193,0,0),(24,'f73c88d6-38a9-40b3-ab48-388df0ec9e62',1512115998522,0,0),(25,'3cdf3fe7-b373-4185-ab49-7faff711deb7',1512116463679,0,0),(26,'7c65126e-9a84-4352-9faf-1ec1616d09fd',1512116698974,0,0),(27,'97bb021c-17b4-4bf6-9a53-ade6beb35d19',1512116811432,0,0),(28,'fcedff1a-277a-4e9f-8580-6dc85436c047',1512116908705,0,0),(29,'826325ab-dbe7-467b-8e06-ba39e05a4448',1512116958788,0,0),(30,'64dc93b1-207e-4b0b-9458-d96f22a16a95',1512117039513,0,0),(31,'35b32299-e592-4a1f-b7d2-4516cb4a302e',1512633749839,0,0);
/*!40000 ALTER TABLE `resettoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_idx` (`account_id`),
  CONSTRAINT `a` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_subject_account1_idx` (`account_id`),
  CONSTRAINT `fk_subject_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_notification`
--

DROP TABLE IF EXISTS `user_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_notification` (
  `account_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `isCheck` tinyint(4) NOT NULL,
  PRIMARY KEY (`account_id`,`notification_id`),
  KEY `bb_idx` (`notification_id`),
  CONSTRAINT `aa` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bb` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_notification`
--

LOCK TABLES `user_notification` WRITE;
/*!40000 ALTER TABLE `user_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_video`
--

DROP TABLE IF EXISTS `view_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `length` varchar(45) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `course_member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_view_video_document1_idx` (`document_id`),
  KEY `fk_view_video_course_member1_idx` (`course_member_id`),
  CONSTRAINT `fk_view_video_course_member1` FOREIGN KEY (`course_member_id`) REFERENCES `course_member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_view_video_document1` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_video`
--

LOCK TABLES `view_video` WRITE;
/*!40000 ALTER TABLE `view_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `view_video` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-10 11:25:59

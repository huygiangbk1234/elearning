$(document).ready(function(){
	var $formUploadFile = $("#upload_form");
	$formUploadFile.validate({
		errorElement : 'span',
		errorClass : 'error-message',
		focusInvalid : false,
		ignore : "",
		rules : {
			file : {
//				required : true,
//				extension: "csv"
			}
		},
		errorPlacement : function(error, element) {
			$(element).before(error);
		},
		highlight : function(element) {
			$(element).addClass("has-error");
		},
		unhighlight : function(element) {
			$(element).removeClass("has-error");
		}

	});
})
window.addEventListener("load",function(){
	var video=new Vue({
		el:'#video',
		data:{
			idCourse:null,
			data:null,
			video:null,
			view:[],
			width:0,
			height:0,
			scaleX:1,
			scaleY:1,
			currentQ:null,
			template:null,
			isShowResult:false,
			result:"",
			time:0,
			time2:0,
			action:null,
			
			idVideo:0,
			currentI:0,
			maxSeek:0,
			lastSeek:0,
		},
		watch:{
			maxSeek:function(){
				if(this.maxSeek>this.lastSeek+1){
					this.lastSeek=this.maxSeek;
					var length=this.maxSeek+"-"+this.video.duration;
					var url="/course/"+this.idCourse+"/video/"+this.data.v.id+"/update/length?length="+length;
					$.ajax({
						url:url,
						method:'post'
					});
				}
			},
			currentQ:function(q){
				var n=q.answers.length;
				this.currentQ.arr=[];
				this.currentQ.arrA=[];
				for(var i=0;i<n;i++){
					if(q.answers[i]['true'])this.currentQ.arr.push(i);
				}
			},
			video:function(){
			},
			data:function(){
				this.video=document.getElementById("videoId");
				this.onBind();
				var x=this.data.v.answers;
				this.data.v.answers=x.split("_");
				this.maxSeek=Number(this.data.v.length.split("-")[0]);
				this.lastSeek=this.maxSeek;
			},
			time:function(t){
				if(t<=0){
					clearInterval(this.action);
						this.time2=3;
						
						this.showResult();
						this.action=setInterval(function(){
							this.time2-=0.1;
						}.bind(this),100);
				}
			},
			time2:function(t){
				if(t<=0){
					clearInterval(this.action);
					this.continueVideo();
				}
			}
		},
		methods:{
			onBind:function(){
				this.video.onloadedmetadata=function(){
					this.width=$(this.video).width();
					this.height=$(this.video).height();
					$("#question").css({width:this.width,height:this.height});
					this.view=[0,this.video.duration]
				}.bind(this)
				this.video.onseeked =function(){
					 this.onTua();
				}.bind(this)
				this.video.onseeking =function(){
					 this.onSeeking();
				}.bind(this)
				$("#videoId").on("timeupdate", function(event){this.onSeek()}.bind(this));
		        $("#videoId").bind('progress',function() {
		        	
		        }.bind(this));
		        $('#videoId').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
		            var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
		            if(state){
		            	
		            }   
		        });
			},
			check:function(i){
				if(!$('c'+i).is(":checked")){
					this.currentQ.arrA.push(i);
				}else{
					var index=this.currentQ.arrA.indexOf(i);
					if(index!=-1)
					this.currentQ.arrA.splice(index,1);
				}
			},
			checkR:function(){
				if(this.currentQ.arrA.length!=this.currentQ.arr.length) return false;
				for(var i=0;i<this.currentQ.arrA.length;i++){
					if(this.currentQ.arr.indexOf(this.currentQ.arrA[i])==-1){return false;}
				}
			},
			showResult:function(){
				var c=this.checkR();
				if(c){
					this.result="Right Answer!";
					this.data.v.answers[this.currentI]='1';
					
					var answers=this.data.v.answers.join("_");
					
					var url="/course/"+this.idCourse+"/video/"+this.data.v.id+"/update/answers/"+answers;
					$.ajax({
						url:url,
						method:'post'
					});
				}else {
					this.result="Right Answer!";
					this.data.v.answers[this.currentI]='_1';
					var answers=this.data.v.answers.join("_");
					var url="/course/"+this.idCourse+"/video/"+this.data.v.id+"/update/answers/"+answers;
					$.ajax({
						url:url,
						method:'post'
					});
				}
				this.isShowResult=true;
				this.time2=3;
				this.action=setInterval(function(){
					this.time2-=0.1;
				}.bind(this),100);
			},
			continueVideo:function(){
				$("#question").animate({"opacity":0,"z-index":-1},200);
				this.isShowResult=false;
				this.video.play();
				this.video.controls=true;
			},
			onTua:function(){
				var c=this.video.currentTime;
				if(c>this.maxSeek)this.video.currentTime=this.maxSeek;
			},
			onSeeking:function(){
				this.video.pause();
			},
			onSeek:function(){
				if(this.video.paused) return;
				var c=this.video.currentTime;
				
				if(c>this.maxSeek)this.maxSeek=c;
				for(var i=0;i<this.data.times.length;i++){
					var t=this.data.times[i];
					if(this.data.v.answers[i]!='0') return;
					if(c>=(t-0.2)&&c<=(t+0.2)){
						this.video.pause();
						this.video.controls=false;
						this.currentQ=(this.data.questions[i]);
						this.isShowResult=false;
						this.currentI=i;
						$("#question").animate({"opacity":100,"z-index":1},500);
// this.time=3;
// this.action=setInterval(function(){
// this.time-=0.1;
// }.bind(this),100);
					}
				}
			},
			getTime:function(t){
				var h=Math.floor(t/3600);
				var m=Math.floor((t-3600*h)/60);
				var s=Math.floor((t-3600*h-m*60));
				if(h<10)h="0"+h;
				if(m<10)m="0"+m;
				if(s<10)s="0"+h;
				return h+":"+m+":"+s;
			},
			save:function(){
				var form = document.getElementById("formFile");
				var formData = new FormData(form);
				var url="/course/"+this.idCourse+"/video/submit?idLesson="+this.lessons[this.currentLesson].id;
				var u="/course/"+this.idCourse+"/video/submit/question?idVideo=";
				var v=this.list;
				for(var i=0;i<this.list.questions.length;i++){
					var q=this.list.questions[i];
					var b=false;
					for(var j=0;j<q.answers.length;j++){
						if(q.answers[j]['true']) b=true;
					}
					if(!b){
						alert("Question not have right answer!");
						return;
					}
				}
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						$.ajax({
							url:u+data,
							method:'post',
							data:JSON.stringify(v),
							contentType:"application/json;charset=UTF-8",
							success:function(data1){
								location.reload();
							}
						});
					},
					error:function(){
						alert("error send data");
					}
				});
			}
		}
	});
	video.idCourse=test.idCourse;
	video.data=test.questions;
	video.idVideo=test.video.id;
});
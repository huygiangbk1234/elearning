window.addEventListener("load",function(){
				var header=new Vue({
					el:"#language",
					data:{
						vi:"",
						en:"",
					},
					method:{
						changeLanguage:function(l){
							location.href=l
						}
					},
					created:function(){
						var href=location.href;
						if(href.split("?").length>1){
							var i=href.indexOf("lang");
							if(i!=-1){
								href=href.substring(0,href.length-8);
							}
							if(href.split("?").length>1){
								this.vi=href+"&lang=vi";
								this.en=href+"&lang=en";
							}else{
								this.vi=href+"?lang=vi";
								this.en=href+"?lang=en";
							}
							
						}else{
							this.vi=href+"?lang=vi";
							this.en=href+"?lang=en";
						}
					}
				});
			});
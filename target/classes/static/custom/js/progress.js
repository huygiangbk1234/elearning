window.addEventListener("load", function() {
	var listCourses = new Vue({
		el : "#body-progress",
		data : {
			idCourse : 0,
			pages:[],
			
			listResultFilter:[],
			
			currentPage : 0,
			arrMembers : [],
			currentArr:[],
			
			data:[],
			
			title:{},
			currentMember:null,
			isFilter:false,
		},
		components:{
			mytable:{
				template: '#table-template',
				data:function(){
					return {a:1,id:js.user.id,currentMember:null,}
				},
				methods:{
					openAssignment:function(id){
						var url="/course/"+this.$parent.idCourse+"/assignment/"+id;
						location.href=url;
					},
					formatDate:function(time){
						var today=new Date(time);
						var dd = today.getDate();
						var mm = today.getMonth()+1; //January is 0!

						var yyyy = today.getFullYear();
						if(dd<10){
						    dd='0'+dd;
						} 
						if(mm<10){
						    mm='0'+mm;
						} 
						var h=today.getHours();
						var m=today.getMinutes();
						
						var today =h+"h"+m+" "+ dd+'/'+mm+'/'+yyyy;
						return today;
					},
					openDetail:function(member){
						this.currentMember=member;
						if(!this.currentMember.detail){
							this.loadDetailMember();
						}else{
							this.$parent.isFilter=true;
						}
					},
					loadDetailMember:function(){
						var self = this;
						$.ajax({
							url : "/course/"+self.$parent.idCourse+"/progress/detail/"+this.currentMember.member.id,
							method:'post',
							success : function(data) {
								self.currentMember.detail=data;
								self.$parent.isFilter=true;
							}
						});
					}
				}
			}
		},
		methods : {
			onBack:function(){
				this.isFilter=false;
			},
			onLoadMore : function() {
				var self = this;
				$.ajax({
					url : "/course/"+self.idCourse+"/progress/",
					method:'post',
					success : function(data) {
						if(data!=null){
							self.data=data;
						}
					}
				});
			}
		}
	});
	listCourses.idCourse=idCourse;
	listCourses.title=title;
	listCourses.onLoadMore();
});
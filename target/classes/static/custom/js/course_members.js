window.addEventListener("load", function() {
	var listCourses = new Vue({
		el : "#body_members",
		data : {
			idCourse : 0,
			pages:[],
			isFilter:false,
			listResultFilter:[],
			title:{avatar:"",name:"",msg:""},
			currentPage : 0,
			arrMembers : [],
			currentArr:[],
		},
		components:{
			mytable:{
				template: '#table-template',
				data:function(){
					return {a:1,id:js.user.id}
				},
				methods:{
					openChat:function(member){
						if(member.member.id==js.user.id) return;
						modalChat.to=member;
					},
					deleteMember:function(member){
						if(member.member.id==js.user.id) return;
						if(confirm("Are you sure?")){
							var url="/course/"+this.$parent.idCourse+"/members/delete?id="+member.id;
							$.ajax({
								url : url,
								method:'post',
								success : function(result) {
									location.reload();
								}
							});
						}
					}
				}
			}
		},
		methods : {
			onChange:function(){
				console.log("filtering");
				var text=$("#input-search").val();
				if(text.replace(/(?:\r\n|\r|\n)/g, '').trim()==''){
					this.isFilter=false;
					return;
				}
				this.isFilter=true;
				var list=[];
				var self=this;
				$.ajax({
					url:"/course/"+idCourse+"/members/filter?name="+text,
					method:'post',
					success:function(data){
						self.listResultFilter=data;
					},
					error:function(e){console.log(e)}
				});
				listCourses.$forceUpdate();
			},
			onLoadMore : function(page) {
				if(this.arrMembers[page]!=null){
					this.currentPage=page;
					this.currentArr=this.arrMembers[page];
					return;
				}
				var self = this;
				$.ajax({
					url : "/course/"+self.idCourse+"/members/page/"+page,
					success : function(result) {
						if(result!=null){
							self.arrMembers[page]=result;
							self.currentPage=page;
							self.currentArr=result;
						}
					}
				});
			}
		}
	});
	listCourses.idCourse=idCourse;
	listCourses.pages=pages;
	listCourses.onLoadMore(0);
	listCourses.title=title;
	var modalAdd=new Vue({
		el:"#modalAddMember",
		data:{
			idCourse:-1,
			arr:[],
			tempEmail:{email:""},
			arrResult:[],
			current:0,
		},
		watch:{
			current:function(n){this.setPl()},
			arrResult:function(){this.current=0;this.setPl()},
		},
		methods:{
			setPl:function(){
				if(this.arrResult.length==0)return;
				this.tempEmail=this.arrResult[this.current];
			},
			clearAll:function(){
				this.arr=[];
			},
			hide:function(){
				this.arr=[];
				this.arrResult=[];
				this.tempEmail={email:""};
				$("#ip-add").val("");
				$("#modalAddMember").modal('hide');
			},
			remove:function(index){
				this.arr.splice(index,1);
			},
			save:function(){
				$("#modalAddMember").modal('hide');
				if(this.arr.length==0)return;
				var list=[];
				for(var i=0;i<this.arr.length;i++){
					var a=this.arr[i];
					var m={
							id:0,
							role:"member",
							alias:a.nickname,
							status:1,
							idCourse:this.idCourse,
							member:{id:a.id}
							};
					list.push(m);
				}
				var url="/course/"+this.idCourse+"/addListMember";
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(list),
					contentType: 'application/json; charset=UTF-8',
					success:function(data){
						location.reload();
					}
				});
			},
			onKeyUp:function(e){
				if(e.keyCode==40){
					if(this.current+1<this.arrResult.length) this.current++;
					return;
				}else if(e.keyCode==38){
					if(this.current-1>=0) this.current--;
					return;
				}
				console.log("filtering");
				this.arrResult=[];
				var text=$("#ip-add").val();
				if(text.replace(/(?:\r\n|\r|\n)/g, '').trim()==''){
					this.isFilter=false;
					return;
				}
				var self=this;
				$.ajax({
					url:"/user/filter?email="+text,
					method:'post',
					success:function(data){
						self.arrResult=data;
					},
					error:function(e){console.log(e)}
				});
			},
			addEmail:function(){
				if(this.tempEmail.email!="") this.arr.push(jQuery.extend(true, {}, this.tempEmail));
					this.arrResult=[];
					this.tempEmail={email:""};
					$("#ip-add").val("");
					console.log(this.arr)
			}
		}
	});
	modalAdd.idCourse=idCourse;
	var modalChat = new Vue({
		el:"#modalChat",
		data:{
			to:null,
			content:"",
		},
		watch:{
			to:function(n){
				if(n==null) $("#modalChat").modal('hide');
				else $("#modalChat").modal('show');
			}
		},
		methods:{
			sendMessage:function(){
				if(this.to==null) return;
				js.msg.sendMsg(this.to.member.id,this.content);
				this.to=null;
			},
			closeModal:function(){
				this.to=null;
				this.content="";
			}
		}
	});
});
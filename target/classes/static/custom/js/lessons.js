var remove=function(e){
		$(e.target).parent().remove();
	}
window.addEventListener("load",function(){
	var File=Vue.extend({
		props:['index'],
		computed:{
			id:function(){return "files"+this.index;}
		},
		template:"<input type='file' name='' v-bind:id='id' v-on:change='$parent.onChange(index)'/>"
	});
	var lessons=new Vue({
		el:'#lessons',
		data:{
			idCourse:-1,
			lessons:[],
		},
		methods:{
			getIcon:function(name){
				var ext=name.split(".");
				ext=ext[ext.length-1].toLowerCase();
				if(ext=='txt') return "<i class='fa fa-file-text-o' aria-hidden='true'></i>";
				else if(ext=='pdf') return "<i class='fa fa-file-pdf-o red' aria-hidden='true'></i>";
				else if(ext=='docx')  return "<i class='fa fa-file-word-o blue' aria-hidden='true'></i>";
				else if(ext=='mp4')  return "<i class='fa fa-file-video-o red' aria-hidden='true' ></i>";
				else if(ext=='png'||ext=='jpg'||ext=='gif')  return "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
				return  "<i class='fa fa-file' aria-hidden='true'></i>";
			},
			fix:function(){
				this.lessons.forEach(function(l){
					if(!l.isOpen) l.isOpen=false;
					if(!l.isEdit) l.isEdit=false;
				});
			},
			createLesson:function(){
				var text=$("#ip-name").val();
				$("#modalCreateLesson").modal('hide');
				if(checkEmpty(text)){
					alert("Name is empty!");
					return;
				}
				var lesson={name:text,idCourse:this.idCourse,pos:this.lessons.length};
				var url="/course/"+this.idCourse+"/lesson/create";
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(lesson),
					contentType:"application/json;charset=UTF-8",
					success:function(data){
						data.isEdit=false;
						data.isOpen=false;
						self.lessons.push(data);
						js.menu_lesson.loadAllLesson();
					}
				});
			},
			loadInfo:function(index){
				var l=this.lessons[index];
				var url="/course/"+this.idCourse+"/lesson/create";
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(lesson),
					contentType:"application/json;charset=UTF-8",
					success:function(data){
						console.log(data);
						data.isEdit=false;
						data.isOpen=false;
						self.lessons.push(data);
					}
				});
			},
			toggleOpen:function(index){
				var l=this.lessons[index];
				l.isOpen=!l.isOpen;
				lessons.$forceUpdate();
			},
			toggleEdit:function(index){
				var l=this.lessons[index];
				if(l.isEdit){
					var name=$("#name"+index).val();
					if(!checkEmpty(name)){
						this.updateName(l.id,name,l);
					}
				}
				l.isEdit=!l.isEdit;
				lessons.$forceUpdate();
			},
			remove:function(e,index,i){
				this.lessons[index].files[i].isDelete=true;
				remove(e);
			},
			createFileInput:function(name){
				return "<div class='f'>"+
				"<i class='fa fa-file-text-o' aria-hidden='true'></i>"+
				"<span>"+name+"</span>"+
				"<i class='fa fa-times r' aria-hidden='true' onclick='remove(event)'></i>"+
				"</div>";
			},
			onChange:function(index){
				var id="files"+index;
				var f=$("#files"+index);
				var name=f.val();
				var b=false;
				console.log("s");
				$('#listF'+index+' [type=file]').each(function(index,input){
					if($(input).val()==name) b=true;
				});
				if(b) return;
				name=name.split("\\");
				name=name[name.length-1];
				f.after("<div id='temp'></div>");
				new File({parent:this,propsData:{index:index}}).$mount("#temp");
				var list=$("#listF"+index);
				f.attr({"id":'','name':"files"});
				
				var d=this.createFileInput(name);
				var a=$(d).appendTo(list);
				$(a).append(f);
			},
			saveEdit:function(index){
				var l=this.lessons[index];
				var fileDelete=[];
				for(var i=0;i<l.files.length;i++){
					if(l.files[i].isDelete) {
						fileDelete.push(l.files[i].id);
					}
				}
				var form = document.getElementById("listF"+index);
				var formData = new FormData(form);
				formData.append('fileDelete',fileDelete);
				var url="/course/"+this.idCourse+"/lesson/"+l.id+"/update";
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						location.reload();
					},
					error:function(){
						alert("error send data");
					}
				});
			},
			updatePos:function(id,pos){
				var url="/course/"+this.idCourse+"/lesson/"+id+"/updatepos?pos="+pos;
				$.ajax({
					url:url,
					success:function(){
						js.menu_lesson.loadAllLesson();
					}
				});
			},
			updateName:function(id,name,l){
				var url="/course/"+this.idCourse+"/lesson/"+id+"/updatename?name="+name;
				$.ajax({
					url:url,
					success:function(data){
						l.name=data;
					}
				});
			},
			upLesson:function(index){
				if(index>0){
					this.updatePos(this.lessons[index].id,index-1);
					this.updatePos(this.lessons[index-1].id,index);
					this.lessons.move(index,index-1);
				}
			},
			downLesson:function(index){
				if(index<this.lessons.length-1){
					this.updatePos(this.lessons[index].id,index+1);
					this.updatePos(this.lessons[index+1].id,index);
					this.lessons.move(index,index+1);
				}
			},
			
		}
	});
	lessons.idCourse=test.idCourse;
	lessons.lessons=test.lessons;
	lessons.fix();
});
window.addEventListener("load",function(){
	var qb=new Vue({
		el:"#question_bank",
		data:{
			listSubjects:[],
			currentSubject:null,
			current:-1,
			listResultFilter:[],
			resultSearch:null,
			idCourse:-1,
			
			total:0,
			
			
			isEdit:false,
		},
		watch:{
			
		},
		methods:{
			onImport:function(data){
				if(data.length==0){
					alert("Data empty!");
					return;
				}
				if(confirm("Import "+data.length+" question?")){
					var list=[];
					var id=this.currentSubject.id;
					for(var i=0;i<data.length;i++){
						var q=data[i];
						var keys = Object.keys( q );
						console.log(keys);
						var question=q[keys[0]];
						var type=q[keys[1]];
						var answers=[];
						var r=q[keys[2]].split(',');
						
						for(var j=3;j<keys.length;j++){
							var b=r.indexOf((j-2)+"")!=-1?true:false;
							if(checkEmpty(q[keys[j]])) continue;
							var a={
									answer:q[keys[j]],
									True:b,
							};
							answers.push(a);
						}
						if(answers.length==0||checkEmpty(question)) continue;
						var q={
								question:question,
								answers:answers,
								idSubject:id,
								type:type,
						}
						list.push(q);
					}
					if(list.length!=0){
						var url="/subject/import?id="+this.currentSubject.id;
						$.ajax({
							url:url,
							method:'post',
							data:JSON.stringify(list),
							contentType:"application/json;charset=UTF-8",
							success:function(){
								location.reload();
								alert("Import Success!");
							}
						});
					}
				}
			},
			onChange:function(file){
				var self=this;
				 var input = file.target;
				var reader = new FileReader();

			    reader.onload = function(e) {
			    	 var data = e.target.result;
			         var workbook = XLSX.read(data, {
			           type: 'binary'
			         });

			         workbook.SheetNames.forEach(function(sheetName) {
			           // Here is your object
			           var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
			           var json_object = JSON.stringify(XL_row_object);
			           console.log(json_object);
			           self.onImport(XL_row_object);
			         })

			    };

			    reader.onerror = function(ex) {
			      console.log(ex);
			    };

			    reader.readAsBinaryString(input.files[0]);
			},
			openEdit:function(){
				this.currentSubject.temp=this.currentSubject.name;
				$("#modalUpdateSubject").modal('show');
				qb.$forceUpdate();
			},
			createSubject:function(){
				var text=$("#ip-name").val();
				if(checkEmpty(text)){return}
				text=text.trim();
				var url="/subject/create?name="+text;
				$.ajax({
					url:url,
					success:function(data){location.reload()}
				})
			},
			updateSubject:function(){
				var text=this.currentSubject.temp;
				if(checkEmpty(text)){return}
				text=text.trim();
				var url="/subject/update?name="+text+"&id="+this.currentSubject.id;
				$.ajax({
					url:url,
					success:function(data){location.reload()}
				})
			},
			deleteQuestion:function(id){
				if(confirm("Are you sure?")){
					var url="/question/delete?id="+this.resultSearch.id+"&idSubject="+this.currentSubject.id;
					var sefl=this;
					$.ajax({
						url:url,
						success:function(data){sefl.currentSubject.total--;sefl.selectSubject(sefl.current)}
					})
				}
			},
			selectSubject:function(index){
				this.current=index;
				this.resultSearch=null;
				this.isEdit=false;
				$("#input_search").val("");
				this.currentSubject=this.listSubjects[index];
			},
			toggleEditQuestion:function(){
				this.isEdit=!this.isEdit;
			},
			addAnswer:function(){
				this.resultSearch.answers.push({answer:"",'true':false});
			},
			removeAnswer:function(index){
				this.resultSearch.answers.splice(index,1);
			},
			saveEditQuestion:function(){
				console.log(this.resultSearch);
				var self=this;
				this.resultSearch.answers.forEach(function(a,index){
					a.answer=a.answer.trim();
					if(a.answer.replace(/(?:\r\n|\r|\n)/g, '')=='')
						self.resultSearch.answers.splice(index,1);
				});
				var url="/course/"+this.idCourse+"/question_bank/updatequestion";
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(self.resultSearch),
					contentType:"application/json;charser=UTF-8",
					success:function(data){
						self.onSelectQuestion(self.resultSearch);
					},
					error:function(e){console.log(e)}
				});
			},
			toggleImport:function(index){
				var b=this.library[index].ofCourse;
				console.log(b);
				var url="/course/"+this.idCourse+"/question_bank/updatelibrary?idSubject="+this.library[index].id+"&delete="+b;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
					},
					error:function(e){console.log(e)}
				});
			},
			loadLibrary:function(){
				var url="/course/"+this.idCourse+"/question_bank/library";
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						console.log(data);
						self.library=data;
					},
					error:function(e){console.log(e)}
				});
			},
			getA:function(k){
				return String.fromCharCode(k+65);
			},
			showResult:function(data){
				this.resultSearch=data;
			},
			onSelectQuestion:function(q){
				this.listResultFilter=[];
				$("#input_search").val(q.question);
				var url="/course/"+this.idCourse+"/question/get?id="+q.id;
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						self.resultSearch=data;
					},
					error:function(e){console.log(e)}
				});
				
			},
			onFilter:function(){
				console.log("filtering");
				this.resultSearch=null;
				var text=$("#input_search").val().replace(/(?:\r\n|\r|\n)/g, ' ');
				if(text.trim()==''){
					this.listResultFilter=[];
					return;
				}
				var self=this;
				var data=text.split(' ');
				var url="/course/"+this.idCourse+"/questions/filter?idSubject="+this.currentSubject.id;
				this.listResultFilter=[];
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(data),
					contentType: 'application/json; charset=UTF-8',
					success:function(data){
						self.listResultFilter=data;
					},
					error:function(e){console.log(e)}
				});
				
				
				
			},
			submit:function(e){
				var self=this;
				if(self.nums==0) {
					alert("total questions is 0!");
					 e.preventDefault();
					return false;
				}
				$('#formTest').submit(function() {
				    $("#numsEasy").val(self.numsEasy);
				    $("#numsNormal").val(self.numsNormal);
				    $("#numsHard").val(self.numsHard);
				    return true;
				});
			},
			
		},
	});
	qb.listSubjects=test.subjects;
});
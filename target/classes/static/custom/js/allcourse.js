window.addEventListener("load",function(){
	var listCourses=new Vue({
		el:"#listCourses",
		data:{
			recently:null,
			current:[false,false,false,false],
			courses:[],
			courses_temp:[],
			courses_arr:[[],[],[],[]],
			arr_result:[],
			currentIndex:0,
			
			course:{name:"",info:"",timeStart:new Date().getTime(),timeEnd:new Date().getTime()},
			dateStart:0,
			timeStart:0,
			timeEnd:0,
			dateEnd:0,
		},
		mounted:function(){
			this.openDate();
			this.openTime();
		},
		watch:{
			dateEnd:function(d){this.course.timeEnd=d+this.timeEnd*60*1000;},
			timeEnd:function(d){this.course.timeEnd=this.dateEnd+d*60*1000;},
			dateStart:function(d){this.course.timeStart=d+this.timeStart*60*1000},
			timeStart:function(d){this.course.timeStart=this.dateStart+d*60*1000;},
			courses:function(){
				var arr=[];
				var self=this;
				this.courses.forEach(function(c){
					var d=c.timeEnd-c.timeStart;
					c.percent=d>0?Math.round((new Date().getTime()-c.timeStart)/d*100):0;
					c.percent=c.percent>100?100:c.percent;
					c.timeStart=self.formatDate(new Date(c.timeStart));
					c.timeEnd=self.formatDate(new Date(c.timeEnd));
					c.time=c.timeStart+" - "+c.timeEnd;
					if(c.status===-1) self.courses_arr[1].push(c);
					else if(c.status===1) self.courses_arr[2].push(c);
					else if(c.status===2) self.courses_arr[3].push(c);
					if(c.id===self.recently) self.courses_arr[0].push(c);
				});
				this.setCurrent(0);
			}
		},
		methods:{
			openDate:function(b){
				var self=this;
				$("#ip-dateS").pickadate({
					format : 'dd/mm/yyyy',
					today : '',
					clear : '',
					close : '',
					onStart: function() {
					    var date = new Date();
					    this.set('select', date);
					    $("#ip-dateS_root").css({'pointer-events':'none'});
					},
					onOpen:function(){
						$("#ip-dateS_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-dateS_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(listCourses)listCourses.dateStart=this.get('select').pick;
					},
				});
				$("#ip-dateE").pickadate({
					format : 'dd/mm/yyyy',
					today : '',
					clear : '',
					close : '',
					onStart: function() {
					    var date = new Date();
					    this.set('select', date);
					    $("#ip-dateE_root").css({'pointer-events':'none'});
					},
					onOpen:function(){
						$("#ip-dateE_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-dateE_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(listCourses)listCourses.dateEnd=this.get('select').pick;
					},
				});
			},
			openTime:function(b){
				var self=this;
				$("#ip-timeS").pickatime({
					clear : '',
					onStart: function() {
					    this.set('select', 0);
					    $("#ip-timeS_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(listCourses)listCourses.timeStart=this.get('select').pick;
					},
					onOpen:function(){
						$("#ip-timeS_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-timeS_root").css({'pointer-events':'none'});
					},
				});
				$("#ip-timeE").pickatime({
					clear : '',
					onStart: function() {
					    this.set('select', 0);
					    $("#ip-timeE_root").css({'pointer-events':'none'});
					},
					onSet:function(){
						if(listCourses)listCourses.timeEnd=this.get('select').pick;
					},
					onOpen:function(){
						$("#ip-timeE_root").css({'pointer-events':'auto'});
					},
					onClose:function(){
						$("#ip-timeE_root").css({'pointer-events':'none'});
					},
				});
			},
			createCourse:function(){
				console.log(this.course);
				if(checkEmpty(this.course.name)) {
					alert("Title is empty!");
					return;
				}
				if(this.course.timeEnd==this.course.timeStart){
					alert("Time end equal time start!");
					return;
				}
				var text=this.course.info;
				text=text.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				this.course.info=text;
				$.ajax({
					url:"/user/course/create",
					method:'post',
					data:JSON.stringify(this.course),
					contentType:"application/json;charset=UTF8",
					success:function(id){
						if(id!=null){
							location.href="/course/"+id;
						}
					}
				})
			},
			onFocusout:function(){
				this.arr_result=[];
			},
			onFilter:function(){
				this.arr_result=[];
				var n=$("#ip_search").val().toUpperCase()||'';
				if(!n.trim()){
					this.setCurrent();
					return;
				}
				this.current=[false,false,false,false];
				var arr=n.split(' ');
				var ques=[];
				arr.forEach(function(a){
					var b=a.trim();
					if(!b=='') ques.push(b);
				});
				this.courses_temp=[];
				var index=[];
				var self=this;
				var max=0;
				this.courses.forEach(function(c){
					var n=0;
					var name=c.name.toUpperCase();
					ques.forEach(function(q){
						if(name.indexOf(q)!=-1){
							n++;
						}
					});
					if(n!=0) {
						if(n>=max) {
							self.arr_result[max]={"id":c.id,"name":c.name};
							self.courses_temp[max]=c;
							max++;
						}else {
							self.arr_result[n]={"id":c.id,"name":c.name};
							self.courses_temp[n]=c;
						}
					}
				});
			},
			setCurrent:function(x){
				if(x==null) x=this.currentIndex;
				$("#ip_search").val('');
				this.currentIndex=x;
				this.current=[false,false,false,false];
				this.current[x]=true;
				this.courses_temp=this.courses_arr[x];
			},
			formatDate:function(today){
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!

				var yyyy = today.getFullYear();
				if(dd<10){
				    dd='0'+dd;
				} 
				if(mm<10){
				    mm='0'+mm;
				} 
				var today = dd+'/'+mm+'/'+yyyy;
				return today;
			}
		}
	});
	listCourses.courses=courses;
	listCourses.recently=recently;
	$("#ip_search").keyup(function(){
		listCourses.onFilter();
	});
	$("#ip_search").focusout(function(){
		setTimeout(() => {
			listCourses.onFocusout();
		}, 500);
	});
});

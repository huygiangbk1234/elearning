window.addEventListener("load",function(){
	var admin=new Vue({
		el:"#wrapper",
		data:{
			home:{
				course:0,
				student:0,
				teacher:0,
				data:[{
		            period:'2010 T1',
		            course: 100,
		        }, {
		        	 period: '2010 T2',
			            course: 200,
		        }, {
		        	 period: '2010 T3',
			            course: 300,
		        }, {
		        	 period: '2010 T4',
			            course: 400,
		        }, {
		        	 period: '2010 T5',
			            course: 500,
		        }, {
		        	 period: '2010 T6',
			            course: 900,
		        }, {
		        	 period: '2010 T7',
			            course: 500,
		        }, {
		        	 period: '2010 T8',
			            course: 640,
		        }, {
		        	 period: '2010 T9',
			            course: 1200,
		        }, {
		        	 period: '2010 T10',
			            course: 1120,
		        }]
			},
			
		},
		methods:{
			loadNewCourse:function(){
				this.home.course=100;
			},
			loadNewCourse:function(){
				this.home.student=100;
			},
			loadNewCourse:function(){
				this.home.teacher=100;
			},
			
		}
	
	});
	 Morris.Area({
	        element: 'morris-area-chart',
	        data: admin.home.data,
	        xkey: 'period',
	        ykeys: ['course'],
	        labels: ['course'],
	        pointSize: 2,
	        hideHover: 'auto',
	        resize: true
	    });
});
window.addEventListener('load',function(){
	var p=new Vue({
		el:"#profile",
		data:{
			user:{fullname:"",nickname:"",tel:""},
			members:[],
			courses:[],
		},
		watch:{
			members:function(){
				this.members.forEach(function(m){
					m.isEdit=false;
				})
			}
		},
		methods:{
			toggleEdit:function(index){
				if(this.members[index].isEdit){
					var url="/user/profile/update/alias?idMember="+this.members[index].id+"&alias="+this.members[index].alias;
					$.ajax({
						url:url,
						method:'post'
					})
				}
				this.members[index].isEdit=!this.members[index].isEdit;
				p.$forceUpdate();
			},
		
		}
	});
	p.user=test;
	p.members=m;
	p.courses=c;
});
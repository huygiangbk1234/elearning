
window.addEventListener("load",function(){
	var groups=new Vue({
		el:'#groups',
		data:{
			idCourse:-1,
			groups:[],
			members:[],
			currentGroup:null,
			arr:[],
			arrResult:[],
			tempMember:{alias:""},
			current:0,
		},
		watch:{
			current:function(n){this.setPl()},
			arrResult:function(){this.current=0;this.setPl()},
		},
		methods:{
			autoGroup:function(){
				if(this.groups.length==0) return;
				var members=[];
				var idGroups=[];
				var n=[];
				var arr=[];
				for(var i=0;i<this.groups.length;i++){
					var id=this.groups[i].id;
					var k=this.groups[i].members.length;
					idGroups.push(id);
					n.push(k);
				}
				for(var i=0;i<idGroups.length;i++){
					arr.push({id:idGroups[i],k:n[i]});
				}
				this.members.forEach(function(m){
					if(idGroups.indexOf(m.idGroup)==-1){
						members.push(m);
					}
				});
				arr.sort(function(a, b) {
				    return a.k - b.k;
				});
				var i=0;
				while(members.length>0){
					if(arr[i].k==arr[i+1].k){
						i=0;
						this.changeGroupMember(members[0].id,arr[i].id);
						members.splice(0,1);
						arr[i].k++;
					}else if(arr[i].k<arr[i+1].k){
						this.changeGroupMember(members[0].id,arr[i].id);
						members.splice(0,1);
						arr[i].k++;
					}else{
						i++;
						this.changeGroupMember(members[0].id,arr[i].id);
						members.splice(0,1);
						arr[i].k++;
						if(i==arr.length-1)i=0;
					}
				}
				location.reload();
			},
			setPl:function(){
				if(this.arrResult.length==0)return;
				this.tempMember=this.arrResult[this.current];
			},
			openAdd:function(index){
				this.currentGroup=this.groups[index];
				$("#modalAddMember").modal('show');
			},
			save:function(){
				if(this.currentGroup){
					var id=this.currentGroup.id;
					var x=[];
					for(var i=0;i<this.arr.length;i++){
						x.push(this.arr[i].id);
					}
					if(x.length>0){
						var url="/course/"+this.idCourse+"/groups/"+id+"/addlist";
						$.ajax({
							url:url,
							method:'post',
							data:JSON.stringify(x),
							contentType:"application/json;charset=UTF-8",
							success:function(){
								location.reload();
							}
						});
					}
				}
				this.hide();
			},
			hide:function(){
				$("#modalAddMember").modal('hide');
				this.arrResult=[];
				this.tempMember={alias:""};
			},
			removeGroup:function(index){
				if(confirm("Are you sure?")){
					var g=this.groups[index];
					var url="/course/"+this.idCourse+"/groups/delete?idGroup="+g.id;
					$.ajax({
						url:url,
						success:function(){
							location.reload();
						}
					});
				}
			},
			removeMember:function(member){
				if(confirm("Are you sure?")){
					var id=member.id;
					this.changeGroupMember(id,-1);
				}
			},
			changeGroupMember:function(idUser,idGroup){
				var idGroup=idGroup||-1;
				var url="/course/"+this.idCourse+"/groups/updatemember?idUser="+idUser+"&idGroup="+idGroup;
				$.ajax({
					url:url,
					success:function(){
						location.reload();
					}
				});
			},
			addMember:function(){
				$("#ip-add").val("");
				this.arrResult=[];
				if(!this.tempMember) {
					groups.$forceUpdate();
					return;
				}
				var b=false;
				for(var i=0;i<this.arr.length;i++){
					var m=this.arr[i];
					if(m==this.tempMember){
						b=true;break;
					}
				}
				if(!b){
					this.arr.push(this.tempMember);
				}
				this.tempMember={alias:""};
				groups.$forceUpdate();
			},
			remove:function(index){
				this.arr.splice(index,1);
			},
			onKeyUp:function(e){
				if(e.keyCode==13){this.addMember();return}
				if(e.keyCode==40){
					if(this.current+1<this.arrResult.length) this.current++;
					return;
				}else if(e.keyCode==38){
					if(this.current-1>=0) this.current--;
					return;
				}
				this.arrResult=[];
				var t=$("#ip-add").val().toUpperCase();
				if(checkEmpty(t))return;
				for(var i=0;i<this.members.length;i++){
					var m=this.members[i];
					if(m.alias.toUpperCase().indexOf(t)!=-1){
						this.arrResult.push(m);
					}
				}
			},
			toggleEdit:function(index){
				var g=this.groups[index];
				if(g.isEdit){
					var name=g.name;
					if(checkEmpty(name)){
						g.name=g.tempName;
					}else{
						var url="/course/"+this.idCourse+"/groups/update?idGroup="+g.id+"&name="+name;
						$.ajax({
							url:url,
						});
					}
				}else{
					g.tempName=g.name;
				}
				g.isEdit=!g.isEdit;
				
				groups.$forceUpdate();
			},
			toggleOpen:function(index){
				var g=this.groups[index];
				g.isOpen=!g.isOpen;
				groups.$forceUpdate();
			},
			fix:function(){
				this.groups.forEach(function(g){
					g.isOpen=false;
					g.isEdit=false;
				});
			},
			createGroup:function(){
				var n=Math.floor($("#ip-nums").val());
				if(n<=0)return;
				var list=[];
				for(var i=1;i<=n;i++){
					var g={
							name:"Group "+i,
							idCourse:this.idCourse,
							createdAt:new Date().getTime(),
					};
					list.push(g);
				}
				var url="/course/"+this.idCourse+"/groups/create";
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(list),
					contentType:"application/json;charset=UTF-8",
					success:function(data){
						location.reload();
					},
					error:function(){alert("error")}
				})
				$("#myModal").modal("hide");
			},
			deleteGroup:function(index){
				if(confirm("Are you sure?")){
					var url="/course/"+this.idCourse+"/groups/delete?id="+groups[index].id;
					$.ajax({
						url:url,
						method:'post',
						success:function(data){
							location.reload();
						},
						error:function(){
							console.log("error");
						}
					});
				}
			},
			loadUserSubmit:function(){
				var url="/course/"+this.idCourse+"/assignment/"+this.assignment.id+"/loadUserSubmit?not="+true;
				var s=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						user
					}
				})
			},
			loadUserNotSubmit:function(){
				
			},
			loadGroupSubmit:function(){
				
			},
			loadGroupNotSubmit:function(){
				
			},
		}
	});
	groups.idCourse=test.idCourse;
	groups.groups=test.list;
	groups.members=test.listMember;
	groups.fix();
});
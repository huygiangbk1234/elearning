var remove=function(e){
		$(e.target).parent().remove();
	}
var onChangeFilePost=function(){
	var f=$("#ipModal");
	var name=f.val();
	var b=false;
	$('#listFile [type=file]').each(function(index,input){
		if($(input).val()==name) b=true;
	});
	if(b) return;
	name=name.split("\\");
	name=name[name.length-1];
	
	f.after("<input type='file' onchange='onChangeFilePost()' id='ipModal'/>");
	var d="<div class='f'>"+
	"<span class='icon'><i class='fa fa-file-text-o' aria-hidden='true'></i>"+name+"</span>"+
	"<i class='fa fa-times r' aria-hidden='true' onclick='remove(event)'></i>"+
	"</div>";
	var a=$(d).appendTo($("#listFile"));
	f.attr({"id":'','name':"files"})
	$(a).append(f);
};
window.addEventListener("load",function(){
	var post=new Vue({
		el:"#body_posts",
		data:{
			idCourse:-1,
			posts:[],
		},
		methods:{
			sendTopic:function(){
				var title=$("#titleModal").val();
				if(title.replace(/(?:\r\n|\r|\n)/g, '').trim()=='') {
					alert("Title is empty!");
					return;
				}
				title=title.trim();
				var text=$("#textModal").val();
				var b=$('#listFile [type=file]').length==0?false:true;
				if(text.replace(/(?:\r\n|\r|\n)/g, '').trim()!==''){
					b=b||true;
					text=text.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				}else{b=b||false;}
				if(!b) {
					alert("Content is empty!");
					return;
				}
				var form = document.getElementById("formModal");
				var formData = new FormData(form);
				formData.append('content',text);
				formData.append('title',title);
				var url="/course/"+this.idCourse+"/forums/send_post";
				$("modalPost").modal('hide');
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						location.reload();
					},
					error:function(){
						alert("error send data");
					}
				});
			},
			loadView:function(){
				var self=this;
				for(var i=0;i<this.posts.length;i++){
					var j=i;
					var p=this.posts[j];
					p.numsComment=0;
					var url="/course/"+this.idCourse+"/forum/"+p.id+"/count_commemt";
					$.ajax({
						n:i,
						url:url,
						method:"post",
						success:function(data){
							self.posts[this.n].numsComment=data;
							post.$forceUpdate();
						}
					});
				}
				
			},
			formatDate:function(time){
				var today=new Date(time);
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!

				var yyyy = today.getFullYear();
				if(dd<10){
				    dd='0'+dd;
				} 
				if(mm<10){
				    mm='0'+mm;
				} 
				var h=today.getHours();
				var m=today.getMinutes();
				
				var today =h+"h"+m+" "+ dd+'/'+mm+'/'+yyyy;
				return today;
			}
		}
	});
	post.posts=test.posts;
	post.idCourse=test.idCourse;
	post.loadView();
});
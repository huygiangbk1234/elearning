window.addEventListener("load",function(){
	var qb=new Vue({
		el:"#question_bank",
		data:{
			listSubjects:[],
			index:0,
			indexTest:-1,
			currentSubject:{id:-1},
			listResultFilter:[],
			resultSearch:null,
			idCourse:-1,
			
			total:0,
			
			min:0,
			
			nums:0,
			numsEasy:0,
			numsNormal:0,
			numsHard:0,
			easy:100,
			normal:0,
			hard:0,
			
			library:[],
			isEdit:false,
		},
		watch:{
			index:function(){
				this.resultSearch=null;
				$("#input_search").val("");
			},
			indexTest:function(i){
				this.currentSubject=this.listSubjects[i];
				this.nums=this.min;
				this.easy=100;
				this.numsEasy=this.min;
			},
			easy:function(e){
				this.normal=100-e-this.hard;
				if(this.normal<0){
					this.normal=0;
					this.hard=100-this.easy;
				}
				this.countQuestion();
			},
			normal:function(n){
				this.hard=100-this.easy-n;
				if(this.hard<0){
					this.hard=0;
					this.easy=100-this.normal;
				}
				this.countQuestion();
			},
			hard:function(){
				this.hard=100-this.easy-this.normal;
			},
			resultSearch:function(){
				console.log("A");
			},
		},
		methods:{
			toggleEditQuestion:function(){
				this.isEdit=!this.isEdit;
			},
			addAnswer:function(){
				this.resultSearch.answers.push({answer:"",'true':false});
			},
			removeAnswer:function(index){
				this.resultSearch.answers.splice(index,1);
			},
			saveEditQuestion:function(){
				console.log(this.resultSearch);
				var self=this;
				this.resultSearch.answers.forEach(function(a,index){
					a.answer=a.answer.trim();
					if(a.answer.replace(/(?:\r\n|\r|\n)/g, '')=='')
						self.resultSearch.answers.splice(index,1);
				});
				var url="/course/"+this.idCourse+"/question_bank/updatequestion";
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(self.resultSearch),
					contentType:"application/json;charser=UTF-8",
					success:function(data){
						self.onSelectQuestion(self.resultSearch);
					},
					error:function(e){console.log(e)}
				});
			},
			toggleImport:function(index){
				var b=this.library[index].ofCourse;
				console.log(b);
				var url="/course/"+this.idCourse+"/question_bank/updatelibrary?idSubject="+this.library[index].id+"&delete="+b;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
					},
					error:function(e){console.log(e)}
				});
			},
			loadLibrary:function(){
				var url="/course/"+this.idCourse+"/question_bank/library";
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						console.log(data);
						self.library=data;
					},
					error:function(e){console.log(e)}
				});
			},
			getA:function(k){
				return String.fromCharCode(k+65);
			},
			showResult:function(data){
				this.resultSearch=data;
			},
			onSelectQuestion:function(q){
				this.listResultFilter=[];
				$("#input_search").val(q.question);
				var url="/course/"+this.idCourse+"/question/get?id="+q.id;
				var self=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						self.resultSearch=data;
					},
					error:function(e){console.log(e)}
				});
				
			},
			onFilter:function(){
				console.log("filtering");
				this.resultSearch=null;
				var text=$("#input_search").val().replace(/(?:\r\n|\r|\n)/g, ' ');
				if(text.trim()==''){
					this.listResultFilter=[];
					return;
				}
				var self=this;
				var data=text.split(' ');
				var url="/course/"+this.idCourse+"/questions/filter?idSubject="+this.listSubjects[this.index].id;
				this.listResultFilter=[];
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(data),
					contentType: 'application/json; charset=UTF-8',
					success:function(data){
						self.listResultFilter=data;
					},
					error:function(e){console.log(e)}
				});
				
				
				
			},
			onChange:function(){
				if(this.nums<this.min)this.nums=this.min;
				else if(this.nums>this.currentSubject.max)this.nums=this.currentSubject.max;
				this.countQuestion();
			},
			submit:function(e){
				var self=this;
				if(self.nums==0) {
					alert("total questions is 0!");
					 e.preventDefault();
					return false;
				}
				$('#formTest').submit(function() {
				    $("#numsEasy").val(self.numsEasy);
				    $("#numsNormal").val(self.numsNormal);
				    $("#numsHard").val(self.numsHard);
				    return true;
				});
			},
			countQuestion:function(){
				this.numsEasy=Math.round(this.nums*this.easy/100);
				var n=this.nums-this.numsEasy;
				this.numsNormal=Math.round(this.nums*this.normal/100);
				this.numsNormal=this.numsNormal>n?n:this.numsNormal;
				this.numsHard=this.nums-this.numsEasy-this.numsNormal;
			},
		},
	});
	qb.listSubjects=test.list;
	qb.indexTest=0;
	qb.idCourse=test.idCourse;
	if(isLoadLibrary) qb.loadLibrary();
});
Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};
var checkEmpty=function(text){
	if(text.replace(/(?:\r\n|\r|\n)/g, '')=='')return true;
	return false;
};
var getNameVideo=function(name){
	var tok=name.split(".");
	if(tok.length==2)return tok[0];
	return name;
}

window.addEventListener("load", function() {
	var msg = new Vue({
		el : "#content-info",
		data:{
			current:1,
			title:[],
			tab : [ false, false, false ],
			highLightTab:[0,0,0],
			arrMsgCurrent:[],
			currentManager:null,
			message:null,
			notifyManager:{
				notifications:[],
				isMoreNotification:true,
				isLoadingGroup:false,
				loadMoreNotification:function(){
					var url="/notification/get?from="+this.notifications.length;
					var self=this;
					this.isLoadingGroup=true;
					$.ajax({
						url:url,
						success:function(data){
							self.isLoadingGroup=false;
							console.log(data);
							if(data!=null){
								self.notifications=self.notifications.concat(data);
							}else{
								self.isMoreNotification=false;
							}
							if(data.length==0)self.isMoreNotification=false;
						}
					});
				},
				onHandleScrollContent:function(){
					if(!this.isMoreNotification) return;
					var d=document.getElementById('content');
					if(d.scrollTop+d.clientHeight+1<d.scrollHeight||this.isLoadingGroup) return ;
					console.log("load notification");
					this.loadMoreNotification();
				},
				confirmReadNotification:function(){
					
				},
				addNotification:function(data){
					if(js.msg.tab[0]){
						var snd = new Audio("/custom/sound/msg_new.wav");
						snd.play();
					}else js.msg.onhighLightTab(0,true);
					this.notifications.unshift(data);
				},
			},
			eventManager:{
				events:[],
				isLoadingEvent:false,
				isMoreEvent:false,

				open:function(a){
					location.href="/course/"+a.idCourse+"/assignment/"+a.id;
				},
				formatDate:function(time){
					var today = new Date(time);
					var dd = today.getDate();
					var mm = today.getMonth() + 1;
					var yyyy = today.getFullYear();
					if (dd < 10) {
						dd = '0' + dd;
					}
					if (mm < 10) {
						mm = '0' + mm;
					}
					var h = today.getHours();
					var m = today.getMinutes();
					if(h<10) h='0'+h;
					if(m<10) m='0'+m;
					var today = h + "h" + m + ", " + dd
							+ '/' + mm + '/' + yyyy;
					return today;
				},
				loadMoreEvent:function(){
					var url="/events/get?from="+this.events.length;
					var self=this;
					this.isLoadingEvent=true;
					$.ajax({
						url:url,
						success:function(data){
							self.isLoadingEvent=false;
							console.log(data);
							if(data!=null){
								self.events=self.events.concat(data);
							}else{
								self.isMoreEvent=false;
							}
							if(data.length==0)self.isMoreEvent=false;
						}
					});
				},
				onHandleScrollContent:function(){console.log("load event")},
				
			},
			msgManager:{
				group:[],
				isMoreGroup:true,
				arrAvatar:[],
				arrMessage:[],
				isShowMessage:false,
				idGroup:null,
				isLoadingMessage:false,
				isLoadingGroup:false,
				confirmReadMessage:function(idGroup){
					this.getGroupById(idGroup).read=true;
					$.ajax({
						url:"/message/read",
						method:'post',
						data:JSON.stringify({idGroup:idGroup}),
						contentType: 'application/json; charset=UTF-8',
						success:function(data){},
						error:function(e){console.log(e)}
					
					});
				},
				onHandleScrollContent:function(){
					if(!this.isMoreGroup) return;
					var d=document.getElementById('content');
					if(d.scrollTop+d.clientHeight+1<d.scrollHeight||this.isLoadingGroup) return ;
					console.log("load group");
					this.loadGroupMessage();
				},
				onHandleScrollMessage:function(){
					if(document.getElementById('kkk').scrollTop>0||this.isLoadingMessage) return ;
					this.loadMoreMessage();
				},
				scrollBottom:function(time,del){
					if(this.idGroup==null) return;
					time=time||100;
					del=del||0;
					setTimeout(function(){
						var objDiv=document.getElementById('kkk');
						objDiv.scrollTop = (objDiv.scrollHeight-del);
					},time);
				},
				getAvatar:function(idUser){
					return "/custom/img/"+this.arrAvatar[idUser];
				},
				sendMessage(){
					var text=$("#message_text").val();
					if(text.replace(/(?:\r\n|\r|\n)/g, '').trim()!==''){
						text=text.replace(/(?:\r\n|\r|\n)/g, '<br/>');
					}
					$("#message_text").val("");
					js.msg.sendMsg(-1,text,this.idGroup);
					msg.$forceUpdate();
				},
				addArrMessage:function(arr){
					if(arr.length==0) return;
					var idGroup=arr[0].idGroup;
					var g=this.getGroupById(idGroup);
					var index=this.getIndexGroup(g);
					for(var i=0;i<arr.length;i++){
						if(arr[i].idAccount==js.user.id){
							arr[i].me=true;
						}
						g.listMessage.push(arr[i]);
					}
					if(!this.isShowMessage)return;
					this.openMessage(index,1);
					this.scrollBottom(100,document.getElementById('kkk').scrollHeight);
					msg.$forceUpdate();
				},
				getGroupById:function(id){
					for(var i=0;i<this.group.length;i++){
						var group=this.group[i];
						if(group.id==id) return group;
					}
					return null;
				},
				getIndexGroup:function(g){
					return this.group.indexOf(g);
				},
				addMessage:function(data){
					// {from: 34, to: -1, idGroup: 17, content: "hello"}
					var me=data.from==js.user.id?true:false;
					var idGroup=data.idGroup;
					var m={"idAccount":data.from,"content":data.content,"me":me,"isRead":me,"isDelete":false};
					var self=this;
					var group=this.getGroupById(idGroup);
					group.read=m.me;
					var index=this.getIndexGroup(group);
					if(!m.me){
						if(js.msg.tab[1]){
							if(this.isShowMessage){
								if(this.idGroup==idGroup){
									var snd = new Audio("/custom/sound/msg_type.mp3");
									snd.play();
								}else{
									js.msg.onhighLightTab(1,true);
								}
							}else{
								var snd = new Audio("/custom/sound/msg_new.wav");
								snd.play();
							}
						}else{
							js.msg.onhighLightTab(1,true);
						}
					}
					if(group!=null){
						group.listMessage.unshift(m);
						if(this.isShowMessage&&this.idGroup==idGroup){
							this.arrMessage.push(m);
							if(!group.read)this.confirmReadMessage(idGroup);
						}
						this.group.move(index,0);
					}else{
						$.ajax({
							url:"/groupchat/get?id="+idGroup,
							method:'post',
							success:function(data){
								self.group.unshift(data);
							}
						});
					}
					this.scrollBottom();
					msg.$forceUpdate();
				},
				openMessage:function(key,b){
					b=(b==1)?false:true;
					var g=this.group[key];
					this.arrAvatar=[];
					this.arrMessage=g.listMessage.slice().reverse();
					for(var j=0;j<g.listMember.length;j++){
						var a=g.listMember[j];
						this.arrAvatar[a.id]=a.avatar;
					}
					this.isShowMessage=true;
					this.idGroup=g.id;
					if(!g.read)this.confirmReadMessage(this.idGroup);
					
					if(b)this.scrollBottom();
					msg.$forceUpdate();
				},
				onBack:function(){
					this.isShowMessage=false;
					this.idGroup=null;
					js.msg.highLightTab[1]=0;
				},
				loadGroupMessage:function(){
					this.isLoadingGroup=true;
					var self=this;
					$.ajax({
						url:"/groupchat/get/"+this.group.length,
						method:'post',
						success:function(data){
							self.isLoadingGroup=false;
							if(data==null) return;
							if(data.length==0){
								self.isMoreGroup=false;
								return;
							}
							for(var i=0;i<data.length;i++){
								var g=data[i];
								g.isMoreMessage=true;
								for(j=0;j<g.listMessage.length;j++){
									var m=g.listMessage[j];
									if(m.idAccount==js.user.id){
										m.me=true;
									}
								}
								self.group.push(g);
								msg.$forceUpdate();
							}
						},
						error:function(){self.isLoadingGroup=false;}
					});
				},
				loadMoreMessage:function(){
					var group=this.getGroupById(this.idGroup);
					if(!group.isMoreMessage) return;
					this.isLoadingMessage=true;
					var self=this;
					$.ajax({
						url:"/message/get/"+group.id+"/"+group.listMessage.length,
						method:'post',
						success:function(data){
							if(data.length==0) group.isMoreMessage=false;
							self.addArrMessage(data);
							self.isLoadingMessage=false;
							msg.$forceUpdate();
						},
						error:function(){self.isLoadingMessage=false;}
					});
				},
				
			}
		},
		created:function(){
			this.handleMessage();
		},
		components:{
			itemNotify:{
				props: ['link','msg'],
				template:"<p class='item-notify'><a v-bind:href='{link}' class='ellipsis'>{{msg}}</a></p>",
			},
			itemMessage:{
				props: ['a','msg'],
				template:"<div class='message'><img v-bind:src='{link}' alt='avatar' /><div class='info'><div class='name'>{{a.name}}</div><div class='msg ellipsis'>{{msg}}</div></div></div>",
				computed:{
					link:function(){return '/custom/img/'+this.a.avatar;}
				}
			},
			itemEvent:{
				props:['arr'],
				template:"<div class='item-event'><div class='time'>Thứ 6, 2-11-2017, 12:00</div><div class='info ellipsis'>Assignment 1</div></div>"
			},
		},
		methods:{
			onHandleScrollContent:function(){
				this.currentManager.onHandleScrollContent();
			},
			handleMessage:function(){
				var socket = new SockJS('/message');
				var stompClient = Stomp.over(socket);
				var self=this;
				stompClient.connect({}, function(frame) {
					stompClient.subscribe('/user/queue/notify', function(notification) {
						var type=notification.headers.type;
						if(type=="message") self.notifyMessage(JSON.parse(notification.body));
						else if(type=="notification") self.notifyNotification(JSON.parse(notification.body));
					});
				});
			},
			notifyMessage:function(data){
				this.msgManager.addMessage(data);
				msg.$forceUpdate();
			},
			notifyNotification:function(data){
				this.notifyManager.addNotification(data);
				msg.$forceUpdate();
			},
			sendMsg:function(id,content,idGroup){
				idGroup=idGroup||-1;
				var data={
					content:content,
					from:"",
					to:id,
					idGroup:idGroup,
				};
				$.ajax({
					url:"/message/send",
					method:'post',
					data:JSON.stringify(data),
					contentType: 'application/json; charset=UTF-8',
					dataType: 'json',
					success:function(s){
						console.log(s);
					},
					error:function(e){
						console.log(e)
					}
				});
			},
			openTab : function(tab) {
				this.current=tab;
				if(tab==0)this.currentManager=this.notifyManager;
				else if(tab==1)this.currentManager=this.msgManager;
				else if(tab==2)this.currentManager=this.eventManager;
				this.tab = [ false, false, false ];
				this.tab[tab] = true;
				this.msgManager.onBack();
				this.highLightTab[tab]=0;
				msg.$forceUpdate();
			},
			onhighLightTab:function(tab,b){
				this.highLightTab[tab]++;
				if(b){
					var snd = new Audio("/custom/sound/msg_new.wav");
					snd.play();
				}
				msg.$forceUpdate();
			},
			openModalChatMsg:function(){
				$("#modalChatMsg").modal('show');
			},
		}
	});
	msg.title=title_pn_notification;
	js.msg=msg;
	msg.msgManager.loadGroupMessage();
	msg.notifyManager.loadMoreNotification();
	msg.eventManager.loadMoreEvent();
	msg.openTab(1);
	$("#modalChatMsg").appendTo($("body"));
	var msgchat=new Vue({
		el:"#modalChatMsg",
		data:{
			content:"",
			member:null,
			tempEmail:{alias:""},
			arrResult:[],
			current:0,
		},
		watch:{
			current:function(n){this.setPl()},
			arrResult:function(){this.current=0;this.setPl()},
		},
		methods:{
			closeModalChatMsg:function(){
				this.arr=[];
				this.member=null;
				this.arrResult=[];
				this.tempEmail={alias:""};
				$("#ip-s").val("");
				$("#modalChatMsg").modal('hide');
			},
			sendMessage:function(){
				if(this.member==null){
					alert("Error Receiver!");
					return;
				}
				if(checkEmpty(this.content)){
					alert("Error Content!");
					return;
				}
				js.msg.sendMsg(this.member.member.id,this.content);
				this.closeModalChatMsg();
			},
			setPl:function(){
				if(this.arrResult.length==0){
					this.tempEmail={alias:""};
					return;
				}
				this.tempEmail=this.arrResult[this.current];
			},
			addMember:function(){
				if(!this.arrResult)return;
				this.member=this.arrResult[this.current];
			},
			
			onKeyUp:function(e){
				if(e.keyCode==40){
					if(this.current+1<this.arrResult.length) this.current++;
					return;
				}else if(e.keyCode==38){
					if(this.current-1>=0) this.current--;
					return;
				}
				console.log("filtering");
				this.arrResult=[];
				var text=$("#ip-s").val();
				if(text.replace(/(?:\r\n|\r|\n)/g, '').trim()==''){
					return;
				}
				var self=this;
				$.ajax({
					url:"/member/filter?alias="+text,
					method:'post',
					success:function(data){
						self.arrResult=data;
					},
					error:function(e){console.log(e)}
				});
			},
		}
	});

});




window.addEventListener("load",function(){
	var assignment=new Vue({
		el:'#assignments',
		data:{
			idCourse:-1,
			finish:"",
			unfinish:"",
			isFinish:false,
			list:[],
			title:{},
			
		},
		components:{
			my_table:{
				template:"#template-table",props:['list','detail'],
				methods:{
					isVisible:function(a){
						var time=new Date().getTime();
						if(this.$parent.isFinish){
							if(a.timeEnd<=time)return true;
							return false;
						}else{
							if(a.timeEnd>time)return true;
							return false;
						}
					},
					getTime:function(time){
						var today=new Date(time);
						var dd = today.getDate();
						var mm = today.getMonth()+1; //January is 0!
						var hh= today.getHours();
						var mins=today.getMinutes();
						var yyyy = today.getFullYear();
						if(dd<10){
						    dd='0'+dd;
						} 
						if(mm<10){
						    mm='0'+mm;
						} 
						if(hh<10){
							hh='0'+hh;
						} 
						if(mins<10){
							mins='0'+mins;
						} 
						var today = hh+"h"+mins+" "+dd+'/'+mm+'/'+yyyy;
						return today;
					}
				}
			}
		},
		methods:{
			toggleFilter:function(){
				this.isFinish=!this.isFinish;
			},
			loadAllAssignment:function(){
				var url="/course/"+this.idCourse+"/assignments/get";
				var s=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						s.list=data;
						s.fix();
					}
				})
			},
			
			fix:function(){
				this.list.forEach(function(l){
					
				});
			},
		}
	});
	assignment.idCourse=test.idCourse;
	assignment.finish=test.finish;
	assignment.unfinish=test.unfinish;
	assignment.title=test.title;
	assignment.loadAllAssignment();
});
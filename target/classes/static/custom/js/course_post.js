var remove = function(e) {
	$(e.target).parent().remove();
}
var onChangeFilePost = function() {
	var f = $("#ipModal");
	var name = f.val();
	var b = false;
	$('#listFile [type=file]').each(function(index, input) {
		if ($(input).val() == name)
			b = true;
	});
	if (b)
		return;
	name = name.split("\\");
	name = name[name.length - 1];

	f.after("<input type='file' onchange='onChangeFilePost()' id='ipModal'/>");
	f.attr({
		"id" : '',
		'name' : "files"
	})
	var d = js.post.createFileInput(name);
	var a = $(d).appendTo($("#listFile"));
	$(a).append(f);
};
var onChangeFilePostEdit = function() {
	var f = $("#ipModalEdit");
	var name = f.val();
	var b = false;
	$('#listFileEdit [type=file]').each(function(index, input) {
		if ($(input).val() == name)
			b = true;
	});
	if (b)
		return;
	name = name.split("\\");
	name = name[name.length - 1];

	f
			.after("<input type='file' onchange='onChangeFilePostEdit()' id='ipModalEdit'/>");
	var d = "<div class='f'>"
			+ "<span class='icon'><i class='fa fa-file-text-o' aria-hidden='true'></i>"
			+ name
			+ "</span>"
			+ "<i class='fa fa-times r' aria-hidden='true' onclick='remove(event)'></i>"
			+ "</div>";
	var a = $(d).appendTo($("#listFileEdit"));
	f.attr({
		"id" : '',
		'name' : "files"
	})
	$(a).append(f);
};
window
		.addEventListener(
				"load",
				function() {
					var File = Vue
							.extend({
								props : [ 'index' ],
								computed : {
									id : function() {
										return "files" + this.index;
									}
								},
								template : "<input type='file' name='' v-bind:id='id' v-on:change='$parent.onChangeFile(index)'/>"
							});
					var post = new Vue(
							{
								el : "#body_post",
								data : {
									member : {},
									post : {},
									postEdit : {},
									notify_idPost : null,
									notify_idComment : null
								},
								components : {
									file : {

									}
								},
								methods : {
									togglePin : function() {
										if (this.post.priority == 0)
											this.post.priority = 1;
										else
											this.post.priority = 0;
										var url = "/course/"
												+ this.post.idCourse
												+ "/forums/" + this.post.id
												+ "/pin_post?priority="
												+ this.post.priority;
										$.ajax({
											url : url,
											method : 'post',
											success : function(data) {
											},
											error : function() {
												alert("error send data");
											}
										});
									},
									downloadFile : function(file) {
										// file.name="a.mp4";
										// url="/course/"+file.idCourse+"/download?name="+file.name;
										url = "/custom/document/" + file.name;
										window.open(url, '_blank');
									},
									getImageFile : function(name) {
										var ext = name.split(".");
										ext = ext[ext.length - 1].toLowerCase();
										if (ext == 'txt')
											return "<i class='fa fa-file-text-o' aria-hidden='true'></i>";
										else if (ext == 'pdf')
											return "<i class='fa fa-file-pdf-o' aria-hidden='true' style='color:red'></i>";
										else if (ext == 'docx')
											return "<i class='fa fa-file-word-o' aria-hidden='true' style='color:blue'></i>";
										else if (ext == 'mp4')
											return "<i class='fa fa-file-video-o' aria-hidden='true' style='color:red'></i>";
										else if (ext == 'png' || ext == 'jpg'
												|| ext == 'gif')
											return "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
										return "<i class='fa fa-file' aria-hidden='true'></i>";
									},
									remove : function(e, index) {
										this.postEdit.files[index].isDelete = true;
										remove(e);
									},
									hideEditPost : function() {
										this.postEdit = {};
										$("#modalEdit").modal('hide');
									},
									deletePost : function(p) {
										if (confirm("Are you sure?")) {
											var url = "";
											if (!p)
												url = "/course/"
														+ this.post.idCourse
														+ "/forums/"
														+ this.post.id
														+ "/delete_post";
											else {
												url = "/course/"
														+ this.post.idCourse
														+ "/forum/"
														+ this.post.id
														+ "/delete_commemt?idPostReply="
														+ p.id;
											}
											$.ajax({
												url : url,
												method : 'post',
												success : function(data) {
													location.reload();
												},
												error : function() {
													alert("error send data");
												}
											});
										}
									},
									saveEdit : function() {
										var title = this.postEdit.title || null;
										if (title) {
											if (title.replace(
													/(?:\r\n|\r|\n)/g, '')
													.trim() == '') {
												return;
											}
											title = title.trim();
										}

										var text = this.postEdit.editcontent;

										var n = this.postEdit.files.length;
										var fileDelete = [];
										for (var i = 0; i < this.postEdit.files.length; i++) {
											if (this.postEdit.files[i].isDelete) {
												n--;
												fileDelete
														.push(this.postEdit.files[i].id);
											}
										}
										var b = n == 0 ? false : true;
										if (text.replace(/(?:\r\n|\r|\n)/g, '')
												.trim() !== '') {
											b = b || true;
											text = text.replace(
													/(?:\r\n|\r|\n)/g, '<br/>');
										} else {
											b = b || false;
										}
										if (!b)
											return;

										var form = document
												.getElementById("formEdit");
										var formData = new FormData(form);
										formData.append('content', text);
										if (title)
											formData.append('title', title);
										formData.append('fileDelete',
												fileDelete);
										var url = "";
										if (this.postEdit.type == "post")
											url = "/course/"
													+ this.post.idCourse
													+ "/forums/" + this.post.id
													+ "/update_post";
										else if (this.postEdit.type == "postReply") {
											url = "/course/"
													+ this.post.idCourse
													+ "/forum/"
													+ this.post.id
													+ "/update_commemt?idPostReply="
													+ this.postEdit.id;
										}
										this.hideEditPost();
										$.ajax({
											url : url,
											method : 'post',
											contentType : false,
											processData : false,
											data : formData,
											success : function(data) {
												location.reload();
											},
											error : function() {
												alert("error send data");
											}
										});
									},
									editPost : function(p) {
										if (p) {
											this.postEdit = jQuery.extend(true,
													{}, p);
											this.postEdit.type = "postReply";
										} else {
											this.postEdit = jQuery.extend(true,
													{}, this.post);
											this.postEdit.type = "post";
										}
										var find = '<br/>';
										var re = new RegExp(find, 'g');
										this.postEdit.editcontent = this.postEdit.content
												.replace(re, '\n');
										$("#modalEdit").modal('show');
										post.$forceUpdate();
									},
									sendPost : function() {
										var text = $("#textModal").val();
										var b = $('#listFile [type=file]').length == 0 ? false
												: true;
										if (text.replace(/(?:\r\n|\r|\n)/g, '')
												.trim() !== '') {
											b = b || true;
											text = text.replace(
													/(?:\r\n|\r|\n)/g, '<br/>');
										} else {
											b = b || false;
										}
										if (!b)
											return;
										var form = document
												.getElementById("formModal");
										var formData = new FormData(form);
										formData.append('content', text);
										var url = "/course/"
												+ this.post.idCourse
												+ "/forum/" + this.post.id
												+ "/send_subcommemt?parent=-1";
										$("modalPost").modal('hide');
										$.ajax({
											url : url,
											method : 'post',
											contentType : false,
											processData : false,
											data : formData,
											success : function(data) {
												location.reload();
											},
											error : function() {
												alert("error send data");
											}
										});
									},
									sendComment : function(index) {
										var text = $("#text" + index).val();
										var b = $('#listF' + index
												+ ' [type=file]').length == 0 ? true
												: false;
										if (text.replace(/(?:\r\n|\r|\n)/g, '')
												.trim() !== '') {
											b = b && false;
											text = text.replace(
													/(?:\r\n|\r|\n)/g, '<br/>');
										} else {
											b = b && true;
										}
										if (b)
											return;

										var form = document
												.getElementById("form" + index);
										var formData = new FormData(form);
										formData.append('content', text);
										var p = this.post.comments[index];
										var url = "/course/"
												+ this.post.idCourse
												+ "/forum/" + this.post.id
												+ "/send_subcommemt?parent="
												+ p.id;
										var self = this;
										$.ajax({
											url : url,
											method : 'post',
											contentType : false,
											processData : false,
											data : formData,
											success : function(data) {
												p.isLoadedComment = false;
												p.comments = [];
												self.openComment(index, true);
												$("#text" + index).val("");
												$('#listF' + index).empty();
											},
											error : function() {
												alert("error send data");
											}
										});
									},
									createFileInput : function(name) {
										return "<div class='f'>"
												+ "<span class='icon'><i class='fa fa-file-text-o' aria-hidden='true'></i>"
												+ name
												+ "</span>"
												+ "<i class='fa fa-times r' aria-hidden='true' onclick='remove(event)'></i>"
												+ "</div>";
									},
									onChangeFile : function(index) {
										var id = "files" + index;
										var f = $("#files" + index);
										var name = f.val();
										var b = false;

										$('#listF' + index + ' [type=file]')
												.each(function(index, input) {
													if ($(input).val() == name)
														b = true;
												});
										if (b)
											return;
										name = name.split("\\");
										name = name[name.length - 1];
										f.after("<div id='temp'></div>");
										new File({
											parent : this,
											propsData : {
												index : index
											}
										}).$mount("#temp");
										var list = $("#listF" + index);
										f.attr({
											"id" : '',
											'name' : "files"
										});

										var d = this.createFileInput(name);
										var a = $(d).appendTo(list);
										$(a).append(f);
									},
									fixHeight : function(index) {
										var p = this.post.comments[index];
										if (!p.comments)
											return 0;
										var n = p.comments.length > 3 ? 3
												: p.comments.length;
										return n * 75 + "px";
									},
									getSubCommentRender : function(index) {
										var p = this.post.comments[index];
										if (!p.comments)
											return [];
										return p.comments.slice(0).reverse();
									},
									onHandleScrollContentComment : function(index) {
										var p = this.post.comments[index];
										if (!p.isMore)
											return;
										var d = document.getElementById('list'
												+ index);
										if (d.scrollTop > 0 || p.isLoading)
											return;
										console.log("load comment");
										this.loadMoreComment(p, d);
									},
									loadMoreComment : function(p, div) {
										p.isLoading = true;
										var h = 0;
										if (div)
											h = div.scrollHeight;
										var url = "/course/"
												+ this.post.idCourse
												+ "/forum/" + this.post.id
												+ "/get_subcommemt?parent="
												+ p.id + "&from="
												+ p.comments.length;
										$.ajax({
											url : url,
											method : 'post',
											success : function(data) {
												if (data == null) {
													p.isMore = false;
												} else {
													p.comments = p.comments.concat(data);
													if (data.length == 0)
														p.isMore = false;
													if (div)
														setTimeout(function() {
															div.scrollTop = div.scrollHeight- h;
														}, 200);
												}
												p.isLoading = false;
											}
										});
									},
									openComment : function(index, b) {
										var p = this.post.comments[index];
										if (!p.isLoadedComment) {
											if (!p.comments) {
												p.isMore = true;
												p.comments = [];
											}
											this.loadMoreComment(p);
											p.isLoadedComment = true;
										}

										if (!b)$("#comments-post-" + index).toggle();
										setTimeout(function() {
											var l = document.getElementById("list"+ index);
											l.scrollTop = l.scrollHeight;
										}, 300);

									},
									goToByScroll : function(id) {
										var d = $("#" + id);
										$('html,body').animate({
											scrollTop : d.offset().top-80
										}, 'slow');
									},
									highLightDiv:function(id){
										$("#"+id).css('background-color','rgb(255,220,220)');
										setTimeout(function(){
											$("#"+id).css('background-color','transparent');
										},1000);
										setTimeout(function(){
											$("#"+id).css('background-color','rgb(255,220,220)');
										},1600);
										setTimeout(function(){
											$("#"+id).css('background-color','transparent');
										},2200);
									},
									a:function(index){
										var l = document.getElementById("list"+ i);
										console.log(l.scrollHeight+" "+l.clientHeight-l.scrollTop);
									},
									
									checkNotify : function() {
										var self=this;
										for (var i = 0; i < this.post.comments.length; i++) {
											var post = this.post.comments[i];
											if (this.notify_idPost == post.id) {
												if (!this.notify_idComment){
													this.goToByScroll('post'+ post.id);
													this.highLightDiv('post'+ post.id);
												}else {
													this.openComment(i);
													var d = document.getElementById('list'+i);
													var interval=setInterval(function(){
														self.loadMoreComment(post,d);
														setTimeout(function(){
															var n=post.comments.length;
															var h=0;
															for (var j = 0; j < n; j++) {
																h+=(document.getElementById('comment'+post.comments[j].id).clientHeight);
																if(post.comments[j].id==self.notify_idComment){
																	clearInterval(interval);
																	self.goToByScroll('post'+ post.id);
																	h=document.getElementById('list'+i).scrollHeight-h;
																	setTimeout(function(){
																		var d = document.getElementById('list'+i);
																		d.scrollTop=h;
																		self.highLightDiv('comment'+self.notify_idComment);
																		
																	},100);
																	break;
																}
															}
														},100);
													},150);
												}
												return;
											}
										}
									},
									countComment : function() {
										var self = this;
										for (var i = 0; i < this.post.comments.length; i++) {
											var j = i;
											var p = this.post.comments[j];
											p.numsComment = 0;
											var url = "/course/"
												+ this.post.idCourse
												+ "/forum/"
												+ this.post.id
												+ "/count_subcommemt?idPostReply="
												+ p.id;
											$.ajax({
												n : i,
												url : url,
												method : "post",
												success : function(data) {
													self.post.comments[this.n].numsComment = data;
													post.$forceUpdate();
												}
											});
										}
										setTimeout(function() {
											if (self.notify_idPost) {
												self.checkNotify();
											}
										},200);
									},
									loadPost : function() {
										if (!this.post.comments)
											this.post.comments = [];
										var url = "/course/"
												+ this.post.idCourse
												+ "/forum/" + this.post.id
												+ "/get_commemt?from="
												+ this.post.comments.length;
										var self = this;
										$
												.ajax({
													url : url,
													method : 'post',
													success : function(data) {
														for (var i = 0; i < data.length; i++) {
															data[i].newComment = {
																"files" : []
															}
														}
														self.post.comments = self.post.comments
																.concat(data);
														self.countComment();
													}
												});
									},
									formatDate : function(time) {
										var today = new Date(time);
										var dd = today.getDate();
										var mm = today.getMonth() + 1; // January
																		// is 0!

										var yyyy = today.getFullYear();
										if (dd < 10) {
											dd = '0' + dd;
										}
										if (mm < 10) {
											mm = '0' + mm;
										}
										var h = today.getHours();
										var m = today.getMinutes();

										var today = h + "h" + m + "," + dd
												+ '/' + mm + '/' + yyyy;
										return today;
									}
								}

							})
					post.member = test.member;
					post.post = test.post;
					post.loadPost();
					post.notify_idPost = test.notify_idPost;
					post.notify_idComment = test.notify_idComment;
					js.post = post;
				});
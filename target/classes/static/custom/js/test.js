window.addEventListener('load',function(){
	var mytest=new Vue({
		el:"#mytest",
		data:{
			idCourse:0,
			time:0,
			timeM:"00",
			timeS:"00",
			actionCountTime:null,
			isShowResult:false,
			result:"",
			
			listUserAnswer:[],
			
			infoTest:{},
			listEasy:[],
			listNormal:[],
			listHard:[],
			
			listId:[],
			
			pages:[],
			currentPage:0,
			listQuestion:[],
			nQuestionOfPage:10,
			
			listQuestionW:[],
			pagesW:[],
			
		},
		components:{
			question:{
				template:"#template-question",
				props:['q','showError','index'],
				methods:{
					getA:function(i){
						return String.fromCharCode(i+65);
					}
				}
			}
		},
		watch:{
			time:function(t){
				var minute=Math.floor(t/60);
				var second=t-minute*60;
				this.timeS=second>9?second:('0'+second);
				this.timeM=minute>9?minute:('0'+minute);
			}
		},
		methods:{
			getQuestionId:function(list){
				var max=list.length;
				var id=list[Math.floor(Math.random()*max)];
				if(this.listId.indexOf(id)==-1) return id;
				return this.getQuestionId(list);
			},
			initTest:function(){
				console.log(this.infoTest);
				var n1=this.infoTest.numsEasy;
				var n2=this.infoTest.numsNormal;
				var n3=this.infoTest.numsHard;
				for(var i=0;i<this.infoTest.nums;){
					var r=Math.floor(Math.random()*3);//0,1,2
					if(r===0&&n1>0){//cau hoi de
						n1--;
						i++;
						this.listId.push(this.getQuestionId(this.listEasy));
					}else if(r===1&&n2>0){//cau hoi trung binh
						n2--;
						i++;
						this.listId.push(this.getQuestionId(this.listNormal));
					}else if(r===2&&n3>0){//cau hoi kho
						n3--;
						i++;
						this.listId.push(this.getQuestionId(this.listHard));
					}
				}
				this.createNav();
			},
			createNav:function(){
				var p=[];
				var n=this.listId.length;
				for(var i=0;i<n;i++){
					p.push(this.listId[i]);
					if(i==n-1){
						this.pages.push(p);
					}else if(p.length==10){
						this.pages.push(p);
						p=[];
					}
				}
				this.loadQuestion(0,true);
			},
			mixAnswer:function(a){
				var j, x, i;
			    for (i = a.length - 1; i > 0; i--) {
			        j = Math.floor(Math.random() * (i + 1));
			        x = a[i];
			        a[i] = a[j];
			        a[j] = x;
			    }
			},
			checkRight:function(arr1,arr2){
				if(arr1.length!=arr2.length)return false;
				for(var i=0;i<arr2.length;i++){
					if(arr1.indexOf(arr2[i])==-1)return false;
				}
				return true;
			},
			onCheckRight:function(index,i){
				console.log(index+ ' '+i);
				var p=Math.floor((index)/10);
				var n=index-p*10;
				var b=$("#"+index+"-"+i).is(":checked");
				var q=this.listQuestion[p][n];
				if(b){
					q.userAnswer.push(i);
				}else{
					var k=q.userAnswer.indexOf(i);
					if(k!=-1)  q.userAnswer=q.userAnswer.slice(k,1);
				}
				q.isTrue=this.checkRight(q.userAnswer,q.rightAnswer);
				console.log(q);
			},
			onLoadQuestion:function(data,isFirst){
				var n=this.listQuestion.length*10+1;
				for(var i=0;i<data.length;i++){
					var q=data[i];
					q.index=n+i+'.';
					q.userAnswer=[];
					q.rightAnswer=[];
					q.isTrue=false;
					this.mixAnswer(q.answers);
					for(var j=0;j<q.answers.length;j++){
						q.answers[j].isCheck=false;
						if(q.answers[j]['true']) q.rightAnswer.push(j);
					}
				}
				this.listQuestion.push(data);
				if(isFirst) $("#modalStart").fadeIn();
			},
			loadQuestionW:function(key){
				this.currentPage=key;
			},
			loadQuestion:function(index,isFirst){
				if(this.listQuestion[index]!=null) {
					this.currentPage=index;
					return ;
				}
				var listId=this.pages[index];
				if(listId==null) return;
				console.log("load question :"+listId);
				var self=this;
				var url="/course/"+this.idCourse+"/test/get_question"
				$.ajax({
					url:url,
					method:'post',
					data:JSON.stringify(listId),
					contentType: 'application/json; charset=UTF-8',
					success:function(data){
						self.onLoadQuestion(data,isFirst);
						self.currentPage=index;
					//	console.log(data);
					}
				});
			},
			onStart:function(){
				this.actionCountTime=setInterval(function(){
					this.time+=1;
				}.bind(this),1000);
				$("#modalStart").hide();
			},
			onSubmit:function(){
				if(confirm("Do you really want to do this?")){
					if(this.actionCountTime!=null) clearInterval(this.actionCountTime);
					var nRight=0;
					
					var self=this;
					this.listQuestion.forEach(function(list){
						var p=[];
						console.log("ASd");
						for(var i=0;i<list.length;i++){
							var q=list[i];
							if(q.isTrue)nRight++;
							else{
								console.log("A");
								p.push(q);
								if(i==list.length-1){
									self.listQuestionW.push(p);
									self.pagesW.push(1);
								}else if(p.length==10){
									self.listQuestionW.push(p);
									p=[];
									self.pagesW.push(1);
								}
							}
						}
					});
					console.log("so cau dung: "+nRight);
					this.currentPage=0;
					this.isShowResult=true;
					this.result=nRight+"/"+this.infoTest.nums;
					console.log(this.listQuestionW);
				} else return false;
			},
		}
	});
	mytest.idCourse=test.idCourse;
	mytest.infoTest=test.info;
	mytest.listEasy=test.listEasy;
	mytest.listNormal=test.listNormal;
	mytest.listHard=test.listHard;
	mytest.initTest();
});
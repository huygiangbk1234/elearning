window.addEventListener("load",function(){
	var menu_lesson=new Vue({
		el:'#menu-lesson',
		data:{
			idCourse:-1,
			lessons:[],
		},
		methods:{
			isOver:function(time){
				return time<new Date().getTime();
			},
			getIcon:function(name){
				var ext=name.split(".");
				ext=ext[ext.length-1].toLowerCase();
				if(ext=='txt') return "<i class='fa fa-file-text-o' aria-hidden='true'></i>";
				else if(ext=='pdf') return "<i class='fa fa-file-pdf-o' aria-hidden='true' style='color:red'></i>";
				else if(ext=='docx')  return "<i class='fa fa-file-word-o' aria-hidden='true' style='color:blue'></i>";
				else if(ext=='mp4')  return "<i class='fa fa-file-video-o' aria-hidden='true' style='color:red'></i>";
				else if(ext=='png'||ext=='jpg'||ext=='gif')  return "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
				return  "<i class='fa fa-file' aria-hidden='true'></i>";
			},
			toggleOpen:function(index){
				var l=this.lessons[index];
				l.isOpen=!l.isOpen;
			},
			loadAllLesson:function(){
				var url="/course/"+this.idCourse+"/lesson/get";
				var s=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						data.forEach(function(l){l.isOpen=false;});
						s.lessons=data;
						menu_lesson.$forceUpdate();
					}
				})
			}
		}
		
	});
	menu_lesson.idCourse=test2.idCourse;
	menu_lesson.loadAllLesson();
	js.menu_lesson=menu_lesson;
})
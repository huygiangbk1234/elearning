window.addEventListener("load",function(){
	var video=new Vue({
		el:'#video',
		data:{
			list:{questions:[],times:[]},
			video:null,
			lessons:[],
			currentLesson:0,
			length:0,
		},
		methods:{
			setDefaultLesson:function(id){
				for(var i=0;i<this.lessons.length;i++){
					if(this.lessons[i].id==id){
						this.currentLesson=i;
						break;
					}
				}
			},
			loadAllLesson:function(){
				var url="/course/"+this.idCourse+"/lesson/get";
				var s=this;
				$.ajax({
					url:url,
					method:'post',
					success:function(data){
						s.lessons=data;
						if(test.idLesson){
							s.setDefaultLesson(test.idLesson);
						}
					}
				})
			},
			onBind:function(){
				$("#videoId").bind('playing',function() {
		            
		        });
				 $("#videoId").on("timeupdate", function(event){this.onSeek()}.bind(this));
		        $("#videoId").bind('progress',function() {
		        	console.log("Progress");
		        });
			},
			onSeek:function(){
				console.log( $("#video").currentTime);
			},
			addVideo:function(event){
				if(this.video) {
					$(this.video).unbind();
					$(this.video).remove();
				}
				var video = $('<video />', {
				    id: 'videoId',
				    src:URL.createObjectURL(event.target.files[0]),
				    type: 'video/mp4',
				    controls: true
				});
				this.list={questions:[],times:[]};
				video.appendTo($("#ct"));
				this.video=document.getElementById("videoId");
				this.onBind();
			},
			addQuestion:function(){
				this.video.pause();
				var time=this.video.currentTime;
				this.list.questions.unshift({
					question:"",
					answers:[],
					type:0
				});
				this.list.times.unshift(time);
			},
			removeQuestion:function(index){
				this.list.questions.splice(index,1);
			},
			removeAnswer:function(index,i){
				this.list.questions[index].answers.splice(i,1);
			},
			addAnswer:function(index){
				this.list.questions[index].answers.push({'true':false,answer:""});
			},
			getTime:function(t){
				var h=Math.floor(t/3600);
				var m=Math.floor((t-3600*h)/60);
				var s=Math.floor((t-3600*h-m*60));
				if(h<10)h="0"+h;
				if(m<10)m="0"+m;
				if(s<10)s="0"+h;
				return h+":"+m+":"+s;
			},
			save:function(){
				var form = document.getElementById("formFile");
				var formData = new FormData(form);
				var url="/course/"+this.idCourse+"/video/submit?idLesson="+this.lessons[this.currentLesson].id;
				var u="/course/"+this.idCourse+"/video/submit/question?idVideo=";
				var v=this.list;
				for(var i=0;i<this.list.questions.length;i++){
					var q=this.list.questions[i];
					var b=false;
					for(var j=0;j<q.answers.length;j++){
						if(q.answers[j]['true']) b=true;
					}
					if(!b){
						alert("Question not have right answer!");
						return;
					}
				}
				var l=this.video.duration;
				$.ajax({
					url:url,
					method:'post',
					contentType: false,
				    processData: false,
					data:formData,
					success:function(data){
						$.ajax({
							url:u+data+"&length="+l,
							method:'post',
							data:JSON.stringify(v),
							contentType:"application/json;charset=UTF-8",
							success:function(data1){
								location.reload();
							}
						});
					},
					error:function(){
						alert("error send data");
					}
				});
			}
		}
	});
	video.idCourse=test.idCourse;
	video.loadAllLesson();
});
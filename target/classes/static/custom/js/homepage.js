/**
 * 
 */
window.onload=function(){
var slide=new Vue({
	el: '#container-slide',
	data: {
	   isCurrent:0,
	   nums:4,
	   isUp:false,
	   isSliding:false,
	   active:[false,false,false,false,false],
	   slide:["#slide-1","#slide-2","#slide-3","#slide-4","#slide-5"],
	},
	created:function(){
		setTimeout(function(){
			slide.active[0]=true;
			slide.$forceUpdate();
		},100);
		
	},
	mounted:function(){
		$.each($("#slide-3 img"),function(index,img){
			$(img).css('-webkit-transition-delay',index*0.1+'s');
		});
	},
	watch:{
		isCurrent:function(n){
			this.active=[false,false,false,false,false];
			this.active[n]=true;
		}
	},
	methods:{
		
		effect:function(){
			this.slide[this.isCurrent].effect();
		},
		onNext:function(){
			if(this.isCurrent+1>this.nums)return;
			if(this.isSliding) return;
			this.isSliding=true;
			var self=this;
			setTimeout(function(){self.isSliding=false},500);
			$(this.slide[this.isCurrent]).animate({top: '100vh'},500);
			this.isCurrent++;
		},
		onPrev:function(){
			if(this.isCurrent-1<0)return;
			if(this.isSliding) return;
			this.isSliding=true;
			var self=this;
			setTimeout(function(){self.isSliding=false},500);
			this.isCurrent--;
			$(this.slide[this.isCurrent]).animate({top: '0'},500);
		}
	}
});
$(window).bind('mousewheel',function(event){
	if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
        slide.onPrev();
    }
    else {
    	slide.onNext();
    }
});
}
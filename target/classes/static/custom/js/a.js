//'use strict';
//var video=document.querySelector("video");
//
//var config={video:true,audio:false};
////navigator.getUserMedia(config,onStreamFromUser,function(){});
//function onStreamFromUser(stream){
//	video.srcObject=stream;
//	console.log(stream.length);
//}
//
//var nextRTC = new NextRTC({
//        // required
//        wsURL : 'ws://localhost:8080/socket',
//        // required
//    mediaConfig : {
//        video : true,
//                sdpConstraints : {
//            'mandatory' : {
//                'OfferToReceiveVideo' : true
//            }
//        },
//    },
//    // optional in local network
//    peerConfig : {
//        'iceServers' : [
//            { url : "stun:stun.l.google.com:19302" } 
//        ]
//    },
//});
//
//NextRTC.prototype.upperCase = function upperCase(content, custom) {
//    var nextRTC = this;
//    nextRTC.request('upperCase', null, "Hêlo", custom);
//}; 
// this one will send content which will be handled by our handler
var Peer = require('simple-peer')
var peer = new Peer({
  initiator: location.hash === '#init',
  trickle: false,
  stream: stream
})
peer.on('signal', function (data) {
  document.getElementById('yourId').value = JSON.stringify(data)
})

document.getElementById('connect').addEventListener('click', function () {
  var otherId = JSON.parse(document.getElementById('otherId').value)
  peer.signal(otherId)
})

document.getElementById('send').addEventListener('click', function () {
  var yourMessage = document.getElementById('yourMessage').value
  peer.send(yourMessage)
})

peer.on('data', function (data) {
  document.getElementById('messages').textContent += data + '\n'
})
